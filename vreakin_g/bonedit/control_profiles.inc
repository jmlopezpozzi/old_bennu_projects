//
//SELECTOR DE PERFIL DE CONTROL
//Sirve para cambiar el tipo de control.
//Se llama a la función control_profile y se le pasa como parámetro el número de perfil 
//de controles que se quiera usar (perfil==como funciona cada botón o par de botones en
//un momento determinado).
//El array key_filters[] guarda las id de cada filtro de tecla llamado, así luego se pueden
//eliminar cuando se quieran sustituir por otro.
//Por ahora es una versión muy básica, tiene 3 perfiles:
//1: todos los botones funcionan con el filtro Key_menu_mode
//2: los botones izquierda y derecha funcionan como Key_pair_exclusive, el resto como Key_menu_mode
//3: los pares de botones direccionales y botones de disparo funcionan como Key_pair_exclusive
//  ( modificar el 3 )
//4: las direcciones como Key_pair_exclusive y los demás como Key_menu_mode
//La función devuelve el perfil llamado (es decir, el parámetro pasado)
//

CONST
	total_profiles = 3;
END

GLOBAL
	int key_filters[ 5 ];
	byte current_control_profile;
END


FUNCTION control_profile( byte profile )

PRIVATE
	byte i;
END

BEGIN
	FOR ( i = 0; i < 6; i++ )
		IF ( key_filters[ i ] != 0 )
			signal( key_filters[ i ], s_kill );
		END
	END
	
	SWITCH ( profile )
		CASE 1:
			FOR ( i = 0; i < 6; i++ )
				key_filters[ i ] = Key_menu_mode( i );
			END
		END
		
		CASE 2:
			key_filters[ 0 ] = Key_pair_exclusive( 0, 1, 0 );
			FOR ( i = 1; i < 5; i++ )
				key_filters[ i ] = Key_menu_mode( i+1 );
			END
		END
		
		CASE 3:
			key_filters[ 0 ] = Key_pair_exclusive( 0, 1, 0 );
			key_filters[ 1 ] = Key_pair_exclusive( 2, 3, 1 );
			key_filters[ 2 ] = Key_pair_exclusive( 4, 5, 2 );
		END
	
		CASE 4:
			key_filters[ 0 ] = Key_pair_exclusive( 0, 1, 0 );
			key_filters[ 1 ] = Key_pair_exclusive( 2, 3, 1 );
			key_filters[ 2 ] = Key_menu_mode( 4 );
			key_filters[ 3 ] = Key_menu_mode( 5 );
		END
	END
			say( "-control_profile=="+profile );

	RETURN( profile );
END