//INIT.INC
//Procesos y funciones de arranque y control de la partida



//
// FUNCTION init( byte start_mode )
//
// Función que arranca la partida teniendo en cuenta las opciones establecidas a partir de Init_menu().
//byte start_mode  //indica si la partida arranca desde new_level_menu (start_mode==0) o level_loader (start_mode==1)
//
//


FUNCTION init( byte start_mode )  //start_mode==0 normal  start_mode==1 load level

BEGIN
	say( "Init" );
	say( "-start_mode=="+start_mode );

	//RESOLUCION DINAMICA
	IF ( res_x_dyn>0 )
		IF ( rotacion>=0 )
			res_x = dyn_res( &level.playfield_i );
		ELSE
			res_x = dyn_res( &level.playfield_j );
		END
		say( "Init: res_x dinamica resuelta, res_y=="+res_x );
	END
	IF ( res_y_dyn>0 )
		IF ( rotacion>=0 )
			res_y = dyn_res( &level.playfield_j );
		ELSE
			res_y = dyn_res( &level.playfield_i );
		END
		say( "Init: res_y dinamica resuelta, res_y=="+res_y );
	END

	//EJES I,J
	IF ( rotacion >= 0 )
		display_i = res_x;
		display_j = res_y;
				camera_offset_i = -DEV_PAD_X;
				camera_offset_j = -DEV_PAD_Y;
	ELSE
		display_i = res_y;
		display_j = res_x;
				camera_offset_i = -DEV_PAD_Y;
				camera_offset_j = (level.playfield_j+DEV_PAD_X)-res_x;
	END
		
	//hace que los playfields no puedan ser mas grandes que el display_i
	// (solo funciona con start_mode==0)
	
	IF ( start_mode==0 )
		IF ( res_x_dyn==0 AND level.playfield_i>display_i )
			level.playfield_i = display_i;
			say( "Init: playfield_i > display_i, nuevo valor playfield_i=="+level.playfield_i );
		END
		IF ( res_y_dyn==0 AND level.playfield_j>display_j )
			level.playfield_j = display_j;
			say( "Init: playfield_j > display_j, nuevo valor playfield_j=="+level.playfield_j );
		END
	END

	//SCREEN SETUP
	restore_type = COMPLETE_RESTORE;
	dump_type = COMPLETE_DUMP;

#ifdef SCALE2
	IF ( scale_2x_o==1 )
		scale_resolution = ((DEV_PAD_X+res_x)*10000)*2+(DEV_PAD_Y+res_y)*2;
		say( "Init: scaling 2X" );
	ELSE
		scale_resolution = -1;  //en la wiki pone que es NULL pero es -1!!
		say( "Init: scaling NONE" );
	END
#endif

	set_mode( res_x+DEV_PAD_X, res_y+DEV_PAD_Y, 32, MODE_WAITVSYNC );
	set_fps( 60, 0 );
	
	rt_graphics_generation( );
	graphics_generation( );
	
		
	//linea dev_pad
	//drawing_color( rgb( 255, 255, 255 ) );
	//draw_line( DEV_PAD_X, 0, DEV_PAD_X, res_y );
	//raw_line( 0, DEV_PAD_Y, res_x, DEV_PAD_Y );
		
	//LINEAS PLAYFIELD
	drawing_z( 0 );
	drawing_color( rgb( 255, 255, 127 ) );
	IF ( rotacion>=0 )
		draw_line( -1-camera_offset_i, -1-camera_offset_j, level.playfield_i-camera_offset_i, -1-camera_offset_j );
		draw_line( level.playfield_i-camera_offset_i, -1-camera_offset_j, level.playfield_i-camera_offset_i, level.playfield_j-camera_offset_j );
		draw_line( level.playfield_i-camera_offset_i, level.playfield_j-camera_offset_j, -1-camera_offset_i, level.playfield_j-camera_offset_j );
		draw_line( -1-camera_offset_i, level.playfield_j-camera_offset_j, -1-camera_offset_i, -1-camera_offset_j );
	ELSE
		draw_line( display_j-0+camera_offset_j, -1-camera_offset_i, display_j-0+camera_offset_j, level.playfield_i-camera_offset_i );
		draw_line( display_j-1+camera_offset_j, level.playfield_i-camera_offset_i, display_j-level.playfield_j+camera_offset_j, level.playfield_i-camera_offset_i );
		draw_line( display_j-level.playfield_j+camera_offset_j-1, level.playfield_i-camera_offset_i, display_j-level.playfield_j+camera_offset_j-1, -1-camera_offset_i );
		draw_line( display_j-level.playfield_j+camera_offset_j, -1-camera_offset_i, display_j-1+camera_offset_j, -1-camera_offset_i );
	END
		
	//chuleta xa rotado: x = display_j-j_pos+camera_offset_j;
	

	current_control_profile = control_profile( 3 );
	Control_program( );

	IF ( start_mode==0 )
		say( "Init: llamando a map_generation" );
		map_generation( );  //si map_generation() es llamado antes de Debug_ball_spawn() luego este no es reconocido por su type al usar signal(id|type,signal)
		                    //get_status( int processID ) en principio no funciona con tipos de proceso
	ELSEIF ( start_mode==1 )
		say( "Init: llamando a level_render" );
		level_render( &level.block_layout );
	END

	paddle_id = Paddle( level.playfield_i/2.0 , level.playfield_j-_paddle_j_bottom );
	Ball( level.playfield_i/2.0 , level.playfield_j-80.0 , 1.0, gameplay.ball_j_speed );
	
	game_state = 1;
	
	text_z = 0;
	write( 0, HSX, BORDER_UP-10, 5, "Ladrillos que faltan: " );
	write_var( 0, HSX, BORDER_UP-10, 3, blocksleft );

END




//
// FUNCTION dyn_res( int pointer playfield_assign )
//
// Se encarga de establecer la resolución en un eje teniendo en cuenta el tamaño del playfield en el mismo eje. Lo hace usando pasos discretos
//incrementando al siguiente una vez en anterior es sobrepasado. Se usa cuando la opción de resolución dinámica está activada para el eje.
//
// int pointer playfield_assing  //Puntero al eje del playfield para el que hay que resolver la resolución
//
//


FUNCTION dyn_res( int pointer playfield_assign )

BEGIN
	say( "dyn_res: playfield_assign=="+*playfield_assign );
	SWITCH ( *playfield_assign )
		CASE 1..240:
			RETURN( 240 );
		END
		CASE 241..320:
			RETURN( 320 );
		END
		CASE 321..480:
			RETURN( 480 );
		END
		CASE 481..640:
			RETURN( 640 );
		END
		DEFAULT:
			say( "-dyn_res: out of available resolution ranges, playfield_assign=="+playfield_assign );
		END
	END
END




//
// PROCESS Control_program( )
//
// Proceso que se ejecuta durante la partida y permite cambiar los fps, lanzar una bola nueva,
//despertar al proceso debug_ball_spawn(), deshacer o rehacer desapariciones de bloque, generar un mapa nuevo e 
//invocar al gameplay edit menu durante la partida.
//
// Para despertar al proceso Ball_debug_spawn() se comprueba primero que este esté dormido. Si se llama a la función
//map_generation() -en esta versión llamada desde Init(), que crea todos los bloques, antes que a debug_ball_spawn(), al comprobar 
//si este está dormido con get_status( type debug_ball_spawn)==3 obtenemos un resultado erroneo. En el caso de guardar en una variable 
//(dbs_id) la id del proceso debug_ball_spawn() al crearlo, get_status( dbs_id)==3 da el resultado correcto (independientemente
//de cuando se llame a map_generation() ). No entiendo del todo por qué pasa esto.
//
// RESTART PROGRAM (reiniciar programa) manda una señal a todos los demás procesos para que terminen su ejecución, descarga 
//los archivos fpg, borra textos y dibujos, pone los valores pertinentes a 0 y hace que Control_program() termine su ejecución
//para llamar a Main() en su onexit. Restart program no siempre funciona bien y muchas veces cuelga el programa, al parecer
//tras haber terminado todos los procesos y antes del FRAME; que va después del let_me_alone().
//
// UNDO (deshacer) utiliza la posición x e y del bloque al que apunte blocksleft (guardadas en undo_buffer[blocksleft]) y crea
//un nuevo bloque en la posición del antiguo, guardando su nueva id (también en undo_buffer[blocksleft]), luego incrementa
//blocksleft.
//
// REDO (rehacer) decrementa blocksleft y utiliza la id guardada en undo_buffer[blocksleft].redo_id para eliminar el proceso
//con dicha id. No se puede rehacer más allá del último bloque que recibió una colisión. Esto se consigue guardando el 
//valor de blocksleft en redo_limit una bola destruye un bloque y bloqueando la posibilidad de destruir bloques mediante 
//redo si blocksleft es igual a redo_limit.
//
// RESET MAP destruye todos los bloques y vuelve a generar todos los bloques (ninguno roto). Los valores de blocksleft y de
//redo_limit se actualizan, pero no se toca ningun dato en undo_buffer[]. Los datos antiguos van siendo reescritos a medida
//que se usa la tabla (array). Reset map solo se puede hacer con niveles generados automaticamente (no con niveles cargados).
//
// GAMEPLAY EDIT invoca al menu de gameplay edit menu. Para ello pausa la partida, congelando los processos Paddle() y Ball()
//existentes y cambiando el game_state a 2 (pausa). En caso de que se alteren valores del struct global gameplay mediante este
//menu, game_state pasa a ser 3 (actualizar gameplay), se eliminan tanto Paddle() como todas las instancias de Ball(), se generan
//de nuevo los gráficos de estos (con la función rt_graphics_generation()), y se vuelve a invocar a Paddle(). Esto se hace 
//porque el struct gameplay afecta como son y se comportan las bolas y la pala. Hecho esto, el game_state vuelve a ser 1 (partida)
//estado en que los elementos de la partida no están congelados y el resto de funciones de Control_program() funcionan.
// Es importante separar los fpg de los gráficos que no cambian durante la partida (como los bloques) de los que pueden cambiar
//(bola y pala), de lo contrario los bloques pierden su graph al apuntar a un file antiguo.
//
//


PROCESS Control_program( )

PRIVATE
	int dbs_id;
END

BEGIN
	priority = _control_program_priority;
	say( "Control_program" );
			
	dbs_id = Debug_ball_spawn( );
	say( "Control_program: Llamando a Debug_ball_spawn, dbs_id=="+dbs_id );

	LOOP
		
		IF ( key( _f2 ) )  //RESTART PROGRAM
			                        say( "Control_program: RESTART COMMAND" );
			let_me_alone( );        say( "Control_program: Restarting program" );
				FRAME;              say( "Control_program: Porcesses terminated" );
			IF (fpg_exists( idfpg ))
				fpg_unload( idfpg );
			END
			IF (fpg_exists( idfpg_rt))
				fpg_unload( idfpg_rt );
			END                     say( "Control_program: FPGs unloaded" );
			delete_draw( 0 );
			delete_text( 0 );
			blocksleft = 0;			
			game_state = 0;         say( "Control_program: Calling Main" );
			//Main( );
			
				FRAME;
			BREAK;
		END
		
		IF ( game_state==1 )
		
			IF ( key( _v ) )  //60FPS
				set_fps( 60, 0 );
			END
			IF ( key( _c ) )  //03FPS
				set_fps( 3, 0 );
			END
			IF ( key( _z ) )  //BOLA NUEVA
				ball( level.playfield_i/2.0, level.playfield_j-80.0, 1.0, gameplay.ball_j_speed );
				WHILE ( key( _z ) )
					FRAME;
				END
			END

			//UNDO
			IF ( key( _o ) AND blocksleft < undo_limit )  WHILE ( key( _o ) ) FRAME; END
				undo_buffer[blocksleft].redo_id = Block_put( undo_buffer[blocksleft].pos_x*1.0, 
				                                             undo_buffer[blocksleft].pos_y*1.0, undo_buffer[blocksleft].block_code );
				//undo_buffer[blocksleft].redo_id = Block( undo_buffer[blocksleft].pos_x*1.0, 
				//                                         undo_buffer[blocksleft].pos_y*1.0, undo_buffer[blocksleft].block_code );
				say( "UNDO: Recuperando bloque en undo_buffer["+blocksleft+"] pos_x="+undo_buffer[blocksleft].pos_x+" pos_y="+undo_buffer[blocksleft].pos_y+" redo_id="+undo_buffer[blocksleft].redo_id );
				blocksleft++;
			END
		
			//REDO
			IF ( key( _i ) AND blocksleft > redo_limit )  WHILE ( key( _i ) ) FRAME; END
				say( "REDO: Destruyendo bloque en undo_buffer["+(blocksleft-1)+"] redo_id="+undo_buffer[blocksleft-1].redo_id );
				blocksleft--;
				signal( undo_buffer[blocksleft].redo_id, s_kill );
			END

			//DEBUG BALL SPAWN
			IF ( key( _e ) AND get_status( dbs_id ) == 3 )
				signal( dbs_id, s_wakeup );
				say( "Control_program to BDS: RISE FROM YOUR GRAVE!" );
			END
			
			//RESET MAP
			IF ( key( _m ) )  WHILE ( key( _m ) ) FRAME; END
				IF ( file_version_code==22 )  //comprueba que el mapa haya sido generado por map_generation()
					blocksleft = 0;
					say( "Control_program: Reset, blocksleft=="+blocksleft );
					signal( type block, s_kill );  //rompe todos los bloques de golpe
					IF ( get_id( type Block ) == 0 )
						say( "Control_program: Reset successful" );
					ELSE
						say( "Control_program: Reset failed, Block instances left" );
					END
					FRAME;
					map_generation( );
				ELSE
					say( "Control_program: Reset unavailable, file_version_code=="+file_version_code );
				END
			END
			
			//DEBUG_BALL_SPAWN
			IF ( key( _e ) AND get_status( dbs_id ) == 3 )
				signal( dbs_id, s_wakeup );
				say( "Control_program to BDS: RISE FROM YOUR GRAVE!" );
			END
			
			//GAMEPLAY EDIT			
			IF ( key( _g ) )  WHILE ( key( _g ) ) FRAME; END
				say( "Control_program: calling Gameplay_edit_menu");
				WHILE ( game_state = get_id( type Paddle ) )
					signal( game_state, S_FREEZE_TREE );
					say( "-Control_program: Paddle found, id=="+game_state+" frozen" );
				END

				WHILE ( game_state = get_id( type Ball ) )
					signal( game_state, S_FREEZE_TREE );
					say( "-Control_program: Ball found, id=="+game_state+" fozen" );
				END
				game_state = 2;
				Gameplay_edit_menu( );
				Menu_cover( -3, 320, 240 );
			END

			//PADDLE LIVE SWITCH
			IF ( key( _p ) AND !exists( type Paddle_live_switch ) )
				Paddle_live_switch( );
			END


		ELSEIF ( game_state==2 )

			IF ( !exists( type Gameplay_edit_menu ) )         //a lo mejor con comprobar backfire ya sirve
				WHILE ( game_state = get_id( type Paddle ) )
					IF ( get_status( game_state )==4 )
						signal( game_state, S_WAKEUP_TREE );  //signal( type tipo, s_kill ) deberia terminarlos todos...
					END
				END
				WHILE ( game_state = get_id( type Ball ) )
					IF ( get_status( game_state )==4 )
						signal( game_state, S_WAKEUP );
					END
				END
				current_control_profile = control_profile( 3 );
				game_state = 1;
			END


		ELSEIF ( game_state==3 )

			IF ( !exists( type Gameplay_edit_menu ) )         //a lo mejor con comprobar backfire ya sirve
				WHILE ( game_state = get_id( type Paddle ) )
					signal( game_state, S_KILL_TREE );
				END
				WHILE ( game_state = get_id( type Ball ) )
					signal( game_state, S_KILL );
				END
				//graphics_generation( );                     //hace que los bloques pierdan su graph
				rt_graphics_generation( );
				paddle_id = Paddle( level.playfield_i/2.0 , level.playfield_j-_paddle_j_bottom );
				current_control_profile = control_profile( 3 );
				game_state = 1;
			END	

		END


		IF ( ball_pos > 6 )  //modulo??
			ball_pos = 0;
		END
		
		//PRIORITY CHECK
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type CONTROL_PROGRAM" );
		END
		
		FRAME;
		
	END

ONEXIT
	say( "Control_program exiting, calling Main" );
	Main( );
END