/*
* grid-ciber-gorilas.prg
*
*Esta versión usa un array para representar el mapa y hace todas las
*comprobaciones de juego usando este array, la posición, el tamaño y
*el movimiento de los objetos. Esta versión no usa sensores ni la
*función collision de Bennu.
*
*El hecho de que haya dos versiones se debe a que al plantear el
*funcionamiento del juego se me ocurrieron las dos formas, y al no
*saber cuál era "la correcta" decidí hacerlo de las dos maneras para
*ver los pros y los contras de cada una.
*/


#define MAX_TILES_X screenx/tilesize
#define MAX_TILES_Y screeny/tilesize
#define H_HITBOXSIZE hitboxsize/2

#define NOLRKEY controller[0]
#define KEYLEFT controller[1]
#define KEYRIGHT controller[2]
#define KEYUP controller[3]
#define KEYDOWN controller[4]
#define KEYAIM controller[5]
#define KEYSHOOT controller[6]


//#define PRECISE
#define DEBUG_INFO
#define PIXEL_CHECK 3
//Pixel check==> 1=ambos, 2=solo hitbox, 3=solo bananas


Import "mod_proc";
Import "mod_grproc";
Import "mod_video";
Import "mod_map";
Import "mod_draw";
Import "mod_rand";
Import "mod_key";
Import "mod_text";
Import "mod_say";
Import "mod_math";
Import "mod_time";



Const

	screenx=320;
	screeny=240;
	altura_maxima=11;
	altura_minima=2;
	ancho_minimo=2;
	ancho_maximo=5;
	tilesize=16;
	hitboxsize=14;
	gravedad=0.175;
	accel=0.075;
	jet_fuerza=0.25;
	jet_max_fuel=50;
	min_angulo=0;
	max_angulo=90;
	min_fuerza=20;          //minimo de fuerza con la que se puede disparar
	max_fuerza=100;         //la fuerza se entra como porcentaje, el máximo es 100
	relacion_fuerza=0.085;  //constante por la que es multiplicada la fuerza
	tiempo_mensaje=170;     //tiempo de muestra del mensaje arbitral en frames
	max_puntos=3;           //deja de ser const cuando se puedan configurar los puntos máximos
	rango_ancho=5;
	rango_alto=12;
	rango_puntos=15;

End


Global

	int render_counter;        //cuenta las columnas dibujadas en el mapa al generarlo
	int grid[screenx/tilesize-1][screeny/tilesize-1];
	int spawn[2]=3;            //tabla para conocer las posiciones de aparición de los hitbox
	int turn=1;                //indica quien tiene el control de su hitbox (en su turno)
	int turn_promotion=1;      //indica a quien le toca el próximo turno
	int puntos[2]=max_puntos;  //se usan 3 posiciones porque se pasa pl como índice para elegir el campo. opciones.puntos guarda la puntuacion tambien
	int shooting;              //modo shooting. Vale 1 cuando se está apuntando o cuando vuela un proyectil. puede ser bool
	int graficos[3];           //guarda las id de los gráficos al generarlos
	int int_zero_int=0;        //puede que la violacion de segmento se de al acceder a punteros de datos que no existen, así que en el caso de que apunte a algo que no pueda existir probaré a crear un puntero que apunte siempre a un int de valor 0 para esos casos. EXACTO, es así, pero en vez de crear otro puntero, creo el int que vale 0 y lo apunto desde el puntero local (n,s,e,w) correspondiente cuando este tenga que valer cero.

	bool controller[6];    //0=nolr,1=left,2=right,3=up,4=down,5=aim,6=shoot
	bool lr_helper[2];     //0=signal,1=previous signal,3=lorr_switch
	bool ud_helper[2];     //0=signal,1=previous signal,3=uord_switch
	bool aim_helper[1];    //0=signal,1=previous signal
	bool shoot_helper[1];  //0=signal,1=previous signal

	struct opciones
		int puntos=max_puntos;
		int ancho_min=ancho_minimo;
		int ancho_max=ancho_maximo;
		int altura_min=altura_minima;
		int altura_max=altura_maxima;
		bool simetrico=0;
	End	

End


Declare Process bloque(int i, int j)

	Public
		int pointer north;
		int pointer east;
		int pointer south;
		int pointer west;
	End

End


#ifdef PRECISE
Declare Process hitbox(int pl)

	Public

		int posicion_i;   //posición horizontal en la rejilla	
		int posicion_j;   //posición vertical en la rejilla
		int xlec;         //posición horizontal dentro de la casilla actual
		int ylec;         //posición vertical dentro de la casilla actual
		float dx;         //cálculo del desplazamiento horizontal
		float resta_dx;   //guarda la parte decimal para tenerla en cuenta al transformar a entero
		float dy;         //desplazamiento vertical
		float resta_dy;   //idem de resta_dx
		int idx;          //desplazamiento horizontal en pixeles
		int idy;          //desplazamiento vertical en pixeles
		int going;        //indica en que sentido se aplica la fuerza horizontal al moverse
		int direction;    //sentido horizontal del movimiento
		int direction_y;  //sentido vertical del movimiento
		int floor_check;  //vale 0 si hitbox está sobre un bloque, 1 si son dos con el de adelante, -1 si son dos con el de atras
		int jetpack;		
		int on_ground;    //vale 1 si el hitbox reposa en el suelo. puede ser bool
		int player;       //copia de pl, permite identificar al jugador desde otros procesos
		int left_limit;   //podria ser privada aunque este en el PRECISE
		int right_limit;  //idem (se usan para limiter, btw)

	End
	
End
#endif 

#ifndef PRECISE
Declare Process hitbox(int pl)

	Public

		int posicion_i;		
		int posicion_j;
		int xlec;
		int ylec;
		int player;

	End

End
#endif


/*PROCESS Main()
*
*Inicia el juego. Establece la resolución y el framerate, genera los gráficos y llama a Main2() .
*/

Process Main()

Begin

	set_mode(screenx,screeny,32);
	set_fps(60,0);

	graphics_generation();

#ifdef DEBUG_INFO
	write_var(0,0,0,0,render_counter);
	write_var(0,0,30,0,fps);
	write_var(0,0,40,0,turn);
	write_var(0,0,50,0,turn_promotion);
#endif
	
	Main2();

End



/*PROCESS Main2()
*
*Bucle principal del juego. Se encarga de generar el mapa a pedido del usuario, llamar al menu de opciones y de establecer la partida. Durante
*esta se encarga de actualizar los controles y de salir del juego al recibir el comando. Se usa como punto de retorno en la función reset_game()
*/

Process Main2()

Begin

	map_generation();

	Loop
		controller_update();

		If (key(_esc))
			let_me_alone();
			exit();
		End

		If (KEYAIM)
			map_generation();
		End

		FRAME;

		If (KEYSHOOT)
			BREAK;
		End

		If (key(_o))
			options_menu();
		End
	End

	pause_menu();
	hitbox(1);
	hitbox(2);

	Loop
		If (key(_esc))
			exit();
		End

		controller_update();
#ifdef DEBUG_INFO
		If (key(_c) AND exists(type cursor)==0)
			cursor();
		End
#endif
		FRAME;
	End

End



/*FUNCTION graphics_generation()
*
*Genera los gráficos con funciones de dibujo y guarda sus id en la tabla global graficos[].
*Si la función es llamada y los gráficos han sido generados previamente no los vuelve a generar.
*/

Function graphics_generation()

Begin

	//BLOQUE
	graficos[0]=map_new(tilesize,tilesize,32);
	map_clear(0,graficos[0],rgb(255,0,0));
	drawing_map(0,graficos[0]);
	drawing_color(rgb(0,255,0));
	draw_rect(0,0,tilesize-1,tilesize-1);

	//HITBOX
	graficos[1]=map_new(hitboxsize,hitboxsize,32);
	map_clear(0,graficos[1],rgb(255,255,0));

	//BANANA
	graficos[2]=map_new(8,8,32);
	drawing_map(0,graficos[2]);
	drawing_color(rgb(255,255,127));
	draw_fcircle(4,4,3);

	//LETRERO DE PAUSA
	graficos[3]=map_new(text_width(0,"PAUSA")+2,text_height(0,"PAUSA")+2,32);
	map_clear(0,graficos[3],rgb(0,0,0));
	map_put(0,graficos[3],write_in_map(0,"PAUSA",0),1,1);

End



/*FUNCTION map_generation()
*
*Borra el mapa actual (si hay uno) y genera uno nuevo teniendo en cuenta los parámetros de la tabla global opciones .
*Para ello llama a la funcion edificio(int ancho, int alto) sucesivamente hasta llenar la pantalla. Le pasa parámetros
*generados aleatoriamente dentro de los rangos establecidos por la tabla global opciones.
*
*Para generar mapas simétricos, una vez ha llegado a la mitad de la pantalla, lee la rejilla de adelante hacia atrás y 
*dibuja el mapa, columna a columna, de acuerdo a si en su posición simétrica a sido puesto un bloque.
*
*Tiene un mecanismo para que no se le pase el mismo valor como parámetro alto a dos llamadas sucesivas a
*la función edificio(int ancho, int alto) .
*/

Function map_generation()

Private

	int i;
	int altura;
	int altura_previa;  //altura y altura_previa se usan para evitar repeticiones de altura en los edificios

End

Begin

	//RESETEA EL MAPA ANTERIOR
	While (exists(type bloque))	
		signal(type bloque,s_kill);
	End

	For (render_counter=0;render_counter<MAX_TILES_X;render_counter++)
		For (i=0;i<MAX_TILES_Y;i++)
			grid[render_counter][i]=0;
		End
	End

	render_counter=0;

	spawn[1]=0;
	spawn[2]=0;

	//GENERA UN MAPA NUEVO
	If (opciones.altura_min!=opciones.altura_max)
		If (opciones.simetrico==1)

			say("llamado a edificio!");

			Repeat
				altura=rand(opciones.altura_min,opciones.altura_max);

				If (altura!=altura_previa)
					edificio(rand(opciones.ancho_min,opciones.ancho_max),altura);
					rand_seed(rand(0,32000)+get_timer());
				Else
					rand_seed(rand(0,16000)+get_timer());
				End

				altura_previa=altura;
			Until (render_counter>MAX_TILES_X/2-opciones.ancho_max)

			say("ultima llamada a edificio antes de la simetría!");

			Repeat
				altura=rand(opciones.altura_min,opciones.altura_max);
			Until (altura!=altura_previa)

			edificio(MAX_TILES_X/2-render_counter,altura);
				
			//Se deja de llamar a edificio() y se leen las columnas del centro hacia atrás una a una para pintarlas en orden a partir del cenrto
			For (render_counter;render_counter<MAX_TILES_X;render_counter++)	
				For (i=MAX_TILES_Y-1;grid[MAX_TILES_X-render_counter-1][i]!=0;i--)
					grid[render_counter][i]=bloque(render_counter,i);
				End
			End

			spawn[2]=spawn[1];  //el spawn es el mismo si el mapa es simetrico		
		Else
			Repeat
				say("llamado a edificio!");
				altura=rand(opciones.altura_min,opciones.altura_max);

				If (altura!=altura_previa)
					edificio(rand(opciones.ancho_min,opciones.ancho_max),altura);
					rand_seed(rand(0,32000)+get_timer());
				Else
					rand_seed(rand(0,16000)+get_timer());
				End

				altura_previa=altura;
			Until (render_counter>MAX_TILES_X-opciones.ancho_max)
	
			say("ultima llamada a edificio!");

			Repeat
				altura=rand(opciones.altura_min,opciones.altura_max);
			Until (altura!=altura_previa)

			edificio(MAX_TILES_X-render_counter,altura);
		End
	Else
		edificio(MAX_TILES_X,opciones.altura_min);  //en el caso de que la altura minima y maxima sean iguales en las opciones
	End

End



/*
*FUNCTION edificio(int ancho, int alto)
*
*Pone en el mapa un grupo de ancho*alto procesos bloque(i,j) . Lo hace creando tantas columnas como indique el parámetro ancho
*de tantos procesos bloque(i,j) como indique el parámetro alto, desde la parte inferior de la pantalla. Guarda la id de cada 
*proceso bloque(i,j) creado en la posición de la rejilla acorde (grid[i][j]. Para saber en que punto
*del ancho del mapa tiene que empezar a los procesos bloque(i,j) lee la variable global render_counter, que lleva cuenta de cuantas
*columnas han sido dibujadas. Al terminar de llamar a los procesos bloque(x,y) suma la cantidad de columnas avanzadas al valor de
*la variable global render_counter.
*/

Function edificio(int ancho, int alto)

Private

	int i;
	int j;

End

Begin

	say("edificio("+ancho+","+alto+") id=="+id);

	For (i=0;i<ancho;i++)
		If (render_counter+i==spawn[0]-1 AND spawn[1]==0)  //-1 pq render_counter empieza de 0			
			spawn[1]=MAX_TILES_Y-alto;
			say("spawn[1]=="+spawn[1]);
		End

		If (render_counter+i==MAX_TILES_X-spawn[0] AND spawn[2]==0)
			spawn[2]=MAX_TILES_Y-alto;
			say("spawn[2]=="+spawn[2]);
		End

		For (j=MAX_TILES_Y;j>MAX_TILES_Y-alto;j--)
			grid[render_counter+i][j-1]=bloque(render_counter+i,j-1);  //se asigna a grid[1] j-1 para tener en cuenta que la grid empieza en 0
		End
	End

	render_counter=render_counter+i;
	
End



/*FUNCTION controller_update()
*
*Rellena los campos de la tabla global controller[6] según el estado del teclado, siguiendo distintas reglas para cada
*tecla o grupo de teclas que se usa. La mayoría de input se lee a través de esta tabla en todo el programa.
*/

Function controller_update()

Begin
		
	//NOLRKEY
	If (key(_left) OR key(_right))  //si izqd o drcha son pulsadas NORLEY vale 0 (y viceversa)
		NOLRKEY=0;
	Else
		NOLRKEY=1;
	End

	//LEFT OR RIGHT (exclusivas)
	If (NOLRKEY==0)  //si NORLEY vale 0 se ve que se hace con izqd y drch
		If (key(_left) AND key(_right))  //si ambas teclas se pulsan a la vez se pone [0]SIGNAL a 1
			lr_helper[0]=1;
		Else
			lr_helper[0]=0;  //si no, se pone [0]SIGNAL a 0 y [2]LORR según la tecla única pulsada (0=left,1=right)

			If (key(_left))
				lr_helper[2]=0;
			End

			If (key(_right))
				lr_helper[2]=1;
			End
		End

		If (lr_helper[0]==1 AND lr_helper[1]==0)  //si SIGNAL es 1 y PREVIOUS_SIGNAL=0 se hace el switch de LORR
			If (lr_helper[2]==0)
				lr_helper[2]=1;
			Else
				lr_helper[2]=0;
			End
		End

		lr_helper[1]=lr_helper[0];  //se le da a [1]PREVIOUS_SIGNAL el valor de [0]SIGNAL
			
		If (lr_helper[2]==0)  //según lo que valga LORR_SWITCH se pasa un estado a LEFT y a RIGHT 
			KEYLEFT=1;
			KEYRIGHT=0;
		Else
			KEYLEFT=0;
			KEYRIGHT=1;
		End
	Else                      //si NOSKEY está activa tanto LEFT como RIGHT se ponen a cero
		KEYLEFT=0;
		KEYRIGHT=0;
	End

	//UP OR DOWN (exclusivas)
	If (key(_up) OR key(_down))
		If (key(_up) AND key(_down))  //si ambas teclas se pulsan a la vez se pone [0]SIGNAL a 1
			ud_helper[0]=1;
		Else
			ud_helper[0]=0;  //si no, se pone [0]SIGNAL a 0 y [2]UORD según la tecla única pulsada (0=up,1=down)

			If (key(_up))
				ud_helper[2]=0;
			End

			If (key(_down))
				ud_helper[2]=1;
			End
		End

		If (ud_helper[0]==1 AND ud_helper[1]==0)  //si SIGNAL es 1 y PREVIOUS_SIGNAL=0 se hace el switch de UORD
			If (ud_helper[2]==0)
				ud_helper[2]=1;
			Else
				ud_helper[2]=0;
			End
		End

		ud_helper[1]=ud_helper[0];  //se le da a [1]PREVIOUS_SIGNAL el valor de [0]SIGNAL
			
		If (ud_helper[2]==0)        //según lo que valga UORD_SWITCH se pasa un estado a UP y a DOWN 
			KEYUP=1;
			KEYDOWN=0;
		Else
			KEYUP=0;
			KEYDOWN=1;
		End
	Else                            //si no se pulsan ni _up ni _down, tanto UP como DOWN se ponen a cero
		KEYUP=0;
		KEYDOWN=0;
	End

	//AIM	(un pulso)
	If (key(_control))    //se comprueba CTRL (AIM)
		aim_helper[0]=1;  //si es que si se enciende su [0]SIGNAL

		If (aim_helper[0]==1 AND aim_helper[1]==0)  //y en el caso de que [0]SIGNAL valga 1 y [1]PREVIOUS_SIGNAL valga 0 se envía AIM
			KEYAIM=1;
		Else
			KEYAIM=0;
		End
	Else                  //si CTRL no se pulsa SIGNAL y AIM se ponen a 0
		aim_helper[0]=0;
		KEYAIM=0;
	End

	aim_helper[1]=aim_helper[0];  //Al final, [1]PREVIOUS_SIGNAL coge el valor de [0]SIGNAL*/

	//SHOOT	(un pulso)
	If (key(_space))  //se comprueba  SPACE (SHOOT), funciona igual que AIM
		shoot_helper[0]=1;	

		If (shoot_helper[0]==1 AND shoot_helper[1]==0)	
			KEYSHOOT=1;
		Else
			KEYSHOOT=0;
		End
	Else		
		shoot_helper[0]=0;
		KEYSHOOT=0;
	End

	shoot_helper[1]=shoot_helper[0];  //Al final, [1]PREVIOUS_SIGNAL coge el valor de [0]SIGNAL*/

End



/*
*FUNCTION turn_change()
*
*Altera el valor de la variable global turn_promotion entre dos posibles valores cada vez que es llamada.
*Seguramente se pueda implementar como una MACRO.
*/

Function turn_change()

Begin

	If (turn_promotion==1)
		turn_promotion=2;
	Else
		turn_promotion=1;
	End

End



/*
*FUNCTION reset_game()
*
*Restaura los valores iniciales de ciertas variables, detiene al resto de procesos, llama al proceso Main() y termina.
*/

Function reset_game()

Begin

	puntos[2]=0;
	puntos[1]=0;
	shooting=0;
	turn=1;
	turn_promotion=1;

#ifdef PIXEL_CHECK
	map_clear(0,0,0);
#endif

	let_me_alone();
	Main2();

End



/*
*PROCESS options_menu()
*
*Un menú que permite alterar los parámetros de la tabla global options.
*Duerme al resto de procesos y llama al proceso options_controller() , dejando sólo estos dos procesos en ejecución.
*Tiene un mecánismo para dar valores aleatorios a cada elemento de la tabla global options.
*Respeta rangos de mínimo y máximo al operar. Al terminal despierta al resto de procesos y llama a la función
*map_generation() para que genere un nuevo mapa.
*/

Process options_menu()

Private

	int idtexto;
	int idtextovar;
	int cursor;
	string textos[5]="SIMETRIA","PUNTOS PARA GANAR","ANCHO MINIMO","ANCHO MAXIMO","ALTURA MINIMA","ALTURA MAXIMA";

End

Begin

	signal(ALL_PROCESS,s_sleep);
	options_controller();

	Loop
		idtexto=write(0,screenx/2,screeny/2-10,4,textos[cursor]);

		Switch (cursor)
			Case 0:  //SIMETRIA
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.simetrico);

				Repeat
					If (KEYLEFT==1 OR KEYRIGHT==1)
						If (opciones.simetrico==0)
							opciones.simetrico=1;
						Else
							opciones.simetrico=0;
						End
					End
					
					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End

			Case 1:  //PUNTOS PARA GANAR
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.puntos);

				Repeat
					If (KEYRIGHT AND opciones.puntos<rango_puntos)
						opciones.puntos++;
					End

					If (KEYLEFT AND opciones.puntos>1)
						opciones.puntos--;
					End

					If (KEYAIM)
						rand_seed(rand(0,16000)+get_timer());
						opciones.puntos=rand(1,rango_puntos);
					End

					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End

			Case 2:  //ANCHO MINIMO
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.ancho_min);

				Repeat
					If (KEYRIGHT AND opciones.ancho_min<opciones.ancho_max)
						opciones.ancho_min++;
					End

					If (KEYLEFT AND opciones.ancho_min>1)
						opciones.ancho_min--;
					End

					If (KEYAIM)
						rand_seed(rand(0,16000)+get_timer());
						opciones.ancho_min=rand(1,opciones.ancho_max);
					End

					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End

			Case 3:  //ANCHO MAXIMO
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.ancho_max);

				Repeat
					If (KEYRIGHT AND opciones.ancho_max<rango_ancho)
						opciones.ancho_max++;
					End

					If (KEYLEFT AND opciones.ancho_max>opciones.ancho_min)
						opciones.ancho_max--;
					End

					If (KEYAIM)
						rand_seed(rand(0,16000)+get_timer());
						opciones.ancho_max=rand(opciones.ancho_min,rango_ancho);
					End

					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End

			Case 4:  //ALTURA MINIMA
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.altura_min);

				Repeat
					If (KEYRIGHT AND opciones.altura_min<opciones.altura_max)
						opciones.altura_min++;
					End

					If (KEYLEFT AND opciones.altura_min>1)
						opciones.altura_min--;
					End

					If (KEYAIM)
						rand_seed(rand(0,16000)+get_timer());
						opciones.altura_min=rand(1,opciones.altura_max);
					End

					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End

			Case 5:  //ALTURA MAXIMA
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.altura_max);

				Repeat
					If (KEYRIGHT AND opciones.altura_max<rango_alto)
						opciones.altura_max++;
					End

					If (KEYLEFT AND opciones.altura_max>opciones.altura_min)
						opciones.altura_max--;
					End

					If (KEYAIM)
						rand_seed(rand(0,16000)+get_timer());
						opciones.altura_max=rand(opciones.altura_min,rango_alto);
					End

					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End
		End

		If (KEYUP)
			If (cursor!=0)
				cursor--;
			Else
				cursor=5;
			End
		End

		If (KEYDOWN)
			If (cursor!=5)
				cursor++;
			Else
				cursor=0;
			End
		End

		If (KEYSHOOT)
			BREAK;
		End

		delete_text(idtexto);
		delete_text(idtextovar);
	End

	delete_text(idtexto);
	delete_text(idtextovar);
	
	signal(son,s_kill);
	signal(ALL_PROCESS,s_wakeup);
	map_generation();

End



/*
*PROCESS options_controller()
*
*Es llamado por el proceso options_menu() una vez que este ha dormido al resto de procesos. Llama a la función controller_update() en
*un bucle (porque Main2() ha quedado suspendido) para que options_menu() pueda leer input de la tabla global controller[6] .
*Hace algunas alteraciones a la tabla tras la llamada de controller_update() para usar reglas de input propias del menú de 
*opciones. Es terminado via señal de options_menu() . Se ejecuta con una prioridad superior a este una vez llamado.  
*/

Process options_controller()

Private

	bool direction_helper[1];  //0=SIGNAL,1=PREVIOUS SIGNAL
	int which;

Begin

	priority=father.priority-1;

	Loop
		controller_update();

		If (KEYUP OR KEYDOWN OR NOLRKEY==0)  //si se pulsa alguna tecla de direccion
			direction_helper[0]=1;           //se manda una señal

			If (KEYUP)  //segun la tecla que se haya pulsado 'which' recibe un valor
				which=3;
			End

			If (KEYDOWN)
				which=4;
			End

			If (KEYLEFT)  //las laterales prevalecen ('which' se sobreescribe)
				which=1;
			End

			If (KEYRIGHT)
				which=2;
			End	  //(se podría poner que si NOLRKEY==0, KEYUP=0 y KEYDOWN=0 pero no parece hacer falta

			If (direction_helper[0]==1 AND direction_helper[1]==0)  //si es el instante en que hay señal (y antes no habia)
				controller[which]=1;                                //se activa la direccion apretada
			Else
				For (which=1;which<5;which++)                       //de lo contrario se ponen todas las direcciones a cero
					controller[which]=0;
				End
			End
		Else                                                        //si no se pulsa ninguna tecla de direccion
			direction_helper[0]=0;                                  //la señal vale 0
		End

		direction_helper[1]=direction_helper[0];                    //finalmente, la señal anterior se actualiza antes de volver a hacer el bucle

		If (key(_esc))
			let_me_alone();
			exit();
		End

		FRAME;
	End

End



/*
*PROCESS pause_menu()
*
*Proceso que permite pausar (congelar) el resto de procesos, pausando así el juego. Lo crea el proceso Main2() al empezar una partida.
*También permite resetear el juego desde el estado de pausa.
*/

Process pause_menu()

Private

	bool p_pulsado;

End

Begin
	
	z=-1;
	x=screenx/2;
	y=screeny/2;

	Loop
		If (key(_p) AND p_pulsado==0);
			p_pulsado=1;
			graph=graficos[3];
			signal(ALL_PROCESS,s_freeze_force);

			Loop
				If (p_pulsado==1 AND !key(_p))
					p_pulsado=0;
				End

				If (key(_r))
					reset_game();
				End

				If (key(_p) AND p_pulsado==0)
					p_pulsado=1;
					graph=0;
					signal(ALL_PROCESS,s_wakeup);  //los procesos que tengan que volver a congelarse lo hacen en ANTI-DESCONGELANTE
					BREAK;
				End

				If (key(_esc))
					let_me_alone();
					exit();

					BREAK;
				End

				FRAME;
			End
		End

		If (p_pulsado==1 and !key(_p))
			p_pulsado=0;
		End

		FRAME;
	End

End
					


/*PROCESS bloque(int i, int j)
*
*Piezas de las que está formada el mapa. Los parámetros int i e int j indican su posición en una rejilla bidimensional. La id de cada uno 
*de los procesos bloque(i,j) activos está guardada en la casilla correspondiente de la tabla grid[i][j]. Los procesos hitbox(int pl) y
*banana(...) usan la información de esta tabla para interactuar con los bloques. 
*
*Al salir comprueba si su posición es la del bloque más bajo de la columna de spawn de cada jugador (definido en el primer campo de la tabla
*global spawn[2]) y si es así llama al proceso bloque_respawn(i,j) .
*
*También guarda en los punteros públicos east,west,north y south la dirección a las posiciones de la rejilla colindantes. Esto de momento
*no se usa para nada, pero lo hice porque creo que puede ser muy util al poner gráficos y para practicar y entender mejor los punteros.
*/

Process bloque(int i, int j)  //la cabecera se repite tal y como se ha declarado en la sección DECLARE

Begin

	graph=graficos[0];

	x=tilesize*i+tilesize/2;
	y=tilesize*j+tilesize/2;

	If (i>0)
		west=&grid[i-1][j];
	Else
		west=&int_zero_int;  //apunta a un int que vale 0, asi no hay errores
	End

	If (i<MAX_TILES_X-1)
		east=&grid[i+1][j];
	Else
		east=&int_zero_int;
	End

	If (j>0)
		north=&grid[i][j-1];
	Else
		north=&int_zero_int;
	End

	If (j<MAX_TILES_Y-1)
		south=&grid[i][j+1];
	Else
		south=&int_zero_int;
	End

	signal_action(s_wakeup,S_IGN);
	signal(id,s_freeze);

	FRAME;

	/*Loop
		If (key(_i))
			say("i=="+i+" j=="+j+" id:"+id+" N:"+*north+" E:"+*east+" S:"+*south+" W:"+*west);
		End

		FRAME;	
	End*/

OnExit  //hace que si es el ultimo bloque en la columna de spawn, se regenere

	If (exists(type Hitbox) AND j==MAX_TILES_Y-1 AND (i==spawn[0]-1 OR i==MAX_TILES_X-spawn[0]))
		bloque_respawn(i,j);
	End

End



/*
*PROCESS bloque_respawn(int i, int j)
*
*Genera el bloque más bajo de la columna de spawn de cada jugador si este es destruido. Espera a que un jugador anote un punto o se cambie
*de turno una vez destruido el bloque.
*/

Process bloque_respawn(int i, int j)

Private

	int turno_actual;

End

Begin

	turno_actual=turn;

	Loop
		If (exists(type arbitro) OR turn!=turno_actual)
			grid[i][j]=bloque(i,j);
			BREAK;
		End

		FRAME;
	End

End



#ifdef DEBUG_INFO


/*PROCESS hitbox_debug_info(int pl, ... pointers)
*
*Imprime en pantalla valores de variables con las que el proceso hitbox(int pl) que lo ha llamado está trabajando, durante su turno.
*Es llamado por cada proceso hitbox(int pl) en su creación y cada vez que es su turno. Termina cuando ya no es el turno del hitbox
*que lo llamó. Los datos se dibujan en un lado diferente de la pantalla para cada jugador. 
*Recibe como parámetros punteros a diferentes variables públicas de hitbox para luego pasarselos a la función write_var .
*/

Process hitbox_debug_info(int pl, int pointer going, float pointer dx, int pointer direction, int pointer jetpack, float pointer dy, int pointer on_ground, int pointer posicion_i, int pointer posicion_j, int pointer xlec, int pointer ylec, int pointer floor_check)

Private

	int id_textos_debug[21];
	string textos_debug[10]="going","dx","direction","jetpack","dy","on_ground","floor_check","posicion_i","posicion_j","xlec","ylec";
	int i;
	int margen_w;   //el valor que se le pasará como x a la función write
	int margen_wv;  //el valor que se le pasará como x a la función write_var
	int align_w;    //el valor que se le pasará como alineación a la función write
	int align_wv;   //el valor que se le pasará como alineación a la función write_var

End

Begin

	say("empieza hitbox_debug_info para pl=="+pl);

	Switch (pl)
		Case 1:
			margen_w=screenx-75;
			align_w=0;
			margen_wv=screenx-80;
			align_wv=2;
		End

		Case 2:
			margen_w=75;
			align_w=2;
			margen_wv=80;
			align_wv=0;
		End
	End
#ifdef PRECISE
	signal_action(s_freeze_tree,S_IGN);
#endif
	For (i=0;i<7;i++)  //escribe hasta floor_check en un lado
		id_textos_debug[i]=write(0,margen_w,i*10,align_w,textos_debug[i]);
	End

	id_textos_debug[11]=write_var(0,margen_wv,0,align_wv,*going);  //y sus valores tambien
	id_textos_debug[12]=write_var(0,margen_wv,10,align_wv,*dx);
	id_textos_debug[13]=write_var(0,margen_wv,20,align_wv,*direction);
	id_textos_debug[14]=write_var(0,margen_wv,30,align_wv,*jetpack);
	id_textos_debug[15]=write_var(0,margen_wv,40,align_wv,*dy);
	id_textos_debug[16]=write_var(0,margen_wv,50,align_wv,*on_ground);
	id_textos_debug[21]=write_var(0,margen_wv,60,align_wv,*floor_check);

	Switch (pl)  //cambia los margenes y las alineaciones para escribir el resto de la info al otro lado
		Case 2:
			margen_w=screenx-75;
			align_w=0;
			margen_wv=screenx-80;
			align_wv=2;
		End

		Case 1:
			margen_w=75;
			align_w=2;
			margen_wv=80;
			align_wv=0;
		End
	End

	For (i;i<11;i++)  //escribe lo que falta al otro lado
		id_textos_debug[i]=write(0,margen_w,(i-7)*10,align_w,textos_debug[i]);
	End

	id_textos_debug[17]=write_var(0,margen_wv,0,align_wv,*posicion_i);  //y sus valores tambien
	id_textos_debug[18]=write_var(0,margen_wv,10,align_wv,*posicion_j);
	id_textos_debug[19]=write_var(0,margen_wv,20,align_wv,*xlec);
	id_textos_debug[20]=write_var(0,margen_wv,30,align_wv,*ylec);

	Loop
		If (turn_promotion!=pl)
			
			BREAK;
		End

		FRAME;
	End

OnExit

	For (i=0;i<22;i++)
		delete_text(id_textos_debug[i]);
	End

End
#endif



/*PROCESS hitbox(int pl)
*
*Representa la posición de cada gorila en el mapa. Permite a cada jugador conrolarlo en su turno. La posición (y el movimiento) se calcula
*Respecto a su posición en la rejilla. Finalmente se convierte a coordenadas en pantalla.
*
*La versión PRECISE llama a FRAME; antes que la otra porque sus colisiones se calculan mediante los procesos mover_x() y mover_y() . La 
*otra versión calcula las colisiones desde este mismo proceso, después de aplicar el movimiento y antes de calcular si ha cambiado de casilla.
*Las colisiones se calculan accediendo a los datos de grid[i][j], sin usar la funcion collision() . Ambas versiones establecen los límites
*de la pantalla dentro de los que cada hitbox se puede mover desde este proceso. También tienen un mecanismo que permite saber si el hitbox
*está tocando dos casillas (horizontales)(ya que el hitbox es un cuadrado, no un solo punto).
*
*Este proceso lo crea Main2() al empezar una partida o arbitro(int pl) al anotarle un tanto al otro jugador, si no se ha llegado al máximo de puntos.
*/

Process hitbox(int pl)

#ifndef PRECISE
Private

	//int posicion_i;
	//int posicion_j;
	//int xlec;
	//int ylec;         //todos estos ahora son publicos asi las bananas pueden leer sus datos
	float dx;
	float resta_dx;
	float dy;
	float resta_dy;
	int idx;
	int idy;
	int going;
	int direction;
	int direction_y;
	int floor_check;  //vale 0 si hitbox está sobre un bloque, 1 si son dos con el de adelante, -1 si son dos con el de atras
	int jetpack;
	int on_ground;
	int left_limit;   //sirve para limiter, que aquí en vez de ser un proceso separado lo calcula el proceso hitbox
	int right_limit;  //idem

End
#endif

Begin

	graph=graficos[1];
	player=pl;

	Switch (pl)
		Case 1:
			posicion_i=spawn[0]-1;
			left_limit=0;
			right_limit=MAX_TILES_X/2-1;
		End

		Case 2:
			posicion_i=MAX_TILES_X-spawn[0];
			left_limit=MAX_TILES_X/2;
			right_limit=MAX_TILES_X-1;
		End
	End

	posicion_j=spawn[pl]-1;
	xlec=tilesize/2;
	ylec=tilesize-H_HITBOXSIZE;

#ifdef DEBUG_INFO
	hitbox_debug_info(pl,&going,&dx,&direction,&jetpack,&dy,&on_ground,&posicion_i,&posicion_j,&xlec,&ylec,&floor_check);
#endif

#ifdef PRECISE
	signal_action(s_freeze_tree,S_IGN);

	mover_x();
	mover_y();
#endif

	x=tilesize*posicion_i+xlec;	//si no se inicializan x e y el jugador 2 no coge bien su posicion (pk entra en el loop que solo hace FRAME;)
	y=tilesize*posicion_j+ylec;

	Loop
		//AIM (se pone aquí sino cuando se sale del estado shooting enseguida se vuelve a entrar porque se vuelve a comprobar KEYAIM...
		If (KEYAIM AND on_ground==1 AND shooting==0)  //... en cambio aquí ya ha pasado por el FRAME; asi que KEYAIM vale 0)
			aim_process(pl);
			shooting=1;

			going=0;
			dx=0;
#ifndef PRECISE
			idx=0;
#endif
		End

		//STATUS CONTROL
		If (turn_promotion==pl AND turn!=pl AND on_ground==1)
			turn=pl;
			say("status changed from hitbox pl=="+pl+" , turn=="+turn);
		End

		If (shooting==1 OR turn_promotion!=pl)
			grid[posicion_i][posicion_j]=1;              //las casillas de grid en las que se encuentre hitbox tendran el valor 1... 
			grid[posicion_i+floor_check][posicion_j]=1;  //...en modo shooting o en el turno del oponente

			say("hitbox player "+pl+" entrando en modo freeze.");
			say("grid["+posicion_i+"]["+posicion_j+"]=="+grid[posicion_i][posicion_j]+" grid["+(posicion_i+floor_check)+"]["+posicion_j+"]=="+grid[posicion_i+floor_check][posicion_j]);

#ifdef PRECISE
			signal(id,s_freeze_tree);
#endif
			Repeat
				FRAME;
			Until (shooting==0 AND turn_promotion==pl)
	
			grid[posicion_i][posicion_j]=0;
			grid[posicion_i+floor_check][posicion_j]=0;

			say("hitbox player "+pl+" saliendo de modo freeze.");
			say("grid["+posicion_i+"]["+posicion_j+"]=="+grid[posicion_i][posicion_j]+" grid["+(posicion_i+floor_check)+"]["+posicion_j+"]=="+grid[posicion_i+floor_check][posicion_j]);

#ifdef PRECISE
			signal(id,s_wakeup_tree);
#endif
#ifdef DEBUG_INFO
			hitbox_debug_info(pl,&going,&dx,&direction,&jetpack,&dy,&on_ground,&posicion_i,&posicion_j,&xlec,&ylec,&floor_check);
#endif
		End

		//MOVEMENT CONTROL
		If (turn==pl)
			If (KEYLEFT)
				going=-1;
			End

			If (KEYRIGHT)
				going=1;
			End

			If (KEYUP AND jetpack>0)
				jetpack=jetpack-1;
				dy=dy-jet_fuerza;
			End

			If (NOLRKEY)
				If (dx>0)
					going=-1;
				
					If (dx=<accel)
						going=0;
						dx=0;
					End
				End

				If (dx<0)
					going=1;
		
					If (dx=>-accel)
						going=0;
						dx=0;
					End
				End
			End
		End
		
		//CALCULO DE MOVIMIENTO
		dx=dx+accel*going;
		
		If (on_ground==0)
			dy=dy+gravedad;
		End

		//CALCULO DE DIRECCION (FLOAT)
		If (dx>0)
			direction=1;
		End
		If (dx<0)
			direction=-1;
		End
		If (dx==0)
			direction=0;
		End
/*
		If (dy>0)
			direction_y=1;
		End
		If (dy<0)
			direction_y=-1;
		End
		If (dy=0)
			direction_y=0;
		End
*/
		//FLOAT TO INT
		If (dx!=0)
			idx=dx+resta_dx;
			resta_dx=(resta_dx+dx)-idx;
		Else
			idx=0;
		End

	
		If (dy!=0)
			idy=dy+resta_dy;
			resta_dy=(resta_dy+dy)-idy;	
		Else				
			idy=0;
		End

		//CALCULO DE DIRECCION (INT)
/*		If (idx>0)
			direction=1;
		End
		If (idx<0)
			direction=-1;
		End
		If (idx=0)
			direction=0;
		End
*/
		If (idy>0)
			direction_y=1;
		End
		If (idy<0)
			direction_y=-1;
		End
		If (idy==0)
			direction_y=0;
		End

#ifndef PRECISE
		//APLICACION DE MOVIMIENTO
		xlec=xlec+idx;
		ylec=ylec+idy;

		//COLISIONES HORIZONTALES
		If (going!=0)	
			Switch (direction)
				Case 1:
					If (grid[posicion_i+1][posicion_j]!=0 AND xlec+H_HITBOXSIZE>tilesize-1 AND posicion_j>0)
						dx=0;
						going=0;
						xlec=tilesize-H_HITBOXSIZE;
						say("horizontal case 1");
					End
				End

				Case -1:
					If (grid[posicion_i-1][posicion_j]!=0 AND xlec-H_HITBOXSIZE<1 AND posicion_j>0)	
						dx=0;
						going=0;
						xlec=H_HITBOXSIZE;
						say("horizontal case -1");
					End
				End
			End
		End

		//COLISIONES VERTICALES
		Switch (direction_y)
			Case 1:
				If (ylec+H_HITBOXSIZE>tilesize-1 AND posicion_j>0 AND (grid[posicion_i][posicion_j+1]!=0 OR grid[posicion_i+floor_check][posicion_j+1]!=0))	//la comprobacion de posicion_j>0 es porque sino la parte superior de la pantalla cuenta como solida al estar fuera del grid
					on_ground=1;
					dy=0;
					jetpack=jet_max_fuel;
					ylec=tilesize-H_HITBOXSIZE;
					say("vertical case 1");
				End
			End

			Case -1:
				If (ylec-H_HITBOXSIZE<1 AND posicion_j>0 AND (grid[posicion_i][posicion_j-1]!=0 OR grid[posicion_i+floor_check][posicion_j-1]!=0))
					dy=0;
					ylec=H_HITBOXSIZE;
					say("vertical case -1");
				End

				If (on_ground==1)  //deberia comprobar esto o poner on_ground a 0 directamente??
					on_ground=0;
				End
			End

			Case 0:
				If (grid[posicion_i][posicion_j+1]==0 AND grid[posicion_i+floor_check][posicion_j+1]==0 AND on_ground==1)
					on_ground=0;
					say("vertical case 0");
				End
			End
		End
#endif
#ifdef PRECISE
		FRAME;
#endif
		//CALCULO DE CASILLA ACTUAL
		If (xlec>tilesize-1)  //estas lineas cambian posicion_i y posicion_j para que xlec y ylec sean relativas a ellas
			posicion_i=posicion_i+1;
			xlec=xlec-tilesize;
		End

		If (xlec<0)  //antes se hacia arriba, justo despues de sumar dx a xlec y la gravedad a ylec
			posicion_i=posicion_i-1;
			xlec=xlec+tilesize;
		End

		If (ylec>tilesize-1)  //pero eso hace que pueda saltarse algunas posiciones al calcular las colisiones
			posicion_j=posicion_j+1;
			ylec=ylec-tilesize;
		End

		If (ylec<0)  //asi que la manera correcta de resolverlo es aqui, antes de decir la posicion final de x e y.
			posicion_j=posicion_j-1;
			ylec=ylec+tilesize;
		End

		//LIMITER
		If (posicion_i=<left_limit AND xlec<H_HITBOXSIZE)
			going=0;
			dx=0;
#ifndef PRECISE
			idx=0;
#endif
			posicion_i=left_limit;
			xlec=H_HITBOXSIZE;
		End

		If (posicion_i>=right_limit AND xlec>tilesize-H_HITBOXSIZE)
			going=0;
			dx=0;
#ifndef PRECISE
			idx=0;
#endif
			posicion_i=right_limit;
			xlec=tilesize-H_HITBOXSIZE;
		End

		If (posicion_j==MAX_TILES_Y)  //caída por abajo
			If (pl==1)
				pl=2;
			Else
				pl=1;
			End

			signal_action(s_kill_tree,S_IGN);
			signal(id,s_kill_tree);

			arbitro(pl);

			BREAK;
		End

		//FLOOR CHECK
		Switch (xlec)  //esto se hace para ver si pisa dos tiles (funciona bien porque el hitbox es mas pequeño que el tile)
			Case 0..(H_HITBOXSIZE-2):
				floor_check=-1;
			End
		
			Case (tilesize-H_HITBOXSIZE+2)..tilesize:
				floor_check=1;
			End
			
			Default:
				floor_check=0;
			End
		End  //se ha puesto aqui porque así se calculan los rangos con xlec ya resuelto. puede que vaya con un frame de retraso en algunos casos pero no lo parece, aun asi, hacer pruebas

		//POSICION FINAL
		x=tilesize*posicion_i+xlec;
		y=tilesize*posicion_j+ylec;
	
#ifdef PIXEL_CHECK
	#if PIXEL_CHECK!=3
		pixel_checker(xlec);
	#endif
#endif
#ifndef PRECISE
		FRAME;
#endif
	End

OnExit  //hace que si lo mata la banana vuelva a poner a 0 las casillas que puso a 1 cuando entro en modo shooting

	grid[posicion_i][posicion_j]=0;
	grid[posicion_i+floor_check][posicion_j]=0;
	say("hitbox exiting, pl=="+pl);

End



/*
*PROCESS aim_process(int who)
*
*Abre un menú en la partida que permite disparar una banana estableciendo la fuerza y el angulo que darle a esta. Este proceso puede ser
*llamado por uno de los dos procesos hitbox() cuando es su turno y se encuentra en el suelo. Del menú se puede salir para volver a la partida
*(haciendo que a través de hitbox() se salga del modo shooting y recuperando el conrol sobre este) o para disparar la banana.
*/

Process aim_process(who)

Private

	int angulo;
	float fuerza=min_fuerza;
	int textos[3];
	Hitbox hb;

End

Begin

	textos[0]=write(0,father.x,father.y-60,4,"ANGULO");
	textos[1]=write_var(0,father.x,father.y-50,4,angulo);
	textos[2]=write(0,father.x,father.y-40,4,"FUERZA");
	textos[3]=write_var(0,father.x,father.y-30,4,fuerza);
#ifdef PRECISE
	signal_action(id,s_freeze_tree,S_IGN);
#endif
	hb=father.id;

	FRAME;  //Necesita este FRAME; para no leer en su primer bucle la misma señal de KEYAIM que lo invocó
	
	Loop
		If (KEYAIM)
			shooting=0;

			BREAK;
		End

		If (KEYUP AND angulo<max_angulo)
			angulo++;
		End

		If (KEYDOWN AND angulo>min_angulo)
			angulo--;
		End

		Switch (who)
			Case 1:
				If (KEYRIGHT AND fuerza<max_fuerza)
					fuerza++;
				End

				If (KEYLEFT AND fuerza>min_fuerza)
					fuerza--;
				End
			End

			Case 2:
				If (KEYRIGHT AND fuerza>min_fuerza)
					fuerza--;
				End

				If (KEYLEFT AND fuerza<max_fuerza)
					fuerza++;
				End
			End
		End

		If (KEYSHOOT)
			banana(hb,angulo,fuerza,who);

			BREAK;
		End

		FRAME;
	End

OnExit

	For (angulo=0;angulo<4;angulo++)  //"angulo" se usa aquí como si fuera un contador "i", esto lo que hace es borrar textos...
		delete_text(textos[angulo]);  //...es decir, no tiene nada que ver con los angulos.
	End

End



/*
*PROCESS banana(Hitbox hb, int angulo, float fuerza, int who)
*
*Proceso que hace un recorrido parabólico determinado por sus parámetros de entrada y que elimina procesos bloque() o hitbox() al colisionar 
*con ellos. Según el parámetro int who (que indica que jugador la dispara) transforma el valor de int angulo al formato que entiende Bennu,
*teniendo en cuenta hacia a donde apunta el gorila. El valor float fuerza, que es pasado como porcentaje, se multiplica por una constante y
*por el seno y coseno de int angulo para establecer las variables de avance (float bdx; float bdy; ). Luego entra en un bucle en el que se 
*altera sucesivamente su posición teninendo en cuenta toda esta información y se comprueba si hay colisiones. Su movimiento, al igual que 
*hitbox() , también se hace respecto a su posición en la rejilla y antes de comprobar las colisiones gráficas se lee la tabla grid[i][j] .
*
*Si este proceso colisiona contra un proceso bloque() lo destruye y pasa el turno, si colisiona contra el proceso hitbox() del otro 
*jugador lo elimina y llama al proceso arbitro(int pl) pasandole como parámetro int who (jugador que disparó). Si choca contra el 
*mismo hitbox() del jugador que ha disparado, se sale del modo shooting y se pasa de turno. En todos los casos el proceso banana() 
*se destruye (así como si sale por los lados o por debajo).
*/

Process banana(Hitbox hb, int angulo, float fuerza, int who);

Private

	int bposicion_i;
	int bposicion_j;
	int bxlec;
	int bylec;
	float bdx;
	float resta_bdx;
	float bdy;
	float resta_bdy;
	int bidx;
	int bidy;
	int bgoing;
	int collider;
	int grid_var;

End

Begin

	graph=graficos[2];

	Switch (who)
		Case 1:
			angulo=angulo*1000;
			bxlec=hb.xlec+H_HITBOXSIZE+2;
			bgoing=1;
		End

		Case 2: 
			angulo=180000-angulo*1000;
			bxlec=hb.xlec-H_HITBOXSIZE-2;
			bgoing=-1;
		End
	End

	bylec=hb.ylec-H_HITBOXSIZE-2;

	bposicion_i=hb.posicion_i;
	bposicion_j=hb.posicion_j;

	//CALCULO DE CASILLA 
	If (bxlec>tilesize-1)
		bposicion_i=bposicion_i+1;
		bxlec=bxlec-tilesize;
	End

	If (bxlec<0)
		bposicion_i=bposicion_i-1;
		bxlec=bxlec+tilesize;
	End

	If (bylec>tilesize-1)
		bposicion_j=bposicion_j+1;
		bylec=bylec-tilesize;
	End

	If (bylec<0)
		bposicion_j=bposicion_j-1;
		bylec=bylec+tilesize;
	End

	fuerza=fuerza*relacion_fuerza;
	
	bdx=fuerza*cos(angulo);
	bdy=fuerza*sin(angulo);
		
	say("cos=="+cos(angulo)+" sin=="+sin(angulo));
	say("angulo=="+angulo+" fuerza=="+fuerza+" x=="+x+" y=="+y+" bgoing=="+bgoing+" bdx=="+bdx+" bdy=="+bdy);
	
	x=tilesize*bposicion_i+bxlec;
	y=tilesize*bposicion_j+bylec;	

	FRAME;

	Loop	
		//MOVIMIENTO	
		bidx=bdx+resta_bdx;
		resta_bdx=(resta_bdx+bdx)-bidx;

		bxlec=bxlec+bidx;


		bdy=bdy-gravedad;
		bidy=bdy+resta_bdy;
		resta_bdy=(resta_bdy+bdy)-bidy;	
		
		bylec=bylec-bidy;
		
		//CALCULO DE CASILLA 
		If (bxlec>tilesize-1)	
			bposicion_i=bposicion_i+1;
			bxlec=bxlec-tilesize;
		End
	
		If (bxlec<0)		
			bposicion_i=bposicion_i-1;
			bxlec=bxlec+tilesize;
		End
	
		If (bylec>tilesize-1)	
			bposicion_j=bposicion_j+1;
			bylec=bylec-tilesize;
		End

		If (bylec<0)		
			bposicion_j=bposicion_j-1;
			bylec=bylec+tilesize;
		End

		//COMPROBACIÓN DE SALIDA DE PANTALLA
		If (bposicion_i>MAX_TILES_X OR bposicion_i<0 OR bposicion_j>MAX_TILES_Y)
			shooting=0;
			turn_change();

			BREAK;
		End

		x=tilesize*bposicion_i+bxlec;  //si no se establecen x e y antes de usar collision no hace bien la colision
		y=tilesize*bposicion_j+bylec;		

		//COMPROBACION DE COLISIONES
		If (grid[bposicion_i][bposicion_j]!=0 AND bposicion_j>-1)  //se comprueba que bposicion_j>-1 porque sino rompe el mapa por abajo	
			If (grid[bposicion_i][bposicion_j]==1)
		
				say("banana rozando gorila! bpi=="+bposicion_i+" bpj=="+bposicion_j+" grid_var=="+grid_var);

				If (collider=collision(type hitbox))  //si colisiona con un gorila
					hb=collider;  //hb recoge la id de collider para comprobar contra que gorila ha chocado la banana

					If (hb.player!=who)
						signal(collider,s_kill_tree);  //es s_kill_tree para precise, el otro basta con s_kill
						arbitro(who);
	
						say("banana explotada contra gorila!");
						say("bposicion_i=="+bposicion_i+" bposicion_j=="+bposicion_j+" bidx=="+bidx+" bidy=="+bidy);

						BREAK;
					Else  //de lo contrario se pasa el turno
						shooting=0;
						turn_change();

						BREAK;
					End
				End
			Else
				signal(grid[bposicion_i][bposicion_j],s_kill);
				grid[bposicion_i][bposicion_j]=0;
				shooting=0;
				turn_change();

				say("banana explotada contra bloque");
				say("bposicion_i=="+bposicion_i+" bposicion_j=="+bposicion_j+" bidx=="+bidx+" bidy=="+bidy);

				BREAK;
			End
		End

#ifdef PIXEL_CHECK
	#if PIXEL_CHECK!=2
		pixel_checker(x);
	#endif
#endif

		FRAME;
	End

End



/*
*PROCESS arbitro(int pl)
*
*Anota un punto al jugador que se corresponda con el parámetro int pl que recibe. Si la puntuación del jugador es la necesaria para ganar
*la partida (establecida en la estructura global options), llama al proceso game_over(pl). Si no, muestra la puntuación durante un tiempo
*determinado, cambia el turno y crea un proceso hitbox(int pl) para el jugador que acaba de ser eliminado.
*/

Process arbitro(int pl)

Private

	int mensaje[1];
	int muestra_mensaje;

End

Begin

	puntos[pl]=puntos[pl]+1;
	
	If (puntos[pl]==opciones.puntos)
		game_over(pl);
		signal(id,s_kill);
	End

	mensaje[0]=write(0,screenx/2,70,7,"PUNTO PARA EL JUGADOR "+pl);  //esto tendrá que ser distinto si hago de pl un bool
	mensaje[1]=write(0,screenx/2,80,1,"JUGADOR 1: "+puntos[1]+" JUGADOR 2: "+puntos[2]);

	While (muestra_mensaje<tiempo_mensaje)
		muestra_mensaje++;
	
		FRAME;
	End

	If (pl==1)
		pl=2;
	Else
		pl=1;
	End

	hitbox(pl);
	shooting=0;
	
	If (turn_promotion==turn)  //evita que un jugador repita turno al otro morir por caer al suelo tras romperse un bloque debajo suyo
		turn_change();
	End

OnExit

	If (mensaje[0]!=0)
		delete_text(mensaje[0]);
	End
	If (mensaje[1]!=0)
		delete_text(mensaje[1]);
	End

End



/*
*PROCESS game_over(int pl)
*
*Muestra un mensaje anunciando que jugador es el ganador de la partida (pasado como parámetro int pl por el proceso arbitro() ) y deja el
*juego en este estado hasta que se pulsa una tecla que llame a la función reset_game() .
*/

Process game_over(int pl)

Private

	int ganador;

End

Begin

	If (exists(type pause_menu))
		signal(type pause_menu,s_kill);
	End

	ganador=write(0,screenx/2,70,4,"GANA EL JUGADOR "+pl);

	Loop
		If (KEYSHOOT==1 OR KEYAIM==1 OR key(_r))
			reset_game();
			BREAK;
		End

		FRAME;
	End

OnExit

	delete_text(ganador);

End	



#ifdef PRECISE


/*PROCESS mover_x()
*
*Se encarga del movimiento horizontal y colisiones laterales de un hitbox. Su codigo se ejecuta antes del cálculo de posición en casilla del hitbox.
*Un bucle se ejecuta tantas veces como haya de desplazarse horizontalmente el hitbox en ese frame. Este bucle mueve el hitbox en el sentido horizontal
*que indique la variable pública de hitbox int direction. Luego, comprueba si hay colisiones entre hitbox() y bloque() teniendo en cuenta las 
*posiciones de ambos. Si hay colisión detiene a hitbox().
*/

Process mover_x()

Private

	int framer;
	int i;
	Hitbox hb;
	bool negador;

End

Begin

	say("mover_x priority=="+priority+" id=="+id+" father.id=="+father.id);
	hb=father.id;

	Loop
		If (hb.going!=0)
			If (hb.idx==0)	
				framer=1;
				negador=0;
			Else
				framer=abs(hb.idx);
				negador=1;
			End

			For (i=0;i<framer;i++)
				hb.xlec=hb.xlec+hb.direction*negador;
		
				Switch (hb.direction)
					Case 1:
						If (grid[hb.posicion_i+1][hb.posicion_j]!=0 AND hb.xlec+H_HITBOXSIZE>tilesize AND hb.posicion_j>-1)
							hb.dx=0;
							hb.going=0;
							hb.xlec=tilesize-H_HITBOXSIZE;
								say("horizontal case 1");
							BREAK;
						End
					End

					Case -1:
						If (grid[hb.posicion_i-1][hb.posicion_j]!=0 AND hb.xlec-H_HITBOXSIZE<0 AND hb.posicion_j>-1)  //La comprobacion xlec+H_HITBOXSIZE>tilesize-1 en el case 1 y la comprobacion xlec-H_HITBOXSIZE<1 en el case -1 hace que entre en alguno de los cases en cada iteracion //la comprobacion posicion_j>-1 es para que funcione bien por encima del grid
							hb.dx=0;
							hb.going=0;
							hb.xlec=H_HITBOXSIZE;
								say("horizontal case -1");
							BREAK;
						End
					End
				End
			End
		End

		FRAME;

		//ANTI-DESCONGELANTE
		If ((turn_promotion!=hb.player OR shooting==1) AND get_status(id)==2)
			signal(id,s_freeze);
		End
	End
End



/*PROCESS mover_y()
*
*Se encarga del movimiento vertical y colisiones verticales de un hitbox. Su codigo se ejecuta antes del cálculo de posición en casilla del hitbox.
*Un bucle se ejecuta tantas veces como haya de desplazarse verticalmente el hitbox en ese frame. Este bucle mueve el hitbox en el sentido vertical
*que indique la variable pública de hitbox int direction. Luego, comprueba si hay colisiones entre hitbox() y bloque() teniendo en cuenta las 
*posiciones de ambos. Si hay colisión detiene a hitbox(). También se encarga de detectar si hitbox reposa sobre algun bloque() (variable on_gound).
*/

Process mover_y()

Private

	int framer;
	int i;
	Hitbox hb;
	bool negador;

End

Begin

	say("mover_y priority=="+priority);
	hb=father.id;

	Loop
		If (hb.idy==0)	
			framer=1;
			negador=0;
		Else
			framer=abs(hb.idy);
			negador=1;
		End

		For (i=0;i<framer;i++)
			hb.ylec=hb.ylec+hb.direction_y*negador;
		
			Switch (hb.direction_y)
				Case 1:
					If (hb.ylec+H_HITBOXSIZE>tilesize-1 AND hb.posicion_j>0 AND (grid[hb.posicion_i][hb.posicion_j+1]!=0 OR grid[hb.posicion_i+hb.floor_check][hb.posicion_j+1]!=0))  //la comprobacion de posicion_j>0 es porque sino la parte superior de la pantalla cuenta como solida al estar fuera del grid
						hb.on_ground=1;
						hb.dy=0;
						hb.jetpack=jet_max_fuel;
						hb.ylec=tilesize-H_HITBOXSIZE;
						say("vertical case 1");
						BREAK;
					End
				End

				Case -1:
					If (hb.on_ground==1)  //deberia comprobar esto o poner on_ground a 0 directamente??
						hb.on_ground=0;
					End

					If (hb.ylec-H_HITBOXSIZE<0 AND hb.posicion_j>0 AND (grid[hb.posicion_i][hb.posicion_j-1]!=0 OR grid[hb.posicion_i+hb.floor_check][hb.posicion_j-1]!=0))
						hb.dy=0;
						hb.ylec=H_HITBOXSIZE;
						say("vertical case -1");
						BREAK;
					End
				End

				Case 0:
					If (grid[hb.posicion_i][hb.posicion_j+1]==0 AND grid[hb.posicion_i+hb.floor_check][hb.posicion_j+1]==0 AND hb.on_ground==1)
						hb.on_ground=0;
						say("vertical case 0");
					End
				End
			End
		End

		FRAME;

		//ANTI-DESCONGELANTE
		If ((turn_promotion!=hb.player OR shooting==1) AND get_status(id)==2)
			signal(id,s_freeze);
		End
	End
End
#endif		


#ifdef PIXEL_CHECK

/*
*FUNCTION pixel_checker(int xxlec)
*
*Recibe la posición x del proceso que le llama (hitbox() o banana() ) como parámetro int xparam. Llama a la función pchecker(i) 
*pasandole como parámetro i=xparam (¿?) que comprueba si el valor es par o impar. Según el resultado pinta en el fondo de la pantalla
*un píxel de un color o de otro. Esto hace que dichos procesos dejen una estela al moverse. Es una función de debug que sirve para 
*comprobar la programación de los movimientos.
*
*/

Function pixel_checker(int xxlec)

Private

	int pcheck;

End

Begin

	pcheck=pchecker(father.x);

	Switch (pcheck)
		Case 0:
			put_pixel(father.x,father.y,rgb(0,255,0));
		End
		Case 1:
			put_pixel(father.x,father.y,rgb(255,50,0));
		End
		Default:
			put_pixel(father.x,father.y,rgb(50,100,150));
		End
	End

End



/*
*FUNCTION int pchecker(i)
*
*Determina si el número pasado como parámetro i es par o impar. Para esto le resta 2, si da 0 es par, si da 1 es impar y si da otro resultado
*se vuelve a llamar a sí misma pasando tal resultado como parámetro i.
*/

Function int pchecker(i)

Begin

	i=i-2;

	Switch (i)
		Case 0:
			return(i);
		End
		Case 1:
			return(i);
		End
		Default:
			i=pchecker(i);
			return(i);
		End
	End

End

#endif

#ifdef DEBUG_INFO

/*PROCESS cursor()
*
*El proceso cursor() es una utilidad de debug que sirve para mostrar la id de un bloque del mapa a elegir por el usuario marcandolo gráficamente
*para reconocerlo en la pantalla. Se ha dejado en el código por méritos y porque puede seguir siendo útil. Usa la tabla global grid[][] para 
*para acceder a los datos del mapa y modificar los gráficos de bloque().
*/

Process cursor()

Private

	int previous_graph;
	int cursor_graph;
	int tile_id;
	int cursor[1];
	int textos[2];

End

Begin

	cursor_graph=map_new(tilesize,tilesize,32);
	map_clear(0,cursor_graph,rgb(255,255,0));
	drawing_map(0,cursor_graph);
	drawing_color(rgb(0,255,128));
	draw_rect(0,0,tilesize-1,tilesize-1);

	say("llamado a cursor!");
	textos[0]=write_var(0,0,10,0,cursor[0]);
	textos[1]=write_var(0,0,20,0,cursor[1]);
	textos[2]=write_var(0,0,40,0,tile_id);

	tile_id=grid[cursor[0]][cursor[1]];

	If (tile_id!=0)
		previous_graph=tile_id.graph;
		tile_id.graph=cursor_graph;
	End

	Loop
		If (scan_code==0)
			FRAME;
		Else
			If (key(_left) AND cursor[0]>0)
				If (tile_id!=0 AND tile_id!=1)  //esto hace que recupere el gráfico anterior al no estar seleccionado por el cursor
					tile_id.graph=previous_graph;
				End

				cursor[0]=cursor[0]-1;	
				tile_id=grid[cursor[0]][cursor[1]];

				If (tile_id!=0 AND tile_id!=1)
					previous_graph=tile_id.graph;
					tile_id.graph=cursor_graph;
				End
			End

			If (key(_right) AND cursor[0]<MAX_TILES_X-1)
				If (tile_id!=0 AND tile_id!=1)  //de la forma que está hecho ahora es rebuscado al tener todos los bloque() el mísmo gráfico
					tile_id.graph=previous_graph;
				End
				
				cursor[0]=cursor[0]+1;
				tile_id=grid[cursor[0]][cursor[1]];

				If (tile_id!=0 AND tile_id!=1)  //pero sirve para cuando bloque() pueda tener un gráfico diferente en cada instáncia
					previous_graph=tile_id.graph;
					tile_id.graph=cursor_graph;
				End
			End

			If (key(_up) AND cursor[1]>0)
				If (tile_id!=0 AND tile_id!=1)
					tile_id.graph=previous_graph;
				End

				cursor[1]=cursor[1]-1;
				tile_id=grid[cursor[0]][cursor[1]];

				If (tile_id!=0 AND tile_id!=1)
					previous_graph=tile_id.graph;
					tile_id.graph=cursor_graph;
				End
			End

			If (key(_down) AND cursor[1]<MAX_TILES_Y-1)
				If (tile_id!=0 AND tile_id!=1)
					tile_id.graph=previous_graph;
				End

				cursor[1]=cursor[1]+1;
				tile_id=grid[cursor[0]][cursor[1]];

				If (tile_id!=0 AND tile_id!=1)
					previous_graph=tile_id.graph;
					tile_id.graph=cursor_graph;
				End
			End

			FRAME;
		End

		If (key(_x))
			If (tile_id!=0)
				tile_id.graph=previous_graph;
			End

			For (tile_id=0;tile_id<3;tile_id++)  //borra los textos escritos, tile_id se usa como contador y parámetro
				delete_text(textos[tile_id]);
			End

			BREAK;
		End
	End

End
#endif