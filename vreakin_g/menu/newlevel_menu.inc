CONST
	_nlm_option_elements = 5;
	_nlm_reactive_elements = 4;
	
	_default_playfield_i = 240;
	_default_playfield_j = 320;
	_default_block_length = 16;
	_default_block_height = 8;
	_default_lower_m_ratio = 0.2;
	
	_nlm_labels_x = 0;
	_nlm_numbers_x = 180;
	_nlm_line_height = 20;
END



//
//	PROCESS New_level_menu( )
//
//	Permite crear un nivel genérico con los parámetros dados. Para ello usa una struct newlevel que es igual en forma al struct
//global level. El menu permite al usuario establecer los valores en newlevel y al pulsar la tecla N estos se graban en level.
//(Al pulsar la tecla D se cargan valores por defecto en newlevel).
//
// Es un proceso parecido al proceso de creación de nuevo nivel de bonedit, eso hace que use algunos datos que, a pesar de no estar
//en el struct level (ni newlevel) cuyos valores dependen de los establecidos en newlevel para generar el nivel. Estos datos, que
//no se pueden establecer directamente, son calculados y mostrados cada vez que se alteran los de newlevel. 
//
// El nivel generado por esta via llena de bloques todas las posiciones disponibles según los valores introducidos.
//
//
//PRIVATE
//	int cursor                                      //opción del menú seleccionada
//	int onexit_branch                               //0=vuelve a Init_menu(), 1=Lanza Init(0) (se usa este mecanismo para terminar con
//	                                                    el proceso New_level_menu() antes de llamar a otros que escriban texto)
//	int current_coroutine_id                        //id del proceso gestor de opción actual
//	int option_type[_nulm_option_elements-1]        //tipo de parámetro y de forma de regulación de cada opción
//	int array_of_pointers[_nlm_option_elements-1]   //contiene punteros a las variables que modifican cada una de las opciones
//	int reactive_data_pointers[]                    //array con punteros que se pasarán al proceso encargado de mostrar los datos
//	                                                    de información deducida (no modificada por el usuario, pero relativa al valor
//	                                                    de otros parámetros).
//	int pointer global_option                       //puntero elegido entre array_of_pointers que se pasa a los procesos gestores de
//	                                                    opción para modificar el valor de la variable apuntada
//	float pointer f_global_option                   //igual que int pointer global_option pero para trabajar con floats
//	int int_ranges[][1]                             //array 2d que contiene los límites inferior y superior para las opciones que trabajan con
//	                                                    ints ([cursor][0] para el mínimo, [cursor][1]para el máximo)
//	float_float_ranges[][1]                         //igual que int ranges pero para opciones que utilizan floats
//	int text_ids[]                                  //guarda los ids de texto devueltos por write() y write_var() para poder borrarlos
//	struct newlevel                                 //igual que la struct global level pero sin el array int block_layout[]. sirve para
//		int playfield_i, playfield_j                    //configurar los datos del nuevo nivel sin sobreescribir el que esté en ese momento
//		int max_blocks_i, max_blocks_j                  //cargado en el editor. max_blocks_i y max_blocks_j se deducen de los otros 4 
//		int block_length, block_height                  //(y de new_lower_margin_ratio)
//	int new_blockfield_i, new_blockfield_j          //tamaño que pueden ocupar los bloques dentro del playfield, se calcula a partir de
//	                                                    valores del struct newlevel y su resultado se copia a las globales g_blockfield_i
//	                                                    y g_blockfield_j respectivamente
//	float f_new_blockfield_j                        //variable float necesaria para hacer calculos con new_lower_margin_ratio (0.0 a 1.0)
//	float new_lower_margin_ratio                    //cantidad de playfield vacío de posibles bloques reservada a la parte inferior de 
//	                                                    este. es un numero entre 0.0 y 1.0 por el que se multiplica newlevel.playfield_j
//	string option_name[_num_option_elements]        //etiquetas de cada una de las opciones
//
//


PROCESS New_level_menu( )

PRIVATE
	int cursor;
	int onexit_branch;
	int current_coroutine_id;
	int option_type[ _nlm_option_elements-1 ];
	int array_of_pointers[ _nlm_option_elements-1 ];
	int reactive_data_pointers[ _nlm_reactive_elements-1 ];  //se pasa como puntero a NLM_reactive_data porque este toca variables de newlevel
	int pointer global_option;
	float pointer f_global_option;
	int int_ranges[ _nlm_option_elements-1 ][ 1 ];
	float float_ranges[ _nlm_option_elements-1 ][ 1 ];
	int text_ids[ (_nlm_option_elements*2)-1 ];
	struct newlevel
		int playfield_i;
		int playfield_j;
		int max_blocks_i;
		int max_blocks_j;
		int block_length;
		int block_height;
	end
	int new_blockfield_i;
	int new_blockfield_j;
	float f_new_blockfield_j;
	float new_lower_margin_ratio;
	string option_name[ _num_option_elements-1 ];
END

BEGIN
	say( "NEW_LEVEL_MENU" );
			
	//DATA FILL

	option_type[ 0 ] = _otype_uint_arbitrary;
	option_type[ 1 ] = _otype_uint_arbitrary;
	option_type[ 2 ] = _otype_uint_arbitrary;
	option_type[ 3 ] = _otype_uint_arbitrary;
	option_type[ 4 ] = _otype_float_arbitrary;
	
	option_name[ 0 ] = "PLAYFIELD_I";
	option_name[ 1 ] = "PLAYFIELD_J";
	option_name[ 2 ] = "BLOCK_LENGTH";
	option_name[ 3 ] = "BLOCK_HEIGHT";
	option_name[ 4 ] = "LOWER_MARGIN_RATIO";
	
	array_of_pointers[ 0 ] = &newlevel.playfield_i;
	array_of_pointers[ 1 ] = &newlevel.playfield_j;
	array_of_pointers[ 2 ] = &newlevel.block_length;
	array_of_pointers[ 3 ] = &newlevel.block_height;
	array_of_pointers[ 4 ] = &new_lower_margin_ratio;
	
	reactive_data_pointers[ 0 ] = &newlevel.max_blocks_i;
	reactive_data_pointers[ 1 ] = &newlevel.max_blocks_j;
	reactive_data_pointers[ 2 ] = &new_blockfield_i;
	reactive_data_pointers[ 3 ] = &new_blockfield_j;

	int_ranges[ 0 ][ 0 ] = 1; int_ranges[ 0 ][ 1 ] = 640;
	int_ranges[ 1 ][ 0 ] = 1; int_ranges[ 1 ][ 1 ] = 640;
	int_ranges[ 2 ][ 0 ] = 1; int_ranges[ 2 ][ 1 ] = 640;
	int_ranges[ 3 ][ 0 ] = 1; int_ranges[ 3 ][ 1 ] = 640;

	float_ranges[ 4 ][ 0 ] = 0.1; float_ranges[ 4 ][ 1 ] = 0.8;
	
	IF ( level.playfield_i==0 )  //load defaults on first run
		newlevel.playfield_i = _default_playfield_i;
		newlevel.playfield_j = _default_playfield_j;
		newlevel.block_length = _default_block_length;
		newlevel.block_height = _default_block_height;
		new_lower_margin_ratio = _default_lower_m_ratio;
	ELSE
		newlevel.playfield_i = level.playfield_i;
		newlevel.playfield_j = level.playfield_j;
		newlevel.block_length = level.block_length;
		newlevel.block_height = level.block_height;
		
		new_lower_margin_ratio = _default_lower_m_ratio;
	END
		

	//DEDUCTED DATA
	newlevel.max_blocks_i = newlevel.playfield_i/newlevel.block_length;
	new_blockfield_i = newlevel.max_blocks_i*newlevel.block_length;

	f_new_blockfield_j = newlevel.playfield_j*new_lower_margin_ratio;
	newlevel.max_blocks_j = f_new_blockfield_j/newlevel.block_height;
	new_blockfield_j = newlevel.max_blocks_j*newlevel.block_height;
	
			say( "New_level_menu: data written" );


	//MENU DRAW
	graph = map_new( 16, 8, 32 );
	map_clear( 0, graph, rgb( 255, 0, 0 ) );
	graphic_set( 0, graph, G_X_CENTER, 0 );
	graphic_set( 0, graph, G_Y_CENTER, 0 );

	say( "New_level_menu: map drawn" );

	
	text_z = -4;
	FOR ( cursor = 0 ; cursor<_nlm_option_elements ; cursor++ )  //cursor es usado como i
		text_ids[cursor] = write( 0, _nlm_labels_x, _nlm_line_height*cursor, 0, option_name[ cursor ] );
		say( "-text_ids["+cursor+"]=="+text_ids[cursor] );
		say( "-text_ids["+cursor+"]=="+option_name[cursor] );
	END
	
	FOR ( cursor = 0 ; cursor<_nlm_option_elements ; cursor++ )  //cursor es usado como i
		IF ( array_of_pointers[ cursor ]!=0 )
			IF ( option_type[ cursor ]==_otype_uint_arbitrary )

				global_option = array_of_pointers[ cursor ];
				text_ids[(cursor+_nlm_option_elements)] = write_var( 0, _nlm_numbers_x, _nlm_line_height*cursor, 0, *global_option );
				say( "-text_ids["+(cursor+_nlm_option_elements)+"]=="+text_ids[cursor+_nlm_option_elements] );
				say( "-text_ids["+(cursor+_nlm_option_elements)+"]string=="+*global_option );

			ELSEIF ( option_type[ cursor ]==_otype_float_arbitrary )

				f_global_option = array_of_pointers[ cursor ];
				text_ids[(cursor+_nlm_option_elements)] = write_var( 0, _nlm_numbers_x, _nlm_line_height*cursor, 0, *f_global_option );
				say( "-text_ids["+(cursor+_nlm_option_elements)+"](cursor+5)=="+text_ids[cursor+_nlm_option_elements] );
				say( "-text_ids["+(cursor+_nlm_option_elements)+"]string=="+*f_global_option );

			END
		END
	END
	
	NLM_reactive_data( 140, &reactive_data_pointers );
	
	say( "New_level_menu: text drawn" );


	cursor = 0;	
	x = 160;
	y = _nlm_line_height*cursor;
	z = -4;

	LOOP

		//COROUTINE CALL
		IF ( current_coroutine_id==0 )
			IF ( option_type[ cursor ]==_otype_uint_arbitrary )
				say( "New_level_menu: option "+cursor+" is int arbitrary" );

				global_option = array_of_pointers[ cursor ];
				current_coroutine_id = Option_int_arbitrary( global_option, &graph, int_ranges[cursor][0], int_ranges[cursor][1] );
				
			ELSEIF ( option_type[ cursor ]==_otype_float_arbitrary )
				say( "New_level_menu: option "+cursor+" is float selector" );

				global_option = array_of_pointers[ cursor ];
				current_coroutine_id = Option_float_arbitrary( global_option, &graph, float_ranges[cursor][0], float_ranges[cursor][1] );

			ELSE 
				say( "Init_menu: ERROR could not determine option_type, cursor=="+cursor );
			END
		END
		
		FRAME;
		

		IF ( REWIND_K )  WHILE ( REWIND_K ) FRAME; END
			//say( "-New_level_menu: backfire BREAK, editor_status=="+editor_status );
			onexit_branch = 0;
			BREAK;
		END
		IF ( KEYNEW )  WHILE ( KEYNEW ) FRAME; END

			level.playfield_i = newlevel.playfield_i;
			level.playfield_j = newlevel.playfield_j;
			level.max_blocks_i = newlevel.max_blocks_i;
			level.max_blocks_j = newlevel.max_blocks_j;
			level.block_length = newlevel.block_length;
			level.block_height = newlevel.block_height;
			
			h_block_length = level.block_length/2;
			h_block_height = level.block_height/2;
					
			say( "New_level_menu: Creating new level," );
						
			onexit_branch = 1;		
			BREAK;

		END
		
		IF (key(_d)) 		WHILE (key(_d)) FRAME; END
			newlevel.playfield_i = _default_playfield_i;
			newlevel.playfield_j = _default_playfield_j;
			newlevel.block_length = _default_block_length;
			newlevel.block_height = _default_block_height;
			new_lower_margin_ratio = _default_lower_m_ratio;

			newlevel.max_blocks_i = newlevel.playfield_i/newlevel.block_length;
			new_blockfield_i = newlevel.max_blocks_i*newlevel.block_length;
			f_new_blockfield_j = newlevel.playfield_j*new_lower_margin_ratio;
			newlevel.max_blocks_j = f_new_blockfield_j/newlevel.block_height;
			new_blockfield_j = newlevel.max_blocks_j*newlevel.block_height;
			
			say( "New_level_menu: Default values set" );
		END
		
	
		//OPTION CHANGE
		IF ( KEYUP OR KEYDOWN )
			IF ( current_coroutine_id!=0 )
				say( "--New_level_menu: terminar current_coroutine_id=="+current_coroutine_id );
				signal( current_coroutine_id, s_kill );
				current_coroutine_id = 0;
			END
			
			IF ( KEYUP AND cursor>0 )
				cursor--;
			ELSEIF ( KEYDOWN AND cursor<_nlm_option_elements-1 )
				cursor++;
			END
			y = _nlm_line_height*cursor;
			
		//REACTIVE DATA UPDATE
		ELSEIF ( KEYLEFT OR KEYRIGHT )
			newlevel.max_blocks_i = newlevel.playfield_i/newlevel.block_length;
			new_blockfield_i = newlevel.max_blocks_i*newlevel.block_length;
		
			f_new_blockfield_j = newlevel.playfield_j*new_lower_margin_ratio;
			newlevel.max_blocks_j = f_new_blockfield_j/newlevel.block_height;
			new_blockfield_j = newlevel.max_blocks_j*newlevel.block_height;
		END
		
	END

ONEXIT
		
	//text deletion
	text_clearer( _nlm_option_elements*2, &text_ids );
	//FOR (	cursor = 0; cursor<_nlm_option_elements*2; cursor++ )
	//	IF ( text_ids[cursor]!=0 )
	//		delete_text( text_ids[cursor] );
	//		say( "New_level_menu: deleting text, text_ids["+cursor+"]=="+text_ids[cursor] );
	//	ELSE
	//		say( "-nlm_text_clearer: zero found, text_ids["+cursor+"]=="+text_ids[ cursor ] );
	//	END
	//END
	IF ( current_coroutine_id!=0 )
		signal( current_coroutine_id, s_kill );
		current_coroutine_id = 0;
	END
	IF ( exists( type NLM_reactive_data ) )
		signal( type NLM_reactive_data, s_kill );
	END
	
	FRAME;  //hace que NLM_reactive_data( ) borre sus textos antes que Init_menu() escriba los suyos.

	map_unload( 0, graph );
	
	SWITCH ( onexit_branch )
		CASE 0:
			Init_menu( );
		END
		CASE 1:
			init( 0 );
		END
		DEFAULT:
			say( "New_level_menu: switch out of range, onexit_branch=="+onexit_branch );
		END
	END
END



//
//	PROCESS NLM_reactive_data( int vert_padding, int pointer array_of_pointers )
//
// Proceso que muestra en pantalla datos de variables que el usuario no modifica directamente, pero que su valor depende
//de datos que el usuario sí modifica.
//
//PRIVATE
//	int vert_padding                 //Distancia vertical desde el borde superior de la pantalla
//	int pointer array_of_pointers    //Punteros a los datos que ha de imprimir. Los recibe como parámetros (en vez de generarlos
//	                                     él mismo) porque algunos de los datos a mostrar se encuentran en el struct newlevel del
//	                                     proceso New_level_menu() que llama a este
//	int i                            //Usado para iterar en bucles
//	int text_id[]                    //Guarda las ids de texto devueltas por write y write_var para poder borrar los textos
//	int pointer global_option        //Toma valores de array_of_pointers para poder mostrar los datos en cuestion (con write y write_var)
//	string data_labels[]             //Etiquetas de las opciones mostradas
//
//


PROCESS NLM_reactive_data( int vert_padding, int pointer array_of_pointers )

PRIVATE
	int i;
	int text_ids[(_nlm_reactive_elements*2)-1];
	int pointer global_option;
	string data_labels[] = "MAX_BLOCKS_I", "MAX_BLOCKS_J", "BLOCKFIELD I", "BLOCKFIELD J";
END

BEGIN
	say( "-NLM_REACTIVE_DATA" );

	FOR ( i = 0; i<_nlm_reactive_elements; i++ )
		text_ids[ i ] = write( 0, _nlm_labels_x, vert_padding+_nlm_line_height*i, 0, data_labels[i] );
		say( "NLM_reactive_data: writing text, text_ids["+i+"]=="+text_ids[i] );
		say( "--nlm: a i=="+i+" data_labels=="+data_labels[i] );
	END
	
	FOR ( i = 0; i<_nlm_reactive_elements; i++ )
		global_option = array_of_pointers[i];
		text_ids[ i+_nlm_reactive_elements ] = write_var( 0, _nlm_numbers_x, vert_padding+_nlm_line_height*i, 0, *global_option );
		say( "NLM_reactive_data: writing text, text_ids["+(i+_nlm_reactive_elements)+"]=="+text_ids[(i+_nlm_reactive_elements)] );
		say( "--nlm: b i=="+i+" data_labels=="+*global_option);
	END

	signal( id, S_FREEZE );
		
	FRAME;

ONEXIT

	text_clearer( _nlm_reactive_elements*2, &text_ids );
	
	//FOR ( i = 0; i<_nlm_reactive_elements*2 ; i++ )
	//	IF ( text_ids[i]!=0 )
	//		say( "NLM_reactive_data: deleting text, text_ids["+i+"]=="+text_ids[i] );
	//		delete_text( text_ids[i] );
	//	ELSE
	//		say( "NLM_text_clearer: zero found, text_id=="+text_ids[i] );
	//	END
	//END
	
END