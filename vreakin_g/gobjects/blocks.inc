//BLOCKS.INC
//Procesos para los bloques

CONST
	_regen_delay = 120;   //tiempo en frames en que los bloques regenerantes ganan 1 punto de energia
	_shield_delay = 180;  //tiempo en frames que tarda en volver el blindaje a los bloques blindados
	_blink_delay = 180;

	_pwup_gravity = 0.1;  //atención! a partir de 0.2 según la altura llega a caer por debajo de paddle, considerar un valor _pwup_max_gravity
END




//
// PROCESS Block( )
//
// Representación gráfica de un bloque. Cada instancia de Block() es creada por el proceso de comportamiento de
//bloque correspondiente. Una vez invocado recibe sus parámetros de posición y gráfico del proceso que lo invocó y se congela
// El proceso Ball() comprueba todas sus colisiones contra instancias de Block() y escribe
//_ball_impact_msg en su (de Block) variable local msg_type y la fuerza del impacto en msg_value.
// Finalmente, al salir (por orden de su proceso padre) guarda los datos correspondientes en undo_buffer[]
//
//


PROCESS Block( )

BEGIN
	priority = _blocks_priority;
	FRAME;  //hace que coja bien el i_pos y j_pos enviado por father

	INCLUDE "sys/ij-to-xy.inc";
	
	signal( id, s_freeze );
	FRAME;

ONEXIT
	say( g_frame_counter+" | Block id=="+id+" saliendo" );
	blocksleft--;
	undo_buffer[blocksleft].pos_x = i_pos;
	undo_buffer[blocksleft].pos_y = j_pos;
	undo_buffer[blocksleft].block_code = msg_value;
	redo_limit = blocksleft;

	//undo_buffer[blocksleft].redo_id = id;
END




//--------
/*
//DEBUG:!!!!
PROCESS Block( i_pos, j_pos, byte block_code )

BEGIN
	priority = _blocks_priority;
	file = idfpg;
	graph = block_code;

	FRAME;		//hace que coja bien el i_pos y j_pos enviado por father

	INCLUDE "sys/ij-to-xy.inc";
	
	signal( id, s_freeze );
	FRAME;

ONEXIT
	say( g_frame_counter+" | Block id=="+id+" saliendo" );
	blocksleft--;
	undo_buffer[blocksleft].pos_x = i_pos;
	undo_buffer[blocksleft].pos_y = j_pos;
	//undo_buffer[blocksleft].redo_id = id;
	//undo_buffer[blocksleft].block_code = block_code;
	undo_buffer[blocksleft].block_code = 1;			//hack
	redo_limit = blocksleft;
END
*/
//---------




//
// PROCESS Block_normal( i_pos, j_pos, byte block_code )
// PROCESS Block_regen( i_pos, j_pos, byte block_code )
// PROCESS Block_shield( i_pos, j_pos, byte block_code )
// PROCESS Block_blink( i_pos, j_pos, byte block_code )
// PROCESS Block_blink_sh( i_pos, j_pos, byte block_code )
// PROCESS Block_unbreak( i_pos, j_pos, byte block_code )
//
// Procesos de comportamiendo de bloque. Cada uno rige un tipo de bloque distinto. Tras ser invocados por la función block_put(), 
//crean una instancia de Block() cada uno y la usan para mostrar su gráfico y interactuar con los procesos Ball(). De esta manera cada
//bloque puede tener un comportamiento, energia, etc. distinto manteniendo el sistema de colisiones simple. 
// Los procesos de comportamiento de bloque normalmente reaccionan cuando al leer las variables locales son.msg_* ven que ha habido 
//un impacto con la bola. En el caso de que al bloque se le termine la energía estos procesos se terminan junto a su proceso hijo.
// Su prioridad es más alta que la del proceso hijo.
// Por ahora hay los siguientes tipos:
//	Block_normal: se rompe al llegar su energia a 0.
//	Block_regen: regenera su energía paulatinamente tras ser golpeado.
//	Block_shield: hay que darle un primer golpe antes de poder quitarle energía. Si tras una cantidad determinada de tiempo no 
//	    se le vuelve a golpear, vuelve a estar blindado.
//	Block_blink: aparece y desaparece intermitentemente tras una cantidad predeterminada de tiempo.
//	Block_blink_sh: igual que Block_blink pero en vez de desaparecer se vuelve irrompible.
//	Block_unbreak: no se puede romper impactando con la bola.
//
// 


PROCESS Block_normal( i_pos, j_pos, byte block_code )

PRIVATE
	int energy;   //ag3
	int element;  //ag2
END

BEGIN
	priority = _blocks_priority+1;
	FRAME;
	
	Block( );

	//no entiendo pq lo siguiente funciona, pensaba que al ser energy y element ints al mover << los bits a la derecha
	//daría un valor muy superior en vez de descartar bits. Supongo que es porque la operacion la hace sobre block_code
	//que sí que es un byte

	energy = block_code<<5;
	energy = energy>>5;
	element = block_code<<3;
	element = element>>6;

	//element = (block_code-((block_code>>5)<<5))>>3;
	//energy = block_code-(element<<3)-((block_code>>5)<<5);

	say( "Block_normal, element=="+element+" energy=="+energy );

	son.file = idfpg;
	son.graph = (0<<5)+(element<<2)+energy;  //establece el graph original, 0 pq es el tipo 0 en el futuro seguramente haya un fpg dedicado
	son.i_pos = i_pos;
	son.j_pos = j_pos;
	LOOP
		IF (son.msg_type == _ball_impact_msg)
			say( g_frame_counter+" | Block id "+son.id+": received msg_type "+son.msg_type+" msg_value "+son.msg_value );

			IF (energy>son.msg_value)
				energy = energy-son.msg_value;
				son.graph = (0<<5)+(element<<2)+energy;	
				son.msg_type = 0;
				son.msg_value = 0;
				say( "  --Block hit");
			ELSE
				say( "  --Block broken" );
				BREAK;
			END
		END

		FRAME;
	END
ONEXIT
	son.msg_value = block_code;
	signal( son, S_KILL );
	//son.msg_type = 2;
	//signal( son, S_WAKEUP );
END




PROCESS Block_regen( i_pos, j_pos, byte block_code )

PRIVATE
	int energy;
	int element;
	int total_energy;
	int count;
END

BEGIN
	priority = _blocks_priority+1;
	FRAME;
	Block( );

	energy = block_code<<5;
	energy = energy>>5;
	element = block_code<<3;
	element = element>>6;

	say( "Block_normal, element=="+element+" energy=="+energy );

	son.file = idfpg;
	son.graph = (1<<5)+element+energy;  //1<<3+energy;  //establece el graph original, 1 pq es el tipo 1 en el futuro seguramente haya un fpg dedicado
	son.i_pos = i_pos;
	son.j_pos = j_pos;

	total_energy = energy;

	LOOP
		IF (son.msg_type == _ball_impact_msg)
			say( "Block id "+son.id+": received msg_type "+son.msg_type+" msg_value "+son.msg_value );
			say( "--Frame: "+g_frame_counter );

			IF (energy>son.msg_value)
				energy = energy-son.msg_value;
				son.graph = (1<<5)+element+energy;
				son.msg_type = 0;
				son.msg_value = 0;
				say( "  --Block hit");
			ELSE
				say( "  --Block broken" );
				BREAK;
			END
		END

		IF (energy<total_energy)
			count++;
			IF (count==_regen_delay)
				count = 0;
				energy++;
				son.graph = (1<<5)+element+energy;
			END
		END

		FRAME;
	END
ONEXIT
	son.msg_value = block_code;
	signal( son, S_KILL );
END




PROCESS Block_shield( i_pos, j_pos, byte block_code )

PRIVATE
	int energy;
	int element;
	int shield;
	int count;
END

BEGIN
	priority = _blocks_priority+1;
	FRAME;
	Block( );

	energy = block_code<<5;
	energy = energy>>5;
	element = block_code<<3;
	element = element>>6;

	say( "Block_shield, element=="+element+" energy=="+energy );

	son.file = idfpg;
	son.graph = 253;  //2<<3+energy;  //establece el graph original, 2 pq es el tipo 2 en el futuro seguramente haya un fpg dedicado
	son.i_pos = i_pos;
	son.j_pos = j_pos;

	shield = 1;

	LOOP
		IF (son.msg_type == _ball_impact_msg)
			say( g_frame_counter+" | Block id "+son.id+": received msg_type "+son.msg_type+" msg_value "+son.msg_value );
			say( "--Frame: "+g_frame_counter );

			IF (shield==1)
				shield = 0;
				son.graph = (2<<5)+element+energy;
			ELSE
				IF (energy>son.msg_value)
					energy = energy-son.msg_value;
					son.graph = (2<<5)+element+energy;
					say( "  --Block hit");
				ELSE
					say( "  --Block broken" );
					BREAK;
				END
			END
			son.msg_type = 0;
			son.msg_value = 0;
		END

		IF (shield==0)
			count++;
			IF (count==_shield_delay);
				count = 0;
				shield = 1;
				son.graph = 253;
			END
		END

		FRAME;
	END
ONEXIT
	son.msg_value = block_code;
	signal( son, S_KILL );
END




PROCESS Block_blink( i_pos, j_pos, byte block_code )

PRIVATE
	int energy;
	int element;
	int active;
	int count;
END

BEGIN
	priority = _blocks_priority+1;
	FRAME;
	Block( );

	energy = block_code<<5;
	energy = energy>>5;
	element = block_code<<3;
	element = element>>6;
	say( "Block_normal, element=="+element+" energy=="+energy );

	son.file = idfpg;
	son.graph =(3<<5)+element+energy;
	son.i_pos = i_pos;
	son.j_pos = j_pos;

	active = 1;

	LOOP
		count++;
		IF (count==_blink_delay)
			active = 1-active;
			count = 0;
			IF (active==0)
				//son.graph = 254;
				signal( son, S_SLEEP );
			ELSE
				//son.graph =(3<<5)+element+energy;
				signal( son, S_FREEZE );
			END
		END

		IF (son.msg_type==_ball_impact_msg)
			IF (active)
				say( g_frame_counter+" | Block id "+son.id+": received msg_type "+son.msg_type+" msg_value "+son.msg_value );

				IF (energy>son.msg_value)
					energy = energy-son.msg_value;
					son.graph =(3<<5)+element+energy;
					son.msg_type = 0;
					son.msg_value = 0;
					say( "  --Block hit");
				ELSE
					say( "  --Block broken" );
					BREAK;
				END

			ELSE
				son.msg_type = 0;
				son.msg_value = 0;
			END
		END

		FRAME;
	END
ONEXIT
	son.msg_value = block_code;
	signal( son, S_KILL );
END




PROCESS Block_blink_sh( i_pos, j_pos, byte block_code )

PRIVATE
	int energy;
	int element;
	int active;
	int count;
END

BEGIN
	priority = _blocks_priority+1;
	FRAME;
	Block( );

	energy = block_code<<5;
	energy = energy>>5;
	element = block_code<<3;
	element = element>>6;	

	say( "Block_normal, element=="+element+" energy=="+energy );

	son.file = idfpg;
	son.graph =(4<<5)+element+energy;
	son.i_pos = i_pos;
	son.j_pos = j_pos;

	active = 1;

	LOOP
		count++;
		IF (count==_blink_delay)
			active = 1-active;
			count = 0;
			IF (active==0)
				son.graph = 254;
				//signal( son, S_SLEEP );
			ELSE
				son.graph =(4<<5)+element+energy;
				//signal( son, S_FREEZE );
			END
		END

		IF (son.msg_type==_ball_impact_msg)
			IF (active)
				say( g_frame_counter+" | Block id "+son.id+": received msg_type "+son.msg_type+" msg_value "+son.msg_value );

				IF (energy>son.msg_value)
					energy = energy-son.msg_value;
					son.graph =(4<<5)+element+energy;
					son.msg_type = 0;
					son.msg_value = 0;
					say( "  --Block hit");
				ELSE
					say( "  --Block broken" );
					BREAK;
				END

			ELSE
				son.msg_type = 0;
				son.msg_value = 0;
			END
		END

		FRAME;
	END
ONEXIT
	son.msg_value = block_code;
	signal( son, S_KILL );
END




PROCESS Block_unbreak( i_pos, j_pos, byte block_code )  //de momento lo unico que hace es poner un bloque que no se puede romper

BEGIN
	priority = _blocks_priority+1;
	FRAME;

	Block( );

	son.file = idfpg;
	son.graph = 255;
	son.i_pos = i_pos;
	son.j_pos = j_pos;
	FRAME;
END




//
// PROCESS Block_power_up( i_pos, j_pos, int pwup_type, int pwup_value, int block_code )
//
// Bloque que otorga un power up al ser golpeado (por ahora no tiene energia). El tipo de power up otorgado
//viene dado por los valores pwup_type y pwup_value. block_code sirve para poder hacer undo y redo.
//
//


PROCESS Block_power_up( i_pos, j_pos, int pwup_type, int pwup_value, int block_code )

BEGIN
	priority = _blocks_priority+1;
	FRAME;

	Block( );

	son.file = idfpg;
	son.graph = block_code;
	son.i_pos = i_pos;
	son.j_pos = j_pos;

	LOOP
		IF ( son.msg_type == _ball_impact_msg )
			son.msg_value = block_code;
			signal( son, S_KILL );
			Power_up( i_pos, j_pos, pwup_type, pwup_value, 1.0 );
			BREAK;
		END
		FRAME;
	END
ONEXIT
	//son.msg_value = block_code;
	//signal( son, S_KILL );
END	




//
// PROCESS Power_up( i_pos, j_pos, msg_type, msg_value, float ball_pow_vx )
//
// Proceso que representa el power up. Creado por Block_power_up() al ser roto. msg_type y msg_value definen 
//la acción del power up cuando es cogido por el jugador. El power up hace un movimiento parabólico con aceleración
//(gravedad) y se considera que el jugador lo coge cuando consigue tocarlo con la pala. Cuando esto sucede,
//Power_up() escribe sus valores msg_type y msg_value en el elemento correspondiente (idicado por pw_catch_write_index)
//del struct global pw_catch[].
//
//


PROCESS Power_up( i_pos, j_pos, msg_type, msg_value, float ball_pow_vx )

PRIVATE
	float vy;
END

BEGIN
	file = idfpg;
	graph = _pwup_graph;
	vy = -gameplay.ball_j_speed;

	say( g_frame_counter+" | Power_up! msg_type=="+msg_type+" msg_value"+msg_value );

	INCLUDE "sys/ij-to-xy.inc";
	FRAME;

	LOOP
		i_pos = i_pos+ball_pow_vx;
		j_pos = j_pos+vy;
		vy = vy+_pwup_gravity;

		IF ( i_pos>level.playfield_i OR i_pos<0 )
			ball_pow_vx = -ball_pow_vx;
		END

		IF ( j_pos>level.playfield_j )
			vy = 0;
			//BREAK;
		END

		//IF ( collision( type Paddle ) )  //en la expresion de abajo el 4 es para poder coger el pw aunque no toque su centro

		IF ( j_pos>level.playfield_j-_paddle_j_bottom AND i_pos>paddle_id.i_pos-4-gameplay.paddle_size/2 AND i_pos<paddle_id.i_pos+4+gameplay.paddle_size/2 )
			say( g_frame_counter+" | Power_up: cogido por Paddle! msg_type=="+msg_type+" msg_value=="+msg_value );
			pw_catch[pw_catch_write_index].pu_type = msg_type;
			pw_catch[pw_catch_write_index].pu_value = msg_value;
			pw_catch_write_index++;
			IF (pw_catch_write_index>=_pw_catch_elements)
				pw_catch_write_index = 0;
			END
			BREAK;
		END

		INCLUDE "sys/ij-to-xy.inc";
		FRAME;
	END
ONEXIT
	say( g_frame_counter+" | Power_up "+id+" saliendo");
END





//
// FUNCTION allball_pwup( int pu_type, int pu_value );
//
// Aplica los power ups que afectan a todas las bolas.
//
//


FUNCTION allball_pwup( int pu_type, int pu_value )

PRIVATE
	int ball_id;
	int i;  //debug
END

BEGIN
	say( "allball: pu_type=="+pu_type+" pu_value=="+pu_value );
	WHILE ( ball_id = get_id( type Ball ) )
		ball_id.msg_type = pu_type;
		ball_id.msg_value = pu_value;
		i++;
	END
	say( "--applied to "+i+" Ball processes" );
END