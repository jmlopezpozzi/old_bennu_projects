//
//	EDITOR.INC
//		Contiene los procesos y funciones necesarias para editar un nivel.
//
//


CONST
	_lds_elements = 8;
	_lds_min_x_padding = 220;	
	_lds_labels_x = 20;
	_lds_numbers_x = 160;
	_lds_line_height = 20;

	_max_ag1 = 7;
	_max_ag2 = 3;
	_max_ag3 = 7;

	_cursor_labels = 8;
END




//
// FUNCTION start_editor( )
// FUNCTION stop_editor( )
//
//Sirven para arrancar y parar el editor.
//
//


FUNCTION start_editor( )

BEGIN
	say( "start_editor" );
	current_control_profile = control_profile( 1 );
	Editor_grid( );
	Editor_level_display( );              //El orden establece la prioridad
	editor_cursor_id = Editor_cursor( );  //La prioridad es importante para editor_array_update
	Editor_control_program( );
	
	g_blockfield_i = level.max_blocks_i*level.block_length;
	g_blockfield_j = level.max_blocks_j*level.block_height;
END


FUNCTION stop_editor( )

BEGIN
	say( "stop_editor" );
	IF ( exists( type Editor_grid ) )
		signal( type Editor_grid, s_kill );
	END
	IF ( exists( type Editor_level_display ) )
		signal( type Editor_level_display, s_kill );
	END
	IF ( exists( type Editor_control_program ) )
		signal( type Editor_control_program, s_kill );
	END
	IF ( exists( type Control_program_debug ) )
		signal( type Control_program_debug, s_kill );
	END
	IF ( exists( type Editor_cursor ) )
		signal( type Editor_cursor, s_kill );
		editor_cursor_id = 0;
	END
	WHILE ( exists ( type Editor_block_display ) )
		signal( type Editor_block_display, s_kill );
	END
	WHILE ( exists (type Block_type_show ) )
		signal( type Block_type_show, s_kill );
	END
END




//
//	PROCESS Editor_grid( )
//
//Dibuja una cuadricula que representa todas las posiciones posibles donde se puede poner un bloque en el nivel actual.
//
//


PROCESS Editor_grid( )

PRIVATE
	int i;
END

BEGIN
	say( "editor_grid" );
	graph = map_new( level.playfield_i+1, level.playfield_j+1, 32 );  //+1 para que se vea la ultima linea
	graphic_set( 0, graph, G_X_CENTER, 0 );
	graphic_set( 0, graph, G_Y_CENTER, 0 );
	drawing_map( 0, graph );
	drawing_color( rgb( 255, 255, 255 ) );
	FOR (i = 0; i<=level.max_blocks_i; i++)
		draw_line( level.BLOCK_LENGTH*i, 0, level.BLOCK_LENGTH*i, level.playfield_j );
	END
	FOR (i = 0; i<=level.max_blocks_j; i++)
		draw_line( 0, i*level.BLOCK_HEIGHT, level.playfield_i, i*level.BLOCK_HEIGHT );
	END
	say( "-editor_grid: grid drawn" );
	signal( id, S_FREEZE );
	FRAME;
ONEXIT
	map_unload( 0, graph );
	say( "Editor_grid: EXIT" );
END




//
//	PROCESS Editor_cursor( )
//
//Permite seleccionar una posición de la cuadricula y poner, cambiar o quitar un bloque en la posición seleccionada.
//Dado que el array donde se guarda la información de los bloques es unidimensional, para saber en que posición 
//del array hay que modificar el dato se usa la formula row*max_blocks_i+column . También se encarga de mostrar
//la información guardada en la casilla elegida.
//
//


PROCESS Editor_cursor( )

PRIVATE
	int i;

	int row = 0;           //fila seleccionada
	int column = 0;        //columna seleccionada

	//int block_type;      //ag1  //datos a grabar en la casilla seleccionada
	//int block_energy;    //ag3

	int ag1;
	int ag2;
	int ag3;
	byte block_code;

	int grid_ag1;          //datos existentes en la casilla seleccionada
	int grid_ag2;
	int grid_ag3;
	byte grid_bcode;

	int binfo_labels[_cursor_labels-1];   //ids de cadenas con las infos de cursor y casilla
	int binfo_vlabels[_cursor_labels-1];
END

BEGIN
	say( "EDITOR_CURSOR" );
	//Control_program_debug( id );

	graph = map_new( level.BLOCK_LENGTH, level.BLOCK_HEIGHT, 32 );
	graphic_set( 0, graph, G_X_CENTER, 0 );
	graphic_set( 0, graph, G_Y_CENTER, 0 );
	drawing_map( 0, graph );
	drawing_color( rgb( 0, 255, 0 ) );
	draw_rect( 1, 1, level.BLOCK_LENGTH-1, level.BLOCK_HEIGHT-1 );
	z = -2;
	say( "-Editor_cursor: graph drawn");

	say( "Editor_cursor: calling Block_type_show" );
	Block_type_show( &block_code );
	say( "Editor_cursor: Block_type_show called" );
		
	FRAME;
	
	LOOP
		IF ( editor_status==0 AND binfo_labels[0]==0 )  //escribe datos de bloque (grid y cursor), funciona volviendo de los menus
			binfo_vlabels[0] = write_var( 0, _screenx, 0, 2, block_code );
			binfo_vlabels[1] = write_var( 0, _screenx, 10, 2, ag1 );
			binfo_vlabels[2] = write_var( 0, _screenx, 20, 2, ag2 );
			binfo_vlabels[3] = write_var( 0, _screenx, 30, 2, ag3 );

			binfo_labels[0] = write( 0, _screenx-30, 0, 2, "block_code" );
			binfo_labels[1] = write( 0, _screenx-30, 10, 2, "(btype) ag1" );
			binfo_labels[2] = write( 0, _screenx-30, 20, 2, "(element) ag2" );
			binfo_labels[3] = write( 0, _screenx-30, 30, 2, "(energy) ag3" );


			binfo_vlabels[4] = write_var( 0, _screenx, 50, 2, grid_bcode );
			binfo_vlabels[5] = write_var( 0, _screenx, 60, 2, grid_ag1 );
			binfo_vlabels[6] = write_var( 0, _screenx, 70, 2, grid_ag2 );
			binfo_vlabels[7] = write_var( 0, _screenx, 80, 2, grid_ag3 );

			binfo_labels[4] = write( 0, _screenx-30, 50, 2, "grid_bcode" );
			binfo_labels[5] = write( 0, _screenx-30, 60, 2, "grid_ag1" );
			binfo_labels[6] = write( 0, _screenx-30, 70, 2, "grid_ag2" );
			binfo_labels[7] = write( 0, _screenx-30, 80, 2, "grid_ag3" );
		END
		WHILE ( key( _e ) OR key( _f ) )  //borra datos de bloque al entrar a los menus
			IF ( binfo_labels[0]!=0 )
				FOR ( i = 0; i<_cursor_labels; i++ )
					delete_text( binfo_labels[i] );
					delete_text( binfo_vlabels[i] );
					binfo_labels[i] = 0;
					binfo_vlabels[i] = 0;
				END
			END
			FRAME;
		END

		IF ( KEYRIGHT AND column<level.max_blocks_i-1 )
			column++;
		END
		IF ( KEYLEFT AND column>0 )
			column--;
		END
		IF ( KEYDOWN AND row<level.max_blocks_j-1 )
			row++;
		END
		IF ( KEYUP AND row>0 )
			row--;
		END

		grid_bcode = level.block_layout[(row*level.max_blocks_i)+column];
		grid_ag1 = grid_bcode>>5;
		grid_ag2 = (grid_bcode-(grid_ag1<<5))>>3;
		grid_ag3 = grid_bcode-(grid_ag2<<3)-(grid_ag1<<5);

		IF (key( _a ) AND ag1<_max_ag1)
			IF (current_control_profile==1)
				WHILE (key( _a )) FRAME; END
			END
			ag1++;
		ELSEIF (key( _z ) AND ag1>0)
			IF (current_control_profile==1)
				WHILE (key( _z )) FRAME; END
			END
			ag1--;
		END

		IF (key( _s ) AND ag2<_max_ag2)
			IF (current_control_profile==1)
				WHILE (key( _s )) FRAME; END
			END
			ag2++;
		ELSEIF (key( _x ) AND ag2>0)
			IF (current_control_profile==1)
				WHILE (key( _x )) FRAME; END
			END
			ag2--;
		END

		IF (key( _d ) AND ag3<_max_ag3)
			IF (current_control_profile==1)
				WHILE (key( _d )) FRAME; END
			END
			ag3++;
		ELSEIF (key( _c ) AND ag3>0)
			IF (current_control_profile==1)
				WHILE (key( _c )) FRAME; END
			END
			ag3--;
		END

		block_code = (ag1<<5)+(ag2<<3)+ag3;

	
		IF (FIRE)
			level.block_layout[(row*level.max_blocks_i)+column] = block_code;
			say( "Editor_cursor: putting block at column=="+column+" row=="+row );
			say( "Editor_cursor: level.block_layout[row*level.max_blocks_i+column]==["+(row*level.max_blocks_i+column)+"] "+level.block_layout[row*level.max_blocks_i+column]);
			editor_array_update = 1;
		END
	
		x = column*level.BLOCK_LENGTH;
		y = row*level.BLOCK_HEIGHT;
		FRAME;
	END
ONEXIT
	//IF (exists( type Block_type_show ))  //es terminado por stop_editor() de lo contrario da problemas al crear un nuevo nivel
	//	signal( type Block_type_show, S_KILL );  //esto pasa porque tiene una prioridad inferior al cursor
	//END

	FOR (row = 0; row<_cursor_labels; row++)
		delete_text( binfo_vlabels[row] );
		delete_text( binfo_labels[row] );
	END
END




//
// PROCESS Block_type_show( byte pointer block_code )
//
//Muestra sobre el cursor el valor de block_code elegido para grabar en la casilla apuntada. Es llamado por Editor_cursor()
//y usa sus coordenadas x,y . Tiene una prioridad inferior a Editor_cursor() para que al leer estas coordenadas ya hayan
//sido actualizadas durante el frame. Es terminado por stop_editor() en vez de Editor_cursor() porque de lo contrario falla
//al crear un mapa nuevo por motivo de la prioridad.
//
//


PROCESS Block_type_show( byte pointer block_code )

PRIVATE
	int graph_ids[255];
	int temp_map_id;
	int i;
END

BEGIN
	priority = father.priority-1;
	say( "Block_type_show enters" );

	FOR (i = 0; i<256; i++)
		temp_map_id = write_in_map( 0, i, 0);
		graph_ids[i] = map_new( map_info_get( 0, temp_map_id, G_WIDTH ), map_info_get( 0, temp_map_id, G_HEIGHT), 32 );
		map_clear( 0, graph_ids[i], rgb( 0, 0, 255 ) );
		map_put( 0, graph_ids[i], temp_map_id, 0, 0 );
		graphic_set( 0, graph_ids[i], G_X_CENTER, -level.block_length/2+map_info_get( 0, graph_ids[i], G_WIDTH )/2 );
		graphic_set( 0, graph_ids[i], G_Y_CENTER, -level.block_height/2+map_info_get( 0, graph_ids[i], G_HEIGHT )/2 );
		map_unload( 0, temp_map_id );

		//say( "Block_type_show: graph_ids["+i+"] size=="+map_info_get( 0, graph_ids[i], G_WIDTH )+" by "+map_info_get( 0, graph_ids[i], G_HEIGHT) );
		//say( "--graph_ids["+i+"]=="+graph_ids[i] );
	END
	say( "Block_type_show: number graphics generated, entering loop" );
	LOOP
		graph = graph_ids[*block_code];
		x = father.x;
		y = father.y;

		FRAME;
	END

ONEXIT
	FOR (i = 0; i<256; i++)
		map_unload( 0, graph_ids[i] );
	END
	say( "Block_type_show exits" );
END
	



//
//	PROCESS Editor_level_display( )
//	PROCESS Editor_block_display( x, y, graph )
//
//Proceso que muestra en que posiciones hay bloques puestos. Para ello lee el array en level.block_layout[] y pone un 
//proceso Editor_block_display() en cada casilla donde toque, guardando sus ids en otro array used_blocks[]. En el momento
//en que se ha modificado el nivel (usando Editor_cursor()) se eliminan todos las instancias de Editor_block_display() y
//se crean nuevas correspondiendose con el estado actual de level.block_layout[]. Editor_block_display() simplemente 
//muestra el gráfico correspondiente y se congela.
//
//PRIVATE
//	int i;                      //Usado para recorrer arrays
//	int used_blocks[];          //Aquí se guardan las ids de los procesos Editor_block_display() para luego poder eliminarlos
//	int block_display_graph[];  //Usado para crear el grafico de Editor_block_display() y pasarselo como argumento graph a cada 
//	                                instancia creada. Crea 1 por cada posible valor en level.block_layout.
//	int temp_map_id;            //Usado para generar los gráficos a guardar en block_display_graph() al arrancar.
//
//


PROCESS Editor_level_display( )

PRIVATE
	int i;
	int used_blocks[LEVEL_ARRAY_ELEMENTS];
	int block_display_graph[255];
	int temp_map_id;
END

BEGIN
	say( "EDITOR_LEVEL_DISPLAY" );

	FOR (i = 0; i<256; i++)
		temp_map_id = write_in_map( 0, i, 0 );
		block_display_graph[i] = map_new( level.block_length, level.block_height, 32 );
		drawing_map( 0, block_display_graph[i] );
		map_clear( 0, block_display_graph[i], rgb( 255, 0, 0 ) );
		map_put( 0, block_display_graph[i], temp_map_id, (level.block_length/2)-map_info_get( 0, temp_map_id, G_WIDTH )/2, 
		                                                 (level.block_height/2)-map_info_get( 0, temp_map_id, G_HEIGHT )/2 );
		map_unload( 0, temp_map_id );
	END
	
	LOOP
		FOR (i = 0; i<=LEVEL_ARRAY_ELEMENTS; i++)
			IF (level.block_layout[i]>0)
				temp_map_id = level.block_layout[i];    //esto se puede ahorrar pasandlo directamente al proceso
				used_blocks[i] = Editor_block_display( find_column( i )*level.BLOCK_LENGTH, 
				                                       find_row( i )*level.BLOCK_HEIGHT, block_display_graph[temp_map_id] );
			END
		END
		editor_array_update = 0;
		
		REPEAT
			FRAME;
		UNTIL (editor_array_update==1)
		
		FOR (i = 0; i<=LEVEL_ARRAY_ELEMENTS; i++)
			IF (used_blocks[i]!=0)
				signal( used_blocks[i], s_kill );
			END
			used_blocks[i] = 0;
		END
	END
END



PROCESS Editor_block_display( x, y, graph )

BEGIN
	graphic_set( 0, graph, G_X_CENTER, 0 );
	graphic_set( 0, graph, G_Y_CENTER, 0 );
	signal( id, s_freeze );
	FRAME;
END




//
//	FUNCTION find_column( int i )
//	FUNCTION find_row( int i )
//
//Funciones que, a partir de la posición del elemento i del array unidimensional level.block_layout (y todos los
//que funcionen igual) encuentra su posición en la cuadrícula que representa el nivel.
//
//


FUNCTION find_column( int i )

BEGIN
	LOOP
		IF (i>=level.max_blocks_i)
			i = i-level.max_blocks_i;
		ELSE
			BREAK;
		END
	END
	RETURN(i);
END


FUNCTION find_row( int i )

PRIVATE
	int j;
END

BEGIN
	LOOP
		IF (i>=level.max_blocks_i)
			i = i-level.max_blocks_i;
			j++;
		ELSE
			BREAK;
		END
	END
	RETURN(j);
END




//
//	PROCESS Editor_control_program( )
//	Permite llamar a File_manager() o New_level_menu() desde el editor. Al llamar a alguno de estos dos procesos se pone la variable
//global editor_status a 1. Mientras editor_status valga 1 Editor_control_program no hace nada.
//	También permite cambiar el control_profile para el cursor, recordando cual era el seleccionado gracias a la variable privada
//cursor_control_profile (es necesario puesto que tanto New_level_menu() como File_manager() cambian el control_profile ).
//	Por último, también muestra o borra el Level_data_show( )
//
//


PROCESS Editor_control_program( )

PRIVATE
	byte cursor_control_profile;
END

BEGIN
	say( "EDITOR CONTROL PROGRAM" );
	current_control_profile = control_profile( 1 );
	cursor_control_profile = current_control_profile;
	
	priority = editor_cursor_id.priority-1;  //evita doble reconocimento de FIRE con el cursor por cambio de control_profile
	
	//write_var( 0, _screenx, 0, 2, current_control_profile );
	//write_var( 0, _screenx, 12, 2, cursor_control_profile );

	LOOP
		SWITCH ( editor_status )
			CASE 0:
				say( "-Editor_control_program: A, editor_status=="+editor_status );
				IF ( get_status( editor_cursor_id )==4 )
					signal( editor_cursor_id, s_wakeup );
				END
				IF ( exists( type Level_data_show ) )     //AND get_status( type Level_data_show )==4 )
					signal( type Level_data_show, s_wakeup );
				END
				current_control_profile = control_profile( cursor_control_profile );
				FRAME;
				REPEAT
				
					//FILE MANAGER
					IF (key( _f ) and !exists( type File_manager ))	
						WHILE (key( _f ))	FRAME; END
						IF ( exists( type Editor_cursor ) )
							editor_status = 1;
							signal( editor_cursor_id, s_freeze );
						END
						File_manager( 0 );
						
					//NEW LEVEL MENU
					ELSEIF (key( _e ) and !exists( type New_level_menu )) 
						WHILE (key( _e )) FRAME; END
						IF ( exists( type Editor_cursor ) )
							editor_status = 1;
							signal( editor_cursor_id, s_freeze );
						END
						New_level_menu( );
						
					//LEVEL DATA SHOW
					ELSEIF (key( _i )) 
						WHILE (key( _i )) FRAME; END
						IF (exists( type Level_data_show ))
							signal( type Level_data_show, s_kill );
						ELSE
							Level_data_show( );
						END
						
					//CONTROL PROFILE
					ELSEIF (BACKFIRE)
						//say( "Editor_control_program: BACKFIRE" );
						IF ( current_control_profile==1 )
							//say( "-Editor_control_program: profile 1 to 4" );
							current_control_profile = control_profile( 4 );
							cursor_control_profile = current_control_profile;
							drawing_map( 0, editor_cursor_id.graph );
							drawing_color( rgb( 255, 255, 0 ) );
							draw_rect( 1, 1, level.block_length-1, level.block_height-1 );
						ELSE  //current_control_profile==4
							//say( "-Editor_control_program: profile 4 to 1" );
							current_control_profile = control_profile( 1 );
							cursor_control_profile = current_control_profile;
							drawing_map( 0, editor_cursor_id.graph );
							drawing_color( rgb( 0, 255, 0 ) );
							draw_rect( 1, 1, level.block_length-1, level.block_height-1 );
						END
					END

					FRAME;
				UNTIL ( editor_status==1 )
			END
			CASE 1:
				say( "-Editor_control_program: B, editor_status=="+editor_status );
				REPEAT
					FRAME;
				UNTIL ( editor_status==0 )
			END
			DEFAULT:
				say( "-Editor_control_program: ERROR, editor_status=="+editor_status );
			END
		END
	END
END




//
//	PROCESS Level_data_show( )
//
//	Muestra información del nivel actual, leyendo (principalmente) valores del struct global level. Al ser llamado muestra los datos
//y entra en un bucle que lo primero que hace es congelar el proceso. Al ser descongelado borra los textos, los vuelve a mostrar
//con los datos actualizados y vuelve a congelar el proceso. Este proceso es terminado usando una señal con signal() (desde 
//Editor_control_program() ).
//
//PRIVATE
//	int i                        //Usado para iterar bucles
//	int text_id[]                //Guarda las ids de texto devueltas por write() y write_var() para poder borrar los textos
//	int array_of_pointers[]      //Direcciones de memoria de los datos a mostrar
//	int pointer global_option    //Puntero usado junto a write_var() para mostrar los valores de los datos. Toma valores de array_of_pointers[]
//	string data_labels[]         //Etiquetas de la información mostrada
//
//


PROCESS Level_data_show( )

PRIVATE
	int i;
	int text_id[_lds_elements*2-1];
	int array_of_pointers[_lds_elements-1];
	int pointer global_option;
	//string pointer s_global_option;
	string data_labels[] = "PLAYFIELD_I", "PLAYFIELD_J", "MAX_BLOCKS_I", "MAX_BLOCKS_J",
	                       "BLOCK_LENGTH", "BLOCK_HEIGHT", "BLOCKFIELD I", "BLOCKFIELD J";
END

BEGIN
	say( "-LEVEL DATA SHOW" );
	array_of_pointers[0] = &level.playfield_i;
	array_of_pointers[1] = &level.playfield_j;
	array_of_pointers[2] = &level.max_blocks_i;
	array_of_pointers[3] = &level.max_blocks_j;
	array_of_pointers[4] = &level.block_length;
	array_of_pointers[5] = &level.block_height;
	array_of_pointers[6] = &g_blockfield_i;
	array_of_pointers[7] = &g_blockfield_j;
		
	LOOP
		//DATA UPDATE
		FOR (i = 0; i<_lds_elements*2; i++)
			IF (text_id[i]!=0)
				say( "-Level_data_show: deleting text, text_id["+i+"]=="+text_id[i] );
				delete_text( text_id[i] );
			END
		END
		
		IF (level.playfield_i>_lds_min_x_padding)
			text_z = 0;  //vigilar con esto!
			FOR (i = 0; i<_lds_elements; i++)
				text_id[i] = write( 0, level.playfield_i+_lds_labels_x, i*_lds_line_height, 0, data_labels[i] );
				//say( "-Level_data_show: writing text, text_id["+i+"]=="+text_id[i] );
			END
			FOR (i = 0; i<_lds_elements; i++)
				global_option = array_of_pointers[i];
				text_id[i+_lds_elements] = write_var( 0, level.playfield_i+_lds_numbers_x, i*_lds_line_height, 0, *global_option );
				//say( "-Level_data_show: writing text, text_id["+(i+_lds_elements)+"]=="+text_id[(i+_lds_elements)] );
			END
		ELSE
			text_z = 0;  //vigilar con esto!
			FOR (i = 0; i<_lds_elements; i++)
				text_id[i] = write( 0, _lds_min_x_padding+_lds_labels_x, i*_lds_line_height, 0, data_labels[i] );
				//say( "-Level_data_show: writing text, text_id["+i+"]=="+text_id[i] );
			END
			FOR (i = 0; i<_lds_elements; i++)
				global_option = array_of_pointers[i];
				text_id[i+_lds_elements] = write_var( 0, _lds_min_x_padding+_lds_numbers_x, i*_lds_line_height, 0, *global_option );
				//say( "-Level_data_show: writing text, text_id["+(i+_lds_elements)+"]=="+text_id[(i+_lds_elements)] );
			END
		END
		
		signal( id, s_freeze );
		FRAME;  //al descongelarse actualiza el texto (por si se ha cargado un mapa con distinto playfield)
	END
ONEXIT
	FOR (i = 0; i<_lds_elements*2; i++)
		IF (text_id[i]!=0)
			//say( "-Level_data_show: deleting text, text_id["+i+"]=="+text_id[i] );
			delete_text( text_id[i] );
		END
	END
END




//
//	PROCESS Control_program_debug( int target_id )
//
//	Informa por consola de los cambios de estado (alive,sleeping,frozen...) del proceso cuya id es target_id
//
//


PROCESS Control_program_debug( int target_id )

PRIVATE
	int recorded_status;
END

BEGIN
	say( "CONTROL_PROGRAM_DEBUG" );
	LOOP
		IF ( get_status( target_id )!=recorded_status )
			recorded_status = get_status( target_id );
			say( "--Control_program_debug: New target_id-recorded_status=="+recorded_status );
		END
		
		FRAME;
	END
END