Hypnotic spiral effect.

Key q: Increase angluar speed
Key a: Decrease angular speed
Key w: Increase processing step
Key s: Decrease processing step
Key e: Increase distance between elements
Key d: Decrease distance between elements
Key c: Reset distance between elements