#define LEVEL_ARRAY_ELEMENTS 9999
#define KEYLOAD (key(_l)) 
#define KEYSAVE (key(_s)) 
#define KEYNEW (key(_n))



IMPORT "mod_key";
IMPORT "mod_video";
IMPORT "mod_say";
IMPORT "mod_proc";
IMPORT "mod_draw";
IMPORT "mod_map";
IMPORT "mod_string";
IMPORT "mod_file";
IMPORT "mod_dir";
IMPORT "mod_regex";
IMPORT "mod_text";
IMPORT "mod_debug";



CONST
	_lvlfile_version_id = 101012;			//para guardar la version del archivo

	_root_priority = 10;
	_keyboard_reader_priority = 5;
	_keyboard_filters_priority = 3;
	_control_program_priority = 1;
	
	_screenx = 640;
	_screeny = 480;
END


INCLUDE "controllers.inc";
INCLUDE "control_profiles.inc";



//
//	DATOS GLOBALES
//
//	struct level                            //Representa el nivel
//		int playfield_i, playfield_i            //Tamaño del playfield en horizontal y vertical. Viene dado por el archivo de nivel.
//		int max_blocks_i, max_blocks_j          //Maximo de bloques que caben en cada eje.
//		int block_length, block_height          //Tamaño del bloque en cada eje.
//		byte block_layout[]                     //Un array que tiene tantos elementos como bloques caben en el mapa actual
//		                                            en esta primera versión funciona de manera que 0 quiere decir que no hay bloque 
//		                                            y 1 que sí. El array es unidimensional, las posiciones se representan contando 
//		                                            primero en horizontal de derecha a izquierda y luego en vertical de arriba a abajo.
//	int g_blockfield_i, g_blockfield_j      //Tamaño de la parte del playfield que pueden cubrir los bloques en cada eje.
//	int editor_array_update                 //Valor que indica un cambio en el struct level. (1=hubo cambio en el último frame, 0=sin cambios)
//	int editor_status                       //Valor que indica si algún menú (File_manager() o New_level_menu()) está abierto a la vez que
//	                                            el editor.
//	int editor_cursor_id                    //Variable que guarda la id del proceso Editor_cursor() actual.
//	int file_version_code                   //Se usa para grabar la versión de la especificación de archivo (y leerla al cargar).
//	string last_loaded_file                 //Rellenados por el file_manager, guardan la ruta del ultimo nivel cargado y del ultimo nivel
//	string last_saved_file                  //guardado para escribirlo en pantalla y saber sobre qué archivos se trabaja.
//
//


GLOBAL
	struct level
		int playfield_i;
		int playfield_j;
		int max_blocks_i;
		int max_blocks_j;
		int block_length;
		int block_height;
		byte block_layout[LEVEL_ARRAY_ELEMENTS];
	end
	
	int g_blockfield_i;
	int g_blockfield_j;
	
	int editor_array_update;
	int editor_status;         //0=editor activo, 1=menus activos
	int editor_cursor_id;
	
	int file_version_code;
	
	string last_loaded_file;
	string last_saved_file;
END



INCLUDE "editor.inc";
INCLUDE "filemanager.inc";
INCLUDE "filemanager-gui.inc";
INCLUDE "newlevel-menu.inc";



PROCESS Main( )

BEGIN
	say( "MAIN4" );
	set_mode( _screenx, _screeny, 32 );
	priority = _root_priority;
	Keyboard_reader( );
	New_level_menu( );
	say( "Main: entering LOOP" );
	
	LOOP
		IF (!key( _esc ))
			IF ( key( _f1 ) )
				DEBUG;
			END
			FRAME;
		ELSE
			BREAK;
		END
	END
	let_me_alone( );
END