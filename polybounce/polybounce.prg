Import "mod_proc";
Import "mod_key";
Import "mod_video";
Import "mod_map";
Import "mod_rand";
Import "mod_text";
Import "mod_say";
Import "mod_math";
Import "mod_draw";

#define SPEED_H 0  //con el valor 1 se activa el speed hack, con el 0 se desactiva


Const

	screenx=640;
	screeny=480;
	//speed_h=1;

End


Global

	int dist=10;	
	int steps=4;
	int idpunto1;
	int idpunto2;
	int idpunto3;
	int idpunto4;
	//int colors;
	int vr;
	int vg;
	int vb=255;

End


/*Declare Process colorsource()

	Public

		int vr;
		int vg;
		int vb;

	End

End*/



Process Main()

Begin

	set_mode(screenx,screeny,32,MODE_DOUBLEBUFFER|MODE_WAITVSYNC);
	restore_type=COMPLETE_RESTORE;
	//dump_type=COMPLETE_DUMP;
	set_fps(60,0);

	idpunto1=Punto(rand(0,screenx/2),rand(0,screeny/2),rand(-4,4),rand(-4,4));
	idpunto2=Punto(rand(screenx/2,screenx),rand(0,screeny/2),rand(-4,4),rand(-4,4));
	idpunto3=Punto(rand(0,screenx/2),rand(screeny/2,screeny),rand(-4,4),rand(-4,4));
	idpunto4=Punto(rand(screenx/2,screenx),rand(screeny/2,screeny),rand(-4,4),rand(-4,4));
	
	colorsource();

#ifdef SPEED_H

	#if SPEED_H==1	

	door_speed();
	say("speed_H==1, proceso door");

	#else

	door();
	say("speed_H==0, proceso door_speed");

	#endif

#endif

	Loop

		If (key(_esc))
			let_me_alone();
			exit();
		End

		If (key(_i))
			infomap();
		End
	
		If (key(_o))
			delete_text(0);
		End

		FRAME;

	End

End


Process Punto(x,y,int dx,int dy)

Private

	bool checkx;
	bool checky;
	int i;
	float fangle;

End

Begin

	//graph=map_new(4,4,32);
	//map_clear(0,graph,rgb(255,0,0));

	/*write_var(0,0,0,0,dx);
	write_var(0,0,10,0,dy);
	write_var(0,20,0,0,x);
	write_var(0,20,10,0,y);*/

	If (dx==0)
		dx=1;
	End
	If (dy==0)
		dy=1;
	End

	For (i=1;i<steps;i++)
	
		say("control del for, id=="+id+" i=="+i);

		Clone

			fangle=atan2(dy,dx);
			x=x+(dist*cos(fangle))*i;
			y=y+(dist*sin(fangle))*i;

			i=steps+1;  //esto se pone porque los clones empiezan su codigo dentro del FOR, por lo tanto, mientras su "i" sea menor que "steps" crearan más clones hasta que salgan clones con el número 4 en su "i" que cuando vuelvan a iterar el FOR ya estarán fuera de la condición de iteración.
			say("punto creado, id=="+id);
			
		End

	End

	say("BEFORE LOOP id=="+id);

	Loop

		checkx=0;
		checky=0;

		If (x>screenx)
			x=screenx-x+screenx;
			dx=dx*(-1);
			
			checkx=1;
		End

		If (y>screeny)
			y=screeny-y+screeny;
			dy=dy*(-1);

			checky=1;
		End

		If (x<0)
			x=x*(-1);
			dx=dx*(-1);

			checkx=1;
		End

		If (y<0)
			y=y*(-1);
			dy=dy*(-1);
			
			checky=1;
		End

		If (checkx==0)
			x=x+dx;
		End

		If (checky==0)
			y=y+dy;
		End

		FRAME;

	End
			
End


Process colorsource()

Begin

	Loop

		For (vr=0;vr<255;vr++)
			vb--;
			FRAME;
		End
		For (vg=0;vg<255;vg++)
			vr--;
			FRAME;
		End
		For (vb=0;vb<255;vb++)
			vg--;
			FRAME;
		End

	End

End

//Este es otro ciclo para el color. vb tiene que empezar en 0 para que este funcione (ahora empieza en 255)

/*		For (vr=255;vr>0;vr--)
			vg++;
			FRAME;
		End
		For (vg=255;vg>0;vg--)
			vb++;
			FRAME;
		End
		For (vb=255;vb>0;vb--)
			vr++;
			FRAME;
		End*/


#ifdef SPEED_H

	#if SPEED_H==0

Process door()

Private

	int i;
	int cursor1;
	int cursor2;
	int cursor3;
	int cursor4;
	int zdraw=1;
	bool fill=0;
	bool alpha_enable=1;

End

Begin	

	say("proceso door()");

	Loop

		If (fill==0)
			delete_draw(0);
		End

		If (key(_c) AND fill==1)
			fill=0;
			say("c! fill=="+fill);
		End
		
		If (key(_f) AND fill==0)
			fill=1;
			say("f! fill=="+fill);
		End

		If (key(_a) AND alpha_enable==0)
			alpha_enable=1;
		End

		If (key(_z) AND alpha_enable==1)
			alpha_enable=0;
		End
			
		cursor1=idpunto1.son;
		cursor2=idpunto2.son;
		cursor3=idpunto3.son;
		cursor4=idpunto4.son;

		zdraw=1;

		For (i=steps;i>0;i--)  //no se si es 1 o es 0 pero pongo 1 porque supongo que será más fácil de ver si es erroneo.

			If (alpha_enable==1)			

				drawing_color(rgba(vr,vg,vb,(255/steps)*i));

			Else

				drawing_color(rgb(vr,vg,vb));

			End

			drawing_z(zdraw);
	
			draw_line(cursor1.x,cursor1.y,cursor2.x,cursor2.y);
			draw_line(cursor2.x,cursor2.y,cursor3.x,cursor3.y);
			draw_line(cursor3.x,cursor3.y,cursor4.x,cursor4.y);
			draw_line(cursor4.x,cursor4.y,cursor1.x,cursor1.y);

			If (cursor1.bigbro!=0)  //se comprueba para saber si hay que saltar ya al padre (el proceso de más atrás)	

				cursor1=cursor1.bigbro;	
				cursor2=cursor2.bigbro;	
				cursor3=cursor3.bigbro;	
				cursor4=cursor4.bigbro;	

			Else

				cursor1=cursor1.father;
				cursor2=cursor2.father;
				cursor3=cursor3.father;
				cursor4=cursor4.father;

			End

			zdraw=zdraw+1;

		End

		FRAME;

	End

End


	#else


Process door_speed()

Private

	int i;
	int cursor1;
	int cursor2;
	int cursor3;
	int cursor4;
	bool fill=0;
	bool alpha_enable=1;

End

Begin	

	say("proceso door_speed()");

	Loop

		If (fill==0)
			delete_draw(0);
		End

		If (key(_c) AND fill==1)
			fill=0;
			say("c! fill=="+fill);
		End
		
		If (key(_f) AND fill==0)
			fill=1;
			say("f! fill=="+fill);
		End

		If (key(_a) AND alpha_enable==0)
			alpha_enable=1;
		End

		If (key(_z) AND alpha_enable==1)
			alpha_enable=0;
		End
			
		cursor1=idpunto1.id;
		cursor2=idpunto2.id;
		cursor3=idpunto3.id;
		cursor4=idpunto4.id;
		
		If (alpha_enable==1)

			For (i=0;i<steps;i++)
	
				drawing_color(rgba(vr,vg,vb,(255/steps)*(i+1)));
	
				draw_line(cursor1.x,cursor1.y,cursor2.x,cursor2.y);
				draw_line(cursor2.x,cursor2.y,cursor3.x,cursor3.y);
				draw_line(cursor3.x,cursor3.y,cursor4.x,cursor4.y);
				draw_line(cursor4.x,cursor4.y,cursor1.x,cursor1.y);
					
				cursor1++;
				cursor2++;
				cursor3++;
				cursor4++;

			End

		Else

			For (i=0;i<steps;i++)	

				drawing_color(rgb(vr,vg,vb));
	
				draw_line(cursor1.x,cursor1.y,cursor2.x,cursor2.y);
				draw_line(cursor2.x,cursor2.y,cursor3.x,cursor3.y);
				draw_line(cursor3.x,cursor3.y,cursor4.x,cursor4.y);
				draw_line(cursor4.x,cursor4.y,cursor1.x,cursor1.y);
			
				cursor1++;
				cursor2++;
				cursor3++;
				cursor4++;
			
			End

		End

		FRAME;

	End

End

/*
Se han separado en dos bucles FOR, se activa uno o otro según alpha_enable. Así, comprueba el valor de alpha_enable una vez por frame en vez de en cada iteración del FOR. En principio así es más rápido pero hay que hacer más pruebas. (Esto de momento no está implementado en la versión normal)
*/

	#endif

#endif


Process infomap()

Begin

	write_var(0,0,20,0,idpunto1.id);
	If (idpunto1.smallbro!=0) write_var(0,50,20,0,idpunto1.smallbro); End
	If (idpunto1.bigbro!=0) write_var(0,100,20,0,idpunto1.bigbro); End
	If (idpunto1.son!=0) write_var(0,150,20,0,idpunto1.son); End

	write_var(0,0,30,0,idpunto2.id);
	If (idpunto1.smallbro!=0) write_var(0,50,30,0,idpunto2.smallbro); End
	If (idpunto1.bigbro!=0) write_var(0,100,30,0,idpunto2.bigbro); End
	If (idpunto1.son!=0) write_var(0,150,30,0,idpunto2.son); End

	write_var(0,0,40,0,idpunto3.id);
	If (idpunto1.smallbro!=0) write_var(0,50,40,0,idpunto3.smallbro); End
	If (idpunto1.bigbro!=0) write_var(0,100,40,0,idpunto3.bigbro); End
	If (idpunto1.son!=0) write_var(0,150,40,0,idpunto3.son); End

	write_var(0,0,50,0,idpunto4.id);
	If (idpunto1.smallbro!=0) write_var(0,50,50,0,idpunto4.smallbro); End
	If (idpunto1.bigbro!=0) write_var(0,100,50,0,idpunto4.bigbro); End
	If (idpunto1.son!=0) write_var(0,150,50,0,idpunto4.son); End

	write(0,0,0,0,"ID");
	write(0,50,0,0,"SMALLB");
	write(0,100,0,0,"BIGB");
	write(0,150,0,0,"SON");

	write(0,300,20,0,"proceso1");
	write(0,300,30,0,"proceso2");
	write(0,300,40,0,"proceso3");
	write(0,300,50,0,"proceso4");

	write_var(0,screenx,0,2,vr);
	write_var(0,screenx,10,2,vg);
	write_var(0,screenx,20,2,vb);

	write(0,screenx-30,0,2,"Rojo");
	write(0,screenx-30,10,2,"Verde");
	write(0,screenx-30,20,2,"Azul");

	write_var(0,screenx,screeny,8,fps);

	FRAME;

End