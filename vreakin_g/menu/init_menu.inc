CONST
	_init_menu_elements = 4;
END


//
//	PROCESS Init_menu( )
//
//


PROCESS Init_menu( )

PRIVATE
	int cursor;                                   //Opción seleccionada en el menú
	int text_ids[ _init_menu_elements-1 ];        //Guarda los ids de las cadenas de texto para poder borrarlas
	string option_name[ _init_menu_elements-1 ];  //Cadenas de texto a mostrar en el menu
END

BEGIN
	say( "INIT MENU" );

	current_control_profile = control_profile( 1 );
	
	//DATA WRITE
	
	option_name[ 0 ] = "GAME SETTINGS";
	option_name[ 1 ] = "LOAD LEVEL";
	option_name[ 2 ] = "GAMEPLAY EDIT";
	option_name[ 3 ] = "LEVEL GENERATION";

	say( "Init_menu: data written" );


	//MENU DRAW
	graph = map_new( 16, 16, 32 );
	map_clear( 0, graph, rgb( 255, 0, 0 ) );
	
	say( "Init_menu: map drawn" );


	text_z = 0;
	FOR ( cursor = 0 ; cursor<_init_menu_elements ; cursor++ )  //cursor es usado como i
		text_ids[ cursor ] = write( 0, HSX-30, BORDER_UP+32+20*cursor, 2, option_name[ cursor ] );
		//say( "-init_menu_text_ids["+cursor+"]=="+text_ids[cursor] );
		//say( "-init_menu_text_ids["+cursor+"]=="+option_name[cursor] );
	END
	
	say( "Init_menu: text written" );


	cursor = 0;		
	x = HSX+32;
	y = BORDER_UP+36+20*cursor;

	
	//VERTICAL MENU
	LOOP
	
		//PRIORITY CHECK
		//IF( PRIORITY_CHECK )
		//	say( "PRIORITY=="+priority+" id=="+id+" type INIT_MENU" );
		//END
	
		FRAME;

		//OPTION OK
		IF ( FIRE )
			BREAK;
		END 
		
		//OPTION CHANGE			
		IF ( KEYUP AND cursor>0 )
			cursor--;
		ELSEIF ( KEYDOWN AND cursor<_init_menu_elements-1 )
			cursor++;
		END
		y = BORDER_UP+36+20*cursor;
		
	END


	text_clearer( _init_menu_elements, &text_ids );
	/*
	FOR ( i = 0; i<_init_menu_elements*2; i++ )
				say( "-Init_menu: menu_info_clear: text_ids["+i+"]=="+text_ids[i] );
		IF ( option_name[ i ]!=0 )
			delete_text( text_ids[i] );
		END
	END
	*/

	map_unload( 0, graph );
		
	SWITCH ( cursor )
		CASE 0:
			Game_settings_menu( );
		END
		CASE 1:
			File_manager( );
		END
		CASE 2:
			Gameplay_edit_menu( );
		END
		CASE 3:
			New_level_menu( );
		END
		DEFAULT:
			say( "Init_menu: ERROR, option not found, cursor=="+cursor );
		END
	END
	
END