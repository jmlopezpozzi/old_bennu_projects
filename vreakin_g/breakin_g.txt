This project started by modding a breakout code example.

Features:
  High configurability:
    Screen resolution, screen orientation, playfield size, block size,
    and more can be changed during program runtime. Ball and paddle
    behaviour can be changed during gameplay.
  User made maps:
    The program can load maps previoulsy designed with the Bonedit
    program.
  Different kinds of blocks.
  Different kinds of paddle movement types.
  Initial implementation of power ups.
  A testing tool that is able to shoot balls from any position in the
  playfield in any of the configured valid angles.
  Controls that know how to deal with two opposing directions pressed.

In order to become a full game it needs:
  Finished implementation of power-ups.
  Winning state, level progression system and a scoring system.
  Proper presentation, graphics and sound.

The idea is to make the program able to let the user create a file that
defines game settings (from a finite set of options) and a sequence of
levels. This file can then be used by other players to play the game as
defined or to further edit it.