
//
//	PROCESS File_manager( bool loadonly )
//
//Proceso que permite cargar o grabar niveles. Puede buscar en los directorios del sistema de archivos, encontrar 
//archivos de nivel compatibles, cargarlos, grabar sobre ellos o crear un archivo nuevo en el directorio elegido.
//Puede funcionar sin GUI mediante mensajes en el terminal. bool loadonly==1 hace que no sea posible guardar o crear archivo.
//
//DATOS:
//	PUBLIC
//	int fm_status;          //El estado en el que se encuentra la operación del File_manager(). Este proceso se basa principalmente
//	                            //en un switch que elige rama según el valor de fm_status. Los estados son los siguientes:
//	                            // 0: NEW DIR, entra en un directorio, lee sus contenidos y rellena dos arrays de string con los 
//	                            //  contenidos del directorio clasificados como directorios encontrados dentro del directorio 
//	                            //  actual (found_dirs[]) y archivos de nivel encontrados en el directorio actual (found_files[])
//	                            // 1: DIR PICKER, permite elegir entre los directorios encontrados para acceder a ellos.
//	                            // 2: FILES PICKER, permite elegir entre los archivos de nivel encontrados para cargarlos o grabar	
//	                            //  sobre ellos. Desde aquí también se puede crear un nuevo archivo.
//	                            // 3: LOAD FILE, permite cargar el archivo seleccionado, pidiendo confirmación.
//	                            // 4: SAVE FILE, permite grabar sobre el archivo seleccionado, pidiendo confirmación.
//	                            // 5: NEW FILE, permite crear un nuevo archivo de nivel en el directorio actual. El nombre de archivo
//	                            //  es dado por el programa siguiendo un patrón e incrementando un valor.
//	                            // 6: BACK TO EDITOR, cierra el File_manager() volviendo al editor.
//	int num_of_files;       //Cantidad de archivos de nivel encontrados en el directorio actual.
//	int num_of_dirs;        //Cantidad de directorios encontrados en el directorio actual.
//	int files_cursor;       //Archivo elegido entre los encontrados.
//	int dirs_cursor;        //Directorio elegido entre los encontrados.
//	string found_files[]    //Lista con los nombres de archivo de nivel encontrados en el directorio actual.
//	string found_dirs[]     //Lista con los nombres de los directorios encontrados en el directorio actual.
//
//	PRIVATE
//	int dir_handle;         //Handle devuelto por diropen()
//	int file_num;           //Variable que se usa para conocer cual es el numero en el nombre de archivo de nivel.
//	int current_max_num;    //Junto a file_num se usa para saber cual es el número superior en los nombres de archivo de los archivos
//	                            //encontrados que sigan el patrón de nomenclatura. Esto es así porque los números en los nombres de archivo
//	                            //pueden no ser contiguos.
//	string filename_result; //Variable donde se guardan los resultados de diropen() en cada iteración antes de clasificarlos.
//	string new_filename;    //Variable donde se guarda el nombre de archivo correspondiente a la hora de crear un nuevo archivo de nivel.
//	string regex_pattern;   //Los archivos de nivel tienen un nombre que sigue este patrón, así es como el programa los reconoce como
//	                            //archivos de nivel. A la hora de crear archivos nuevos se sigue este patrón, incrementando la parte numerica
//	                            //partiendo del número más alto encontrado (no rellena huecos).
//
//


PROCESS File_manager( bool loadonly )

PUBLIC
	int fm_status;      //estado del file manager
	int num_of_files;
	int num_of_dirs;
	int files_cursor;
	int dirs_cursor;
	
	string found_files[999];
	string found_dirs[999];
END

PRIVATE
	int dir_handle;				
	int file_num;
	int current_max_num;
	int i;
	
	string filename_result;
	string new_filename;
	string regex_pattern = "^bglvl[[:digit:]][[:digit:]][[:digit:]].lvl$";
END

BEGIN
	say( "FILE_MANAGER" );

	current_control_profile = control_profile( 1 );
	LOOP
		SWITCH ( fm_status )
			CASE 0:  //NEW DIR
				dir_handle = diropen( "*" );
				IF (dir_handle==0)
					say( "File_manager: ERROR could not open directory" );
				ELSE
					FOR (i = 0; i<num_of_files; i++)
						found_files[i] = "";
					END
					FOR (i = 0; i<num_of_dirs; i++)
						found_dirs[i] = "";
					END
					files_cursor = 0;
					dirs_cursor = 0;
					num_of_files = 0;
					num_of_dirs = 0;
					LOOP
						filename_result = dirread( dir_handle );
						IF (filename_result=="")
							BREAK;
						END
						IF (regex( regex_pattern, filename_result )==0)
							found_files[num_of_files] = filename_result;
							num_of_files++;
						END
						IF (fileinfo.directory==1)
							found_dirs[num_of_dirs] = filename_result;
							num_of_dirs++;
						END
					END
					say( "File_manager: new directory lists written" );
					dirclose( dir_handle );
					
					IF (!exists( type Filemanager_gui ) )  //el gui se llama aquí sino al principio no muestra bien la informacion
						Filemanager_gui( loadonly, id );
					END
					
					FRAME;  //hace que el gui actualice la informacion al cambiar de directorio
					fm_status = 1;  //dirs picker
					
				END
			END
			
			CASE 1:  //DIR PICKER
				say( "File_manager: current directory:" );
				say( "-"+cd( ) );
				say( "File_manager: directory selection mode" );
				say( "-"+num_of_dirs+" directories found" );
				say( "-File_manager: selected directory: ("+(dirs_cursor+1)+"/"+num_of_dirs+")" );
				say( "--"+found_dirs[dirs_cursor] );
				FRAME;  //hace que no vuelva al editor cuando le damos a BACKFIRE en el prompt

				LOOP
					IF (KEYDOWN AND dirs_cursor<num_of_dirs-1)
						dirs_cursor++;
						say( "-File_manager: selected directory: ("+(dirs_cursor+1)+"/"+num_of_dirs+")" );
						say( "--"+found_dirs[dirs_cursor] );
						
					END
					IF (KEYUP AND dirs_cursor>0)
						dirs_cursor--;
						say( "-File_manager: selected directory: ("+(dirs_cursor+1)+"/"+num_of_dirs+")" );
						say( "--"+found_dirs[dirs_cursor] );
							
					END
					IF (KEYRIGHT)
						fm_status = 2;  //files picker
						BREAK;
					END
					IF (FIRE)
						chdir( found_dirs[dirs_cursor] );
						fm_status = 0;  //list builder
						BREAK;
					END
					IF (BACKFIRE)
						fm_status = 6;  //back to editor
						BREAK;
					END
					
					FRAME;
				END
			END
			
			CASE 2:  //FILE PICKER
				say( "File_manager: current directory:" );
				say( "-"+cd( ) );
				say( "File_manager: file selection mode" );
				FRAME;  //hace que no vuelva al editor cuando le damos a BACKFIRE en el prompt

				IF (num_of_files==0)
					say( "File manager: no files found in current directory" );  //ojo! aunque no haya archivos se tiene que poder crear uno nuevo
				ELSE
					say( "-"+num_of_files+" files found" );
					say( "-File_manager: selected file: ("+(files_cursor+1)+"/"+num_of_files+")" );
					say( "--"+found_files[files_cursor] );	
				END

				LOOP
					IF (KEYDOWN AND files_cursor<num_of_files-1)
						files_cursor++;
						say( "-File_manager: selected file: ("+(files_cursor+1)+"/"+num_of_files+")" );
						say( "--"+found_files[files_cursor] );							
					END
					IF (KEYUP AND files_cursor>0)
						files_cursor--;
						say( "-File_manager: selected file: ("+(files_cursor+1)+"/"+num_of_files+")" );
						say( "--"+found_files[files_cursor] );							
					END
					IF (KEYLEFT)			
						fm_status = 1;  //dirs picker
						BREAK;
					END
					IF (num_of_files>0 AND KEYLOAD)
						fm_status = 3;  //load file
						BREAK;
					END
					IF ( loadonly==0 )
						IF (num_of_files>0 AND KEYSAVE)
							fm_status = 4;  //save file
							BREAK;
						END
						IF (KEYNEW)
							fm_status = 5;  //new file
							BREAK;
						END
					END
					IF (BACKFIRE)
						fm_status = 6;  //back to editor
						BREAK;
					END
						
					FRAME;
				END
			END
			
			CASE 3:  //LOAD FILE
				say( "File_manager: LOAD file "+found_files[files_cursor] );
				say( "-current editor data will be replaced, please confirm" );

				LOOP
					IF (FIRE)  //confirm
						dir_handle = fopen( found_files[files_cursor], O_READ );  //dir_handle hace de file_handle
						fseek( dir_handle, 0, SEEK_SET );
						i = fread( dir_handle, file_version_code );

						IF (file_version_code==_lvlfile_version_id)  //version check
							fseek( dir_handle, sizeof( file_version_code ), SEEK_SET );
							i = i+fread( dir_handle, level );
							fclose( dir_handle );
							last_loaded_file = cd( )+"/"+found_files[files_cursor];

							say( "--"+i+" bytes read from file "+found_files[files_cursor] );									
							say( "File_manager: stopping and restarting editor" );

							IF (editor_status==1)
								stop_editor( );
								start_editor( );
							END

							fm_status = 6;  //back to editor
							BREAK;
						ELSE
							say( "-File_manager: ERROR, level file version not compatible, file_version_code=="+file_version_code );
							fclose( dir_handle );
							i = 0;
							fm_status = 2;
							BREAK;
						END						
					END
					IF (BACKFIRE)  //decline
						fm_status = 2;  //files picker
						BREAK;
					END
					
					FRAME;
				END
			END
			
			CASE 4:  //SAVE FILE
				say( "File_manager: SAVE file "+found_files[files_cursor] );
				say( "-previous file data will be overwritten, please confirm" );

				LOOP
					IF (FIRE)  //confirm
						file_version_code = _lvlfile_version_id;
						
						dir_handle = fopen( found_files[files_cursor], O_WRITE );
						fseek( dir_handle, 0, SEEK_SET );
						i = fwrite( dir_handle, file_version_code );
						fseek( dir_handle, sizeof( file_version_code ), SEEK_SET );
						i = i+fwrite( dir_handle, level );
						fclose( dir_handle );
						
						last_saved_file = cd( )+"/"+found_files[files_cursor];
						say( "--"+i+" bytes written to file "+found_files[files_cursor] );
						fm_status = 6;  //back to editor
						BREAK;
					END
					IF (BACKFIRE)  //decline
						fm_status = 2;  //files picker
						BREAK;
					END
					
					FRAME;
				END
			END
			
			CASE 5:  //NEW FILE
				say( "File_manager: NEW file" );
				IF (num_of_files==0)
					new_filename = "bglvl000.lvl";
					say( "-new file with name "+new_filename+" will be created, please confirm" );

					LOOP
						IF (FIRE)  //confirm
							file_version_code = _lvlfile_version_id;
						
							dir_handle = fopen( new_filename, O_WRITE );
							fseek( dir_handle, 0, SEEK_SET );
							i = fwrite( dir_handle, file_version_code );
							fseek( dir_handle, sizeof( file_version_code ), SEEK_SET );
							i = i+fwrite( dir_handle, level );
							fclose( dir_handle );
							
							last_saved_file = cd( )+"/"+new_filename;
							say( "--"+i+" bytes written to file "+new_filename );
							fm_status = 6;  //back to editor
							BREAK;
						END
						IF (BACKFIRE)  //decline
							fm_status = 2;  //files picker
							BREAK;
						END
						
						FRAME;
					END
				ELSE
					current_max_num = 0;
					FOR ( i = 0; i<num_of_files; i++ )
						file_num = atoi( substr( found_files[i], 5, 3 ) );
						IF (file_num>current_max_num)
							current_max_num = file_num;
						END
					END

					say( "-current max number in directory: "+current_max_num );

					SWITCH (current_max_num)
						CASE 0..8:
							new_filename = "bglvl00"+itoa( current_max_num+1 )+".lvl";
						END
						CASE 9..98:
							new_filename = "bglvl0"+itoa( current_max_num+1 )+".lvl";
						END
						CASE 99..998:
							new_filename = "bglvl"+itoa( current_max_num+1 )+".lvl";
						END
						DEFAULT:
							say( "-File_manager: could not create file in directory" );
							say( "--file with number 999 detected" );
							fm_status = 2;  //files picker (rompe el loop global y vuelve al editor sin avisar)
						END
					END
					IF (fm_status==5)
						say( "-new file with name "+new_filename+" will be created, please confirm" );
					END

					LOOP
						IF (fm_status==2)
							BREAK;  //se hace así para que en el caso del DEFAULT (current_max_num==99) vuelva al files_pciker
						END
						IF (FIRE)  //confirm
							file_version_code = _lvlfile_version_id;
							
							dir_handle = fopen( new_filename, O_WRITE );
							fseek( dir_handle, 0, SEEK_SET );
							i = fwrite( dir_handle, file_version_code );
							fseek( dir_handle, sizeof( file_version_code ), SEEK_SET );
							i = i+fwrite( dir_handle, level );
							fclose( dir_handle );
							
							last_saved_file = cd( )+"/"+new_filename;
							say( "--"+i+" bytes written to file "+new_filename );
							fm_status = 6;  //back to editor
							BREAK;
						END
						IF (BACKFIRE)  //decline
							fm_status = 2;  //files picker
						END
						
						FRAME;
					END
				END
			END
			
			CASE 6:  //BACK TO EDITOR
				say( "File_manager: exiting, back to editor" );
				IF (editor_status==1)
					editor_array_update = 1;
					FRAME;  //impide que el editor registre la pulsacion de tecla
					BREAK;
				ELSE   //editor_status==0, first run
					IF (level.playfield_i==0)
						//en caso de que no se haya cargado ningun archivo
						New_level_menu( );     //vuelve a New_level_menu() (first run)
						FRAME;
						BREAK;
					ELSE
						//en caso de que se haya cargado un archivo (level.playfield_i!=0)
						say( "File_Manager: starting editor" );
						start_editor( );  //vamos al editor
						editor_array_update = 1;
						FRAME;
						BREAK;
					END
				END
			END
		END
	END
ONEXIT
	editor_status = 0;
END