#DEFINE KEYB_L key( _left )
#DEFINE KEYB_R key( _right )
#DEFINE KEYB_U key( _up )
#DEFINE KEYB_D key( _down )
#DEFINE KEYB_FIRE key( _l_control )
#DEFINE KEYB_BFIRE key( _l_alt )

#DEFINE KEYLEFT controller[ 0 ]
#DEFINE KEYRIGHT controller[ 1 ]
#DEFINE KEYUP controller[ 2 ]
#DEFINE KEYDOWN controller[ 3 ]
#DEFINE FIRE controller[ 4 ]
#DEFINE BACKFIRE controller[ 5 ]


GLOBAL
	bool controller_source[ 5 ];
	bool controller[ 5 ];         //0==L, 1==R, 2==U, 3==D, 4==FIRE, 5==BACKFIRE
	bool controller_axis[ 2 ];    //para el modo Key_pair_exclusive
END



//
//PROCESS Keyboard_reader( )
//
//Lee que teclas de juego (definidas por macros) se están pulsando y llena la tabla controller_source[]
//Varios filtros procesan la información de controller_source[] para rellenar la tabla controller[]
//que es la que se usará para controlar el programa. Cada filtro hace que una tecla o conjunto de ellas
//funcione de una forma u otra.
//


PROCESS Keyboard_Reader( )

BEGIN

	say( "Keyboard_Reader( ) activo" );
	priority = _keyboard_reader_priority;
	LOOP
		IF ( KEYB_L )
			controller_source[ 0 ] = 1;
		ELSE
			controller_source[ 0 ] = 0;
		END
		
		IF ( KEYB_R )
			controller_source[ 1 ] = 1;
		ELSE
			controller_source[ 1 ] = 0;
		END
		
		IF ( KEYB_U )
			controller_source[ 2 ] = 1;
		ELSE
			controller_source[ 2 ] = 0;
		END
		
		IF ( KEYB_D )
			controller_source[ 3 ] = 1;
		ELSE
			controller_source[ 3 ] = 0;
		END

		IF ( KEYB_FIRE )
			controller_source[ 4 ] = 1;
		ELSE
			controller_source[ 4 ] = 0;
		END
		
		IF ( KEYB_BFIRE )
			controller_source[ 5 ] = 1;
		ELSE
			controller_source[ 5 ] = 0;
		END
		
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type KEYBOARD_READER" );
		END
		
		FRAME;
	END
ONEXIT
	say( "Keyboard_reader "+id+" saliendo" );
END



//
//PROCESS Key_menu_mode( byte key1 )
//
//Controla el dato que se escribirá en controller[key1] de manera que este sólo sea 1 en el 
//primer frame que se pulse la tecla que se corresponda a key1 (siendo el resultado 0 en los
//siguientes frames aunque se deje apretada la tecla). key1 es un número que se corresponde
//con las constantes que se están usando para identificar cada tecla.
//


PROCESS Key_menu_mode( byte key1 )

PRIVATE
	bool status_signal;
	bool previous_status_signal;
END

BEGIN
	priority = _keyboard_filters_priority;
	LOOP
		IF ( controller_source[ key1 ] != 0 )
			status_signal = 1;
		
			IF ( status_signal == 1 AND previous_status_signal == 0 )
				controller[ key1 ] = 1;
			ELSE
				controller[ key1 ] = 0;
			END
		ELSE
			status_signal = 0;
			controller[ key1 ] = 0;  //habria que backportar esta línea a falcon
		END
		
		previous_status_signal = status_signal;
		
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type KEY_MENU_MODE" );
		END
		
		FRAME;
	END
ONEXIT
	say( "Key_menu_mode "+id+" saliendo" );
END



//
//PROCESS Key_pair_exclusive( byte key1, byte key2, byte axis )
//
//Controla los valores que tendrán controller[key1] y controller[key2]
//de manera que sean exclusivos. Pueden valer o los dos 0, o uno de los dos
// 1, pero no pueden valer los dos 1 a la vez. En caso de apretarse las dos
//teclas a la vez prevalece la última que se apretó.
//


PROCESS Key_pair_exclusive( byte key1, byte key2, byte axis )

PRIVATE
	bool status_signal;
	bool previous_status_signal;
END

BEGIN
	priority = _keyboard_filters_priority;
	LOOP
		IF ( controller_source[ key1 ] == 1 OR controller_source[ key2 ] == 1 )
			controller_axis[ axis ] = 1;
			
			IF ( controller_source[ key1 ] == 1 AND controller_source[ key2 ] == 1 )
				status_signal = 1;
			ELSE
				status_signal = 0;
				
				IF ( controller_source[ key1 ] == 1 )
					controller[ key1 ] = 1;
					controller[ key2 ] = 0;
				ELSE
					controller[ key1 ] = 0;
					controller[ key2 ] = 1;
				END
			END
			
			IF ( status_signal == 1 AND previous_status_signal == 0 )
				IF ( controller[ key1 ] == 1 )
					controller[ key1 ] = 0;
					controller[ key2 ] = 1;
				ELSE
					controller[ key1 ] = 1;
					controller[ key2 ] = 0;
				END
			END
			
			previous_status_signal = status_signal;
			
		ELSE
			controller[ key1 ] = 0;
			controller[ key2 ] = 0;
			controller_axis[ axis ] = 0;
		END
		
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type KEY_PAIR EXCLUSIVE" );
		END
		
		FRAME;
	END
ONEXIT
	say( "Key_pair_exclusive "+id+" saliendo" );
END



PROCESS Key_bypass_mode( byte key1 )

BEGIN
	priority = _keyboard_filters_priority;
	LOOP
		controller[ key1 ] = controller_source[ key1 ];
		
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type KEY_BYPASS_MODE" );
		END
		
		FRAME;
	END
ONEXIT
	say( "Key_bypass_mode "+id+" saliendo" );
END