//GENFUNC.INC
//Funciones de generación, regeneración y borrado de elementos del juego. Funciones de apoyo.

/*
CONST
	_ball_graph = 0;
	_paddle_graph = 256;
	_dbs_graph = 257;
	_dbs2_graph = 258;
END
*/  //declaradas en main


CONST
	//significados de ag3 cuando ag2==6
	_pow_base_up_case = 0;
	_pow_base_dwn_case = 1;
	_pow_mod_value_case = 2;
	_pow_base_up_all_case = 3;
	_pow_base_dwn_all_case = 4;
	_pow_mod_value_all_case = 5;

	//significados de ag3 cuando ag2==7 AND ag1==0
	_spd_base_up_case = 0;
	_spd_base_dwn_case = 1;
	_spd_mod_more_case = 2;
	_spd_mod_less_case = 3;
	_spd_base_up_all_case = 4;
	_spd_base_dwn_all_case = 5;
	_spd_mod_more_all_case = 6;
	_spd_mod_less_all_case = 7;
END


//
// FUNCTION graphics_generation()
// FUNCTION rt_graphics_generation()
//
// Crea un nuevo fpg que guarda en idfpg. Dibuja los gráficos en mapas que va añadiendo a este fpg.
//1:ball 2:block 3:paddle 4:debug_ball_spawn 5:debug_ball_spawn (modo 2)
//Para el modo rotado crea gráficos rotados directamente, puesto que rotarlos cambiando el valor
//de angle crea artefactos gráficos (errores, irregularidades).
//
// graphics_generation() crea el fpg en idfpg, rt_graphics_generation() lo crea en idfpg_rt. El motivo de tenerlos separados es 
//que, debido a que Gameplay_edit_menu() se puede invocar durante la partida, las variables que rigen la pala y las bolas (y sus
//gráficos) pueden ser alteradas en medio de la partida. Esto hace que haya que volver a generar algunos gráficos. El tener separados
//los gráficos que cambian solo durante Init (idfpg) de los que pueden cambiar también durante la partida (idfpg_rt) hace que 
//se puedan actualizar estos ultimos (mediante rt_graphics_generation()) sin afectar los primeros. En el caso de que el fpg fuera
//único habría que volver a generar el nivel o de lo contrario los gráficos que solo cambian durante init perderian gu graph al
//apuntar a un file que ha sido descargado (ambas funciones descargan de memoria instancias generadas previamente).
//
// Hay un bug en BennuGD (al menos hasta la version 339) en la que el primer fpg creado con fpg_new da como file 0. Esto es un error
//ya que file==0 es el archivo de gráficos del sistema. Al querer descargar este fpg con file id 0 suceden problemas gráficos 
//(creo recordar que se borran todos los gráficos). Para contrarrestar esto, estas dos funciones usan un hack algo guarro: si ven
//que el fpg creado tiene el código 0, dejan ese fpg vacío y crean otro. Por otro lado, en el momento de descargar el fpg comprueban
//si su código es 0, y si es así no lo descargan. De esta manera se evita que idfpg e idfpg_rt valgan 0. Por otro lado, el código 
//de los fpg parece ir aumentando en una unidad cada vez que se crea uno (sin decrementar al descargarlos.)
//
// Finalmente, en esta versión del programa, los dos fpg se crean iguales para simplificar su gestión (es decir, tanto idfpg como
//idfpg_rt contienen todos los gráficos).
//
//


FUNCTION graphics_generation( )

PRIVATE
	int temp_map;
	int i;
END

BEGIN
	say( "graphics_generation" );

	IF (idfpg!=0 AND fpg_exists( idfpg ))
		say( "-graphics_generation: previous idfpg=="+idfpg+" found, unloading" );
		fpg_unload( idfpg );
	ELSEIF (idfpg==0)
		say( "-graphics_generation: idfpg=="+idfpg+", skipping unload" );
	END
	
	idfpg = fpg_new( );
	say( "-graphics_generation: created fpg idfpg=="+idfpg );

	IF ( idfpg==0 )
		idfpg = fpg_new( );  //hack horrible, hace que reemplace el valor 0
		say( "-graphics_generation: idfpg=="+idfpg+", creado fpg vacio, hack" );
	END


	//BALL
	fpg_add( idfpg, _ball_graph, 0, map_new( gameplay.BALL_SIZE, gameplay.BALL_SIZE, 32 ) );
	drawing_map( idfpg, _ball_graph );
	drawing_color( rgb( 255, 255, 0 ) );
	draw_fcircle( H_BALLSIZE, H_BALLSIZE, H_BALLSIZE-1 );
	

	//GRAFICOS sensibles a la ROTACION
	IF ( rotacion>=0 )
		//BLOCK
		FOR ( i = 1; i<256; i++ )
			fpg_add( idfpg, i, 0, map_new( level.BLOCK_LENGTH, level.BLOCK_HEIGHT, 32 ) );
			drawing_map( idfpg, i );
			drawing_color( rgb( 0, 0, 255 ) );
			draw_box( 0, 0, level.BLOCK_LENGTH, level.BLOCK_HEIGHT );
			drawing_color( rgb( 0, 255, 0 ) );
			draw_rect( 0, 0, level.BLOCK_LENGTH-1, level.BLOCK_HEIGHT-1 );
			temp_map = write_in_map( 0, i, 4 );
			map_put( idfpg, i, temp_map, level.block_length/2, level.block_height/2  );
			map_unload( 0, temp_map );
		END

		//PADDLE
		fpg_add( idfpg, _paddle_graph, 0, map_new( gameplay.PADdle_size, PAD_HEIGHT, 32 ) );
		map_clear( idfpg, _paddle_graph, rgb( 255, 0, 0 ) );
	ELSE
		//BLOCK
		FOR ( i = 1; i<256; i++ )
			fpg_add( idfpg, i, 0, map_new( level.BLOCK_HEIGHT, level.BLOCK_LENGTH, 32 ) );
			drawing_map( idfpg, i );
			drawing_color( rgb( 0, 0, 255 ) );
			draw_box( 0, 0, level.BLOCK_HEIGHT, level.BLOCK_LENGTH );
			drawing_color( rgb( 0, 255, 0 ) );
			draw_rect( 0, 0, level.BLOCK_HEIGHT-1, level.BLOCK_LENGTH-1 );
			temp_map = write_in_map( 0, i, 4 );
			map_put( idfpg, i, temp_map, level.block_length/2, level.block_height/2  );
			map_unload( 0, temp_map );
		END

		//PADDLE
		fpg_add( idfpg, _paddle_graph, 0, map_new( PAD_HEIGHT, gameplay.paddle_size, 32 ) );
		map_clear( idfpg, _paddle_graph, rgb( 255, 0, 0 ) );
	END
	
	//DEBUG_BALL_SPAWN
	fpg_add( idfpg, _dbs_graph, 0, map_new( gameplay.ball_size, gameplay.ball_size, 32 ) );
	drawing_map( idfpg, _dbs_graph );
	drawing_color( rgb( 0, 127, 127 ) );
	draw_fcircle( H_BALLSIZE-1, H_BALLSIZE-1, H_BALLSIZE-2 );
	
	//DEBUG_BALL_SPAWN (ALT)
	fpg_add( idfpg, _dbs2_graph , 0, map_new( gameplay.ball_size, gameplay.ball_size, 32 ) );
	drawing_map( idfpg, _dbs2_graph );
	drawing_color( rgb( 127, 127, 0 ) );
	draw_fcircle( H_BALLSIZE-1, H_BALLSIZE-1, H_BALLSIZE-2 );

	//POWER UP GENERIC
	fpg_add( idfpg, _pwup_graph, 0, map_new( h_block_length, h_block_height, 32 ) );
	map_clear( idfpg, _pwup_graph, rgb( 255, 0, 255 ) );
	temp_map = write_in_map( 0, "PW", 4 );
	map_put( idfpg, _pwup_graph, temp_map, h_block_length/2, h_block_height/2 );
	map_unload( 0, temp_map );

END



FUNCTION rt_graphics_generation( )

BEGIN
	say( "rt_graphics_generation" );

	IF ( idfpg_rt!=0 AND fpg_exists( idfpg_rt ) )
		say( "-rt_graphics_generation: previous idfpg_rt=="+idfpg_rt+" found, unloading" );
		fpg_unload( idfpg_rt );
	ELSEIF (idfpg_rt==0)
		say( "-rt_graphics_generation: idfpg_rt=="+idfpg_rt+", skipping unload" );
	END
	
	idfpg_rt = fpg_new( );
	say( "-rt_graphics_generation: created fpg idfpg_rt=="+idfpg_rt );

	IF ( idfpg_rt==0 )
		idfpg_rt = fpg_new( );  //hack horrible, hace que reemplace el valor 0
		say( "-rt_graphics_generation: idfpg_rt=="+idfpg_rt+", creado idfpg0 vacio, hack" );
	END
	
	//BALL
	fpg_add( idfpg_rt, _ball_graph, 0, map_new( gameplay.BALL_SIZE, gameplay.BALL_SIZE, 32 ) );
	drawing_map( idfpg_rt, _ball_graph );
	drawing_color( rgb( 255, 255, 0 ) );
	draw_fcircle( H_BALLSIZE, H_BALLSIZE, H_BALLSIZE-1 );
	
	//PADDLE
	IF ( rotacion<0 )
		fpg_add( idfpg_rt, _paddle_graph, 0, map_new( PAD_HEIGHT, gameplay.paddle_size, 32 ) );
		map_clear( idfpg_rt, _paddle_graph, rgb( 255, 0, 0 ) );
	ELSE
		fpg_add( idfpg_rt, _paddle_graph, 0, map_new( gameplay.PADdle_size, PAD_HEIGHT, 32 ) );
		map_clear( idfpg_rt, _paddle_graph, rgb( 255, 0, 0 ) );
	END
END




//
// FUNCTION map_generation( )
// PROCESS map_generation( )
//
// Llena la parte superior de la pantalla de bloques. Está programada de forma que el area ocupada por los bloques
//venga dada por lower_margin_ratio del New_level_menu, y el tamaño de los bloques
// En forma de función no usa frame e incrementa blocksleft en el momento de cada llamada a Block_put().
// En forma de proceso, usando FRAME tras cada llamada a Block_put(), vemos como se crea el mapa progresivamente. Esto hace que
//blocksleft tenga que ser calculado antes para que la cuenta total no se joda si hacemos reset, undo o redo desde el 
//Control_program() mientras el mapa se está creando.
// Intenta centrar el nivel generado, en caso de que la cantidad de bloques por su longitud no llenen el playfield_j (usando
//la veriable privada blockfield_i).
//


FUNCTION map_generation( )

PRIVATE
	int i;
	int j;
	int blockfield_i;  //se usa para centrar horizontalmente en caso de que el blockfield_i sea inferior al playfield_i
END

BEGIN
	say( "map_generation" );			

	blockfield_i = level.max_blocks_i*level.block_length;
	blockfield_i = (level.playfield_i-blockfield_i)/2;

	FOR ( i=0; i < level.max_blocks_i; i++ )
		FOR ( j=0; j < level.max_blocks_j; j++ )
			blocksleft++;  //incrementa blocksleft cada vez que crea una instancia (da problemas si creamos bloque a bloque)
			Block_put( (i*level.block_length+h_block_length+blockfield_i)*1.0, (j*level.block_height+h_block_height)*1.0, 1 );
			//FRAME;  //si map_generation() es un proceso genera el mapa bloque a bloque
		END
	END

	file_version_code = 22;  //se usa en Control_program() para saber que el mapa ha sido generado y no cargado, y permitir usar RESET MAP
	redo_limit = blocksleft;
	undo_limit = blocksleft;
END




// FUNCTION level_render( byte pointer p_level_layout )
//
// Pone en el playfield los bloques de un nivel cargado a partir de un archivo. Recorre el array level.block_layout[] (apuntado
//por el puntero byte pointer p_level_layout que recibe como parámetro) y para los elementos en los que encuentra un bloque pone
//estos en el lugar del playfield donde corresponda (deduce su lugar en la cuadrícula usando las funciones find_column() y
//find_row()).
// Intenta centrar el nivel generado, en caso de que la cantidad de bloques por su longitud no llenen el playfield_j (usando
//la variable privada margin_i).
//


FUNCTION level_render( byte pointer p_level_layout )

PRIVATE
	int i;
	int margin_i;  //se usa para centrar horizontalmente en caso de que el blockfield_i sea inferior al playfield_i
END

BEGIN
	//say( "level_render: p_level_layout=="+p_level_layout );
	
	margin_i = level.max_blocks_i*level.block_length;  //calculo de blocksfield
	margin_i = (level.playfield_i-margin_i)/2;
			
	FOR ( i = 0; i<=LEVEL_ARRAY_ELEMENTS; i++ )
		IF ( p_level_layout[i]>0 )
			//say( "-level_render: bloque encontrado, i=="+i );
			Block_put( margin_i+((find_column( i )*level.block_length)*1.0)+h_block_length, 
			                    ((find_row( i )*level.block_height)*1.0)+h_block_height, p_level_layout[i] );
			blocksleft++;
		END
	END
	
	redo_limit = blocksleft;
	undo_limit = blocksleft;
END



//
// FUNCTION block_put( float i_pos_data, float j_pos_data, byte block_code )
//
// Pone un bloque en la posición dada por i_pos_data y j_pos_data. Usa el dato block_code para saber qué bloque hay que poner
//y con qué parametros y invocar al proceso pertinente.
//
//


FUNCTION block_put( float i_pos_data, float j_pos_data, byte block_code )

PRIVATE
	//int block_type;
	//int energy;       //tal vez se pueda enviar el parametro en cuestion calculandolo al vuelo al llamar al proceso hijo
	int ag1;
	int ag2;          //block_type
	int ag3;          //energy

	int pwup_type;
	int pwup_value;
END

BEGIN
	IF ( file_version_code==101012 )  //Archivo de Bonedit4

		ag1 = block_code>>5;
		ag2 = (block_code-(ag1<<5))>>3;
		ag3 = block_code-(ag2<<3)-(ag1<<5);

		SWITCH (ag1)
			CASE 0:
				IF (ag3>0) Block_normal( i_pos_data, j_pos_data, block_code );   END
			END
			CASE 1:
				IF (ag3>0) Block_regen( i_pos_data, j_pos_data, block_code );    END
			END
			CASE 2:
				IF (ag3>0) Block_shield( i_pos_data, j_pos_data, block_code );   END
			END
			CASE 3:
				IF (ag3>0) Block_blink( i_pos_data, j_pos_data, block_code );    END
			END
			CASE 4:		
				IF (ag3>0) Block_blink_sh( i_pos_data, j_pos_data, block_code ); END
			END

			CASE 5:  Block_unbreak( i_pos_data, j_pos_data, block_code );  END

			CASE 6:  //ball_pow power up
				IF (ag3 == _pow_base_up_case)           pwup_type = _pow_base_up_msg;           say("--pow_up");
				ELSEIF (ag3 == _pow_base_dwn_case)      pwup_type = _pow_base_dwn_msg;          say("--pow_dwn");
				ELSEIF (ag3 == _pow_mod_value_case)     pwup_type = _pow_mod_value_msg;         say("--pow_mod");
				ELSEIF (ag3 == _pow_base_up_all_case)   pwup_type = _pow_base_up_all_msg;       say("--pow_upall");
				ELSEIF (ag3 == _pow_base_dwn_all_case)  pwup_type = _pow_base_dwn_all_msg;      say("--pow_dwnall");
				ELSEIF (ag3 == _pow_mod_value_all_case) pwup_type = _pow_mod_value_all_msg;     say("--pow_modall");
				ELSE
					say( "Block_put: ERROR case 6, undefined block value" );
					say( "--block_code=="+block_code+" ag1=="+ag1+" ag2=="+ag2+" ag3=="+ag3 );
				END

				say("--pwup_type=="+pwup_type);

				pwup_value = ball_pow_values[ag2];  //el valor de ball_pow viene dado por ag2
				Block_power_up( i_pos_data, j_pos_data, pwup_type, pwup_value, block_code );
			END

			CASE 7:
				SWITCH (ag2)
					CASE 0:
						IF (ag3 == _spd_base_up_case)           pwup_type = _spd_base_up_msg;       pwup_value = _spd_base_step;    say("---case "+ag3+" pwup_type=="+pwup_type);
						ELSEIF (ag3 == _spd_base_dwn_case)      pwup_type = _spd_base_dwn_msg;      pwup_value = -_spd_base_step;   say("---case "+ag3+" pwup_type=="+pwup_type);
						ELSEIF (ag3 == _spd_mod_more_case)      pwup_type = _spd_mod_more_msg;      pwup_value = _spd_mod_step;     say("---case "+ag3+" pwup_type=="+pwup_type);
						ELSEIF (ag3 == _spd_mod_less_case)      pwup_type = _spd_mod_less_msg;      pwup_value = -_spd_mod_step;    say("---case "+ag3+" pwup_type=="+pwup_type);
						ELSEIF (ag3 == _spd_base_up_all_case)   pwup_type = _spd_base_up_all_msg;   pwup_value = _spd_base_step;    say("---case "+ag3+" pwup_type=="+pwup_type);
						ELSEIF (ag3 == _spd_base_dwn_all_case)  pwup_type = _spd_base_dwn_all_msg;  pwup_value = -_spd_base_step;   say("---case "+ag3+" pwup_type=="+pwup_type);
						ELSEIF (ag3 == _spd_mod_more_all_case)  pwup_type = _spd_mod_more_all_msg;  pwup_value = _spd_mod_step;     say("---case "+ag3+" pwup_type=="+pwup_type);
						ELSEIF (ag3 == _spd_mod_less_all_case)  pwup_type = _spd_mod_less_all_msg;  pwup_value = -_spd_mod_step;    say("---case "+ag3+" pwup_type=="+pwup_type);
						ELSE
							say( "Block_put: ERROR case 7-0, undefined block value" );
							say( "--block_code=="+block_code+" ag1=="+ag1+" ag2=="+ag2+" ag3=="+ag3 );
						END
					END

					DEFAULT:
						say( "Block_put: ERROR case 7-n, undefined block value" );
						say( "--block_code=="+block_code+" ag1=="+ag1+" ag2=="+ag2+" ag3=="+ag3 );
					END
				END
				say("---pwup_type=="+pwup_type);
				Block_power_up( i_pos_data, j_pos_data, pwup_type, pwup_value, block_code );
			END
		
			DEFAULT:
				say( "Block: invalid block_code=="+block_code );
			END
		END

	ELSE  //Archivo de otras versiones de Bonedit
		Block_normal( i_pos_data, j_pos_data, block_code );
	END
END




//
// FUNCTION find_column( int i )
// FUNCTION find_row( int j )
//
// Funciones que deducen la posición en una cuadrícula uniforme del elemento de una lista, sabiendo la posición del elemento
//en esta y la cantidad de ellos que tiene cada fila de la cuadrícula. Se usan para saber en que posición van los bloques
//de los niveles a partir del array level.block_layout[] y del valor de level.max_blocks_i.
//


FUNCTION find_column( int i )

BEGIN
	//say( "find_column( "+i+" )" );
	LOOP
		IF (i>=level.max_blocks_i)
			i = i-level.max_blocks_i;
		ELSE
			BREAK;
		END
	END
	//say( "-find_column: "+i );
	RETURN(i);
END


FUNCTION find_row( int i )

PRIVATE
	int j;
END

BEGIN
	//say( "find_row( "+i+" )" );
	LOOP
		IF (i>=level.max_blocks_i)
			i = i-level.max_blocks_i;
			j++;
		ELSE
			BREAK;
		END
	END
	//say( "-find_row: "+j );
	RETURN(j);
END



//
//	FUNCTION text_clearer( int qty, int pointer p_id_array )
//
// Función que permite borrar un conjunto de textos cuyas ids hayan sido guardadas en un array. La idea es que la usen los
//distintos menus que crean texto indexandolo de en un array al salir.
// Nota: las ids de texto en Bennu siempre van incrementandose. Aunque un texto haya sido borrado, la id de uno nuevo será
//un número más alto que el texto antiguo. Esto no quiere decir que los textos antiguos sigan en la memmoria.
//A veces al intentar listar las strings en memoria desde la consola de debug peta el programa, aunque no parece que tenga
//que ver con el uso de esta función.
//Peta si: intentamos listar las strings antes de entrar al file_manager()
//         a veces trás reiniciar con f2 e intentamos listar las strings
//         a veces al reiniciar con f2 habiendo estado comprobando las strings
// Los errores se dieron tanto usando esta función como borrando el texto desde los propios procesos que lo escribieron.
// 
//	int qty                 //elementos en el array
//	int pointer p_id_array  //puntero al array que contiene los ids de los textos 
//


FUNCTION text_clearer( int qty, int pointer p_id_array )

PRIVATE
	int i;
END

BEGIN
	say( "-text_clearer: qty=="+qty+" p_id_array=="+p_id_array);
	FOR (i = 0; i<qty; i++)
		say( "--text_clearer: i=="+i );
		say( "--text_clearer: p_id_array["+i+"]=="+p_id_array[i]+" &p_id_array["+i+"]=="+&p_id_array[i] );

		IF (p_id_array[i]!=0)
			delete_text( p_id_array[i] );
		ELSE
			say( "--text_clearer: zero found, p_id_array["+i+"]=="+p_id_array[i] );
		END
	END
END



//
//FUNCTION toint( float original )
//
//Convierte un float a int redondeando a la alta a partir de 5 en la parte decimal y entendiendo "a la alta"
//como "alejandose de 0" cuando el número es negativo.
//


FUNCTION toint( float original )

PRIVATE
	int ivalue;
END

BEGIN

	IF ( original == 0.0 )
		say( "toint( 0 )!!" );
		RETURN( 0 );
	END
	
	ivalue = original;
	
	IF ( original > 0.0 )
		original = original-ivalue;
		IF ( original < 0.5 )
			RETURN( ivalue );
		ELSE
			RETURN( ivalue+1 );
		END
	END
	
	IF ( original < 0.0 )
		original = original-ivalue;
		IF ( original > -0.5 )
			RETURN( ivalue );
		ELSE
			RETURN( ivalue-1 );
		END
	END
	
END