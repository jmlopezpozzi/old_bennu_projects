#ifdef NOCLIP


//
//PROCESS Debug_ball_spawn( )
//  version NOCLIP
//Proceso que sirve para comprobar el comportamiento del juego en casos arbitrarios. Permite lanzar bolas en todas las
//direcciones y velocidades que estas pueden ir.
//Es llamado por Control_program() y tras arrancar se pone a dormir. Vuelve a despertar a través de una señal enviada desde Control_program()
//float svx y float svy son los datos que pasarán como parámetros vx y vy a los procesos Ball(i_pos,j_pos,vx,vy) que se creen,
//sus valores se pueden alterar dentro de los rangos en que se vayan a mover vx y vy (es decir, velocidad vertical
//constante en valor absoluto y velocidad horizontal limitada a un rango tipo -n..n).
//Dibuja un circulo en su posicion x,y y usando esta como punto de partida dibuja dos lineas con la misma dirección y con
//distinto alcance y color. La primera crece a medida que svx se aleja de cero, la otra línea llega al final de la pantalla.
//La versión NOCLIP dibuja una línea muy larga y luego la mueve con move_draw().
//


PROCESS Debug_ball_spawn( )

PRIVATE
	float svx = 1.0;              //parámetro que se pasará como velocidad horizontal a Ball()
	float svy;                    //parámetro que se pasará como velocidad vertical a Ball()
	float incremento_svx = 0.1;   //valor que se usará para modivicar svx
	int linea = 0;                //guarda la id de la línea corta
	int linea_larga = 0;          //guarda la id de la línea larga
	byte update;                  //indica si hay que actualizar las líneas y como (0=no, 1=cambio, 2=move)
	bool modo;                    //dos regulaciones de angulo (cambiando incremento_svx): rápida o precisa
END

BEGIN
	i_pos = level.playfield_i/2;
	j_pos = level.playfield_j/2;
	svy = gameplay.ball_j_speed;
	
	signal( id, s_sleep );
	say( "BDS: INIT PRE FRAME x=="+x+" y=="+y+" , going to sleep" );
	FRAME;
	say( "BDS: INIT POST FRAME, awoken my masters!!!" );
			
	file = idfpg;
	graph = _dbs_graph;
	
	IF ( rotacion < 0 )
		x = display_j-j_pos+camera_offset_j;
		y = i_pos-camera_offset_i;	
	ELSE
		x = i_pos-camera_offset_i;
		y = j_pos-camera_offset_j;
	END
		
	//update = 1;		
	
	//primer dibujo de líneas
	drawing_color( rgb( 255, 0, 0 ) );
	drawing_z( -1 );
	linea_larga = draw_line( x, y, x+escala_nc*svx, y+escala_nc*svy );
	drawing_color( rgb( 127, 127, 63 ) );
	drawing_z( -2 );
	linea=draw_line( x, y, x+10*svx, y+10*svy );
	
	LOOP

		//MOVER		
		IF ( key( _a ) AND x > BORDER_LEFT+H_BALLSIZE+1 )
			i_pos--;
			update = 2;
		END
		IF ( key( _d ) AND x < BORDER_RIGHT-H_BALLSIZE-1 )
			i_pos++;
			update = 2;
		END
		IF ( key( _w ) AND y > BORDER_UP+H_BALLSIZE+1 )
			j_pos--;
			update = 2;
		END
		IF ( key( _s ) AND y < BORDER_DOWN-H_BALLSIZE-1 )
			j_pos++;
			update = 2;
		END

		//APUNTAR
		IF ( key( _4 ) AND svx > -gameplay.ball_max_i_speed )
			svx = svx-incremento_svx;
			update = 1;
		END
		IF ( key( _6 ) AND svx < gameplay.ball_max_i_speed )
			svx = svx+incremento_svx;
			update = 1;
		END
		IF ( key( _8 ) AND svy > 0 )
			svy = -gameplay.ball_j_speed;
			update = 1;
		END
		IF ( key( _5 ) AND svy < 0 )
			svy = gameplay.ball_j_speed;
			update = 1;
		END

		//LANZAR BOLA
		IF ( key( _q ) )
			ball( i_pos, j_pos, svx, svy );
			WHILE ( key( _q ) )
				FRAME;
			END
		END

		//LANZAR BOLAS
		IF ( key( _f ) )
			ball( i_pos, j_pos, svx, svy );
		END
		
		//CAMBIAR MODO
		IF ( key( _1 ) )
			IF ( modo == 0 )
				graph = _dbs2_graph;
				incremento_svx = 0.025;
				modo = 1;
			ELSE
				graph = _dbs_graph;
				incremento_svx = 0.1;
				modo = 0;
			END
			
			WHILE ( key( _1 ) )
				FRAME;
			END
		END
		
		//TRANSFORMACION IJ a XY
		IF ( rotacion < 0 )
			x = display_j-j_pos+camera_offset_j;
			y = i_pos-camera_offset_i;	
		ELSE
			x = i_pos-camera_offset_i;
			y = j_pos-camera_offset_j;
		END
		
		//DIBUJO DE LINEAS
		IF ( update != 0)
			IF ( update == 1 )
				//IF ( linea != 0 )
					delete_draw( linea );
				//ELSE
				//	say( "Debug_ball_spawn: no se borra linea, valor=="+linea );
				//END
				//IF ( linea_larga != 0 )
					delete_draw( linea_larga );
				//ELSE
				//	say( "Debug_ball_spawn: no se borra linea_larga, valor=="+linea_larga );
				//END
				
				drawing_color( rgb( 255, 0, 0 ) );
				drawing_z( -1 );
				IF ( rotacion >= 0 )
					linea_larga = draw_line( x, y, x+svx*escala_nc, y+svy*escala_nc );  //xo,yo,xo+xf,yo+yf
				ELSE
					linea_larga = draw_line( x, y, x-svy*escala_nc, y+svx*escala_nc );  //xo,yo,xo-yf,yo+xf
				END
				
				drawing_color( rgb( 127, 127, 63 ) );
				drawing_z( -2 );
				IF ( rotacion >= 0 )
					linea = draw_line( x, y, toint( x+svx*10 ), toint( y+svy*10 ) );  //xo,yo,xo+xf,yo+yf
				ELSE
					linea = draw_line( x, y, x-svy*10, y+svx*10 );  //xo,yo,xo-yf,yo+xf
				END
			ELSE
				move_draw( linea_larga, x, y );
				move_draw( linea, x, y );
			END
		END
		
		update = 0;

		//SALIR
		IF ( key( _r ) )
			delete_draw( linea );
			delete_draw( linea_larga );
			update = 1;
			say( "BDS: a dormir" );
			signal( id, s_sleep );
		END
		
		FRAME;
	END
END


#else


//
//PROCESS Debug_ball_spawn( )
//  (versión con clipping)
//Proceso que sirve para comprobar el comportamiento del juego en casos arbitrarios. Permite lanzar bolas en todas las
//direcciones y velocidades que estas pueden ir.
//Es llamado por Control_program() y tras arrancar se pone a dormir. Vuelve a despertar a través de una señal enviada desde Control_program()
//float svx y float svy son los datos que pasarán como parámetros vx y vy a los procesos Ball(i_pos,j_pos,vx,vy) que se creen,
//sus valores se pueden alterar dentro de los rangos en que se vayan a mover vx y vy (es decir, velocidad vertical
//constante en valor absoluto y velocidad horizontal limitada a un rango tipo -n..n).
//Dibuja un circulo en su posicion x,y y usando esta como punto de partida dibuja dos lineas con la misma dirección y con
//distinto alcance y color. La primera crece a medida que svx se aleja de cero, la otra línea llega al final de la pantalla.
//Esta versión (hace clipping) dibuja la línea larga usando el punto de intersección con el borde del playfield como segundo
//punto para dibujar la línea. Este punto hay que recalcularlo en cada movimiento y se hace con las funciones check_i() y check_j()
//(la línea corta no se recorta contra los bordes del playfield).
//


PROCESS Debug_ball_spawn( )

PRIVATE
	float svx = 1.0;             //parámetro que se pasará como velocidad horizontal a Ball()
	float svy;                   //parámetro que se pasará como velocidad vertical a Ball()
	float incremento_svx = 0.1;  //valor que se usará para modificar svx
	float m;                     //guardará el resultado de svy/svx (es necesario para hacer el clipping)
	int linea;                   //guarda la id de la línea corta
	int linea_larga;             //guarda la id de la línea larga
	byte update;                 //indica si hay que actualizar las líneas y como (0=no, 1=cambio, 2=move (no va, hay que cambiar siempre))
	bool modo;                   //dos regulaciones de angulo: rápida o precisa
	bool anterior;               //lo usa la linea larga para saber el ultimo camino que tomó para el clipping
END

BEGIN
	i_pos = level.playfield_i/2;
	j_pos = level.playfield_j/2;
	svy = gameplay.ball_j_speed;
		
	IF ( rotacion < 0 )
		x = display_j-j_pos+camera_offset_j;
		y = i_pos-camera_offset_i;	
	ELSE
		x = i_pos-camera_offset_i;
		y = j_pos-camera_offset_j;
	END

	signal( id, s_sleep );
	say( "Debug_ball_spawn: INIT PRE FRAME x=="+x+" y=="+y+" , going to sleep" );
	say( "Debug_ball_spawn: id=="+id+" status=="+get_status( id ) );
	FRAME;
	say( "Debug_ball_spawn: INIT POST FRAME, awoken my masters!!!" );
	say( "Debug_ball_spawn: id=="+id+" status=="+get_status( id ) );
			
	file = idfpg;
	graph = _dbs_graph;
	
	//dibujado inicial
	drawing_color( rgb( 255, 0, 0 ) );
	drawing_z( -1 );
	m = svy/svx;
	
	check_j( i_pos, j_pos, svx, svy, m, &linea_larga );  //uso check_j para la inicialización porque es el que coincide con svx=1.0

	drawing_color( rgb( 127, 127, 127 ) );
	drawing_z( -2 );
	
	IF ( rotacion >= 0 )
		linea = draw_line( x, y, x+toint( 10*svx ), y+toint( 10*svy ) );  //xo,yo,xo+xf,yo+yf
	ELSE
		linea = draw_line( x, y, x-toint( 10*svy ), y+toint( 10*svx ) );  //xo,yo,xo-yf,yo+xf
	END
	

	LOOP
	
		//DEBUG
		IF ( key ( _2 ) )
			svx = 0;
		END
		
		//MOVER
		IF ( key( _a ) AND i_pos > 0+H_BALLSIZE+1 )                  //IF ( key( _a ) AND i_pos > i_border_left( )+H_BALLSIZE+1 )
			i_pos--;
			update = 2;
		END
		IF ( key( _d ) AND i_pos < level.playfield_i-H_BALLSIZE-1 )  //IF ( key( _d ) AND i_pos < i_border_right( )-H_BALLSIZE-1 )
			i_pos++;
			update = 2;
		END
		IF ( key( _w ) AND j_pos > 0+H_BALLSIZE+1 )                  //IF ( key( _w ) AND j_pos > j_border_up( )+H_BALLSIZE+1 )
			j_pos--;
			update = 2;
		END	
		IF ( key( _s ) AND j_pos < level.playfield_j-H_BALLSIZE-1 )  //IF ( key( _s ) AND j_pos < j_border_down( )-H_BALLSIZE-1 )
			j_pos++;
			update = 2;
		END

		//APUNTAR
		IF ( key( _4 ) AND svx > -gameplay.ball_max_i_speed )
			svx = svx-incremento_svx;
			update = 1;
		END
		IF ( key( _6 ) AND svx < gameplay.ball_max_i_speed )
			svx = svx+incremento_svx;
			update = 1;
		END
		IF ( key( _8 ) AND svy > 0 )
			svy = -gameplay.ball_j_speed;
			update = 1;
		END
		IF ( key( _5 ) AND svy < 0 )
			svy = gameplay.ball_j_speed;
			update = 1;
		END

		//LANZAR BOLA
		IF ( key( _q ) )
			ball( i_pos, j_pos, svx, svy );
			WHILE ( key( _q ) )
				FRAME;
			END
		END

		//LANZAR BOLAS
		IF ( key( _f ) )
			ball( i_pos, j_pos, svx, svy );
		END
	
		//CAMBIAR MODO
		IF ( key( _1 ) )
			IF ( modo == 0 )
				graph = _dbs_graph;
				incremento_svx = 0.025;
				modo = 1;
			ELSE
				graph = _dbs2_graph;
				incremento_svx = 0.1;
				modo = 0;
			END
			
			WHILE ( key( _1 ) )
				FRAME;
			END
		END
		
		//TRANSFORMACION IJ a XY		
		IF ( rotacion < 0 )
			x = display_j-j_pos+camera_offset_j;
			y = i_pos-camera_offset_i;	
		ELSE
			x = i_pos-camera_offset_i;
			y = j_pos-camera_offset_j;
		END
		
		
		//DIBUJO DE LINEAS
		IF ( update != 0 )
			drawing_z( -1 );
			
			//Línea larga
			IF ( svx != 0.0 )
				IF ( linea_larga!=0 )
					delete_draw( linea_larga );
				ELSE
					say( "Debug_ball_spawn: ERROR, intentando borrar linea_larga=="+linea_larga );
				END
				m = svy/svx;
				
				IF ( anterior == 1 )
					IF ( !check_j( i_pos, j_pos, svx, svy, m, &linea_larga ) )
						check_i( i_pos, j_pos, svx, svy, m, &linea_larga );
						anterior = 0;
					END
				ELSE
					IF ( !check_i( i_pos, j_pos, svx, svy, m, &linea_larga ) )
						check_j( i_pos, j_pos, svx, svy, m, &linea_larga );
						anterior = 1;
					END
				END
				 
			ELSE
				//A no ser que forcemos el valor svx=0.0, esta rama del IF no debería ejecutarse nunca:
				//Por alguna razón Bennu, al venir svx de otro valor diferente a 0.0, cuando este está aparentemente
				//en 0.0 aún es válido (debe ser un numero muy pequeño y no 0 total), el resultado de m=svy/svx es un
				//numero muy grande. Este número es siempre el mismo cada vez que se da el caso dentro de una misma 
				//ejecución del programa, pero cambia en distintas ejecuciones. Supongo que es un mecanismo para 
				//evitar divisiones entre 0.
						
				//m=svy/svx;  //da #INF
				say( "Debug_ball_spawn: ATENCIÓN!!" );
				say( "Debug_ball_spawn: dibujando línea con svx=="+svx );
				say( "Debug_nall_spawn: data, svy=="+svy+" m=="+m );
				
				IF ( linea_larga!=0 )
					delete_draw( linea_larga );
				ELSE
					say( "Debug_ball_spawn: ERROR, intentando borrar linea_larga=="+linea_larga );
				END
				
				IF ( rotacion >= 0 )
					IF ( svy > 0 )
						linea_larga = y_border_down( );  //aqui linea_larga se usa para guardar un valor temporal
					ELSE
						linea_larga = y_border_up( );
					END
					drawing_color( rgb( 255, 0, 0 ) );
					linea_larga = draw_line( x, y, x, linea_larga );
				ELSE
					IF ( svy > 0 )
						linea_larga = x_border_right( );
					ELSE
						linea_larga = x_border_left( );
					END
					drawing_color( rgb( 255, 0, 0 ) );
					linea_larga = draw_line( x, y, linea_larga, y );
				END
				
				//check_j( i_pos, j_pos, svx, svy, m, &linea_larga );  //no se usa
				
			END
			
			
			//Línea corta
			delete_draw( linea );
			
			drawing_z( -2 );
			drawing_color( rgb( 127, 127, 127 ) );

			IF ( rotacion >= 0 )
				linea=draw_line( x, y, x+toint( 10*svx ), y+toint( 10*svy ) );  //xo,yo,xo+xf,yo+yf
			ELSE
				linea=draw_line( x, y, x-toint( 10*svy ), y+toint( 10*svx ) );  //xo,yo,xo-yf,yo+xf
			END
			
		END
		
		update = 0;

		//SALIR
		IF ( key( _r ) )
			delete_draw( linea );
			delete_draw( linea_larga );
			update = 1;
			say( "BDS: a dormir" );
			signal( id, s_sleep );
		END
		
		
		FRAME;
	END
ONEXIT
	FRAME;
	say( "Debug_ball_spawn saliendo" );
END


//
//	FUNCTION check_i( int io, int jo, float svx, float svy, float m, int pointer linea_larga )
//	FUNCTION check_j( int io, int jo, float svx, float svy, float m, int pointer linea_larga )
//
//Calculan el punto de intersección entre la línea larga que dibuja Debug_ball spawn() (que nace en el punto i_pos,j_pos de
//dicho proceso y su dirección y sentido vienen dados por svy,svy) y el borde del playfield, para recortar en ese punto
//check_i() comprueba si hay intersección con los bordes laterales, en caso que sí dibuja la línea y
//devuelve 1, en caso que no, devuelve 0. check_j() hace lo mismo para bordes superior e inferior.
//Las intersecciones se calculan usando ecuaciones de la recta, las mismas que para las intersecciones con los bordes
//de los bloques en la función compute_bounce()  (en esta versión están casi como en un libro de geometría, pero creo
//que se pueden ahorrar algunas divisiones y multiplicaciones poniendo la formula un poco más desarrollada)
//(también tengo
//que probar que diferencias hay entre acceder a *linea_larga o linea_larga (directamente)):
//*linea_larga altera el valor, linea_larga altera la dirección
//Finalmente, la línea se dibuja usando la función draw_line() de Bennu, esta función requiere los puntos en los ejes
//de coordenadas x,y por lo que hay que transformarlos desde i,j.
//(a diferencia de la línea corta o la línea larga sin clipping, no se trabaja directamente sobre x,y porque esto hace 
//que haya que cambiar la ecuación de intersección de una manera que no encontré. En todo caso, así es más consistente
//con el resto del programa).
//
//PARAMETROS:
//	float io, jo;             //posición i,j original
//	float svx, svy;           //velocidad en cada eje (en realidad solo hace falta svx para check_i y vice versa, de momento lo dejo
//	                          //  así para que funcionen bien unos say(), pero incluso así se puede deducir el resultado del parámetro
//	                          //  que nos ahorremos usando el valor de m)
//	float m                   //resultado de svy/svx, lo pasa Debug_ball_spawn() como parámetro porque ya lo calculó antes
//	int pointer linea_larga   //dirección en memoria de linea_larga, para poder guardar la ID de la línea cada vez que se vuelve
//	                          //  a dibujar
//
//PRIVATE:
//	float ic, jc;             //se usan para guardar el punto de intersección en i,j entre el borde del playfield y la línea
//	float xo, yo;             //puntos orígen io,jo transformado a x,y se usarán como parámetros de draw_line()
//	float xc, yc;             //puntos de intersección ic, jc transformados a x,y se usarán como parámetros de draw_line()
// 
//


FUNCTION check_i( float io, float jo, float svx, float svy, float m, int pointer linea_larga )

PRIVATE
	float ic;
	float jc;
	float xo;
	float yo;
	float xc;
	float yc;
END

BEGIN

	IF ( svx > 0 )
		ic = level.playfield_i;  //i_border_right( );
	ELSE
		ic = 0;  //i_border_left( );
	END
	jc = jo-m*io+m*ic;


	
	IF ( jc > 0 AND jc < level.playfield_j )  //IF ( jc>j_border_up( ) AND jc<j_border_down( ) )
		drawing_color( rgb( 255, 0, 0 ) );

		IF ( rotacion < 0 )
			xo = display_j-jo+camera_offset_j;
			yo = io-camera_offset_i;
			xc = display_j-jc+camera_offset_j;
			yc = ic-camera_offset_i;
		ELSE
			xo = io-camera_offset_i;
			yo = jo-camera_offset_j;
			xc = ic-camera_offset_i;
			yc = jc-camera_offset_j;
		END
		
		//say( "check_i: data, m=="+m+" svx=="+svx+" svy=="+svy );	
		//say( "check_i: data, io=="+io+" jo=="+jo+" ic=="+ic );
		//say( "check_i: result, jc=="+jc );
		//say( "check_i: xy coords, xo=="+xo+" yo=="+yo+" xc=="+xc+" yc=="+yc );


		*linea_larga = draw_line( xo, yo, toint( xc ), toint( yc ) );
		//linea_larga = draw_line( xo, yo, xc, yc );  //si a este comentario le ponemos el asterisco de pointer rompe la estructura
		
		//say( "funcion check_i, linea dibujada" );
		RETURN( 1 );
		
	ELSE
	
		//say( "funcion check_i no pasa, hay que llamar a check_j" );
		RETURN( 0 );
	END
END


FUNCTION check_j( float io, float jo, float svx, float svy, float m, int pointer linea_larga )

PRIVATE
	float ic;
	float jc;
	float xo;
	float yo;
	float xc;
	float yc;
END

BEGIN
	IF ( svy > 0 )
		jc = level.playfield_j;  //y_border_down( );
	ELSE
		jc = 0;  //y_border_up( );
	END
	
	ic = (jc-jo+m*io)/m;
	
	IF ( ic > 0 AND ic < level.playfield_i )  //IF ( ic > i_border_left( ) AND ic < i_border_right( ) )
		drawing_color( rgb( 255, 0, 0 ) );
		
		IF ( rotacion < 0 )
			xo = display_j-jo+camera_offset_j;
			yo = io-camera_offset_i;
			xc = display_j-jc+camera_offset_j;
			yc = ic-camera_offset_i;
		ELSE
			xo = io-camera_offset_i;
			yo = jo-camera_offset_j;
			xc = ic-camera_offset_i;
			yc = jc-camera_offset_j;
		END
				
		//say( "check_j: data, m=="+m+" svx=="+svx+" svy=="+svy );	
		//say( "check_j: data, io=="+io+" jo=="+jo+" jc=="+jc );
		//say( "check_j: result, ic=="+ic );
		//say( "check_j: xy coords, xo=="+xo+" yo=="+yo+" xc=="+xc+" yc=="+yc );
			
		*linea_larga = draw_line( xo, yo, toint( xc ), toint( yc ) );
		//linea_larga = draw_line( xo, yo, xc, yc );  //si a este comentario le ponemos el asterisco de pointer rompe el preprocessor

		//say( "funcion check_Y, dibujada linea" );
		RETURN( 1 );
		
	ELSE
	
		//say( "funcion check_J falla y hay que llama a check_I" );
		RETURN( 0 );
	END
END

#endif



//
//	FUNCTION x_border_right( )
//	FUNCTION x_border_left( )
//	FUNCTION y_border_up( )
//	FUNCTION y_border_down( )
//
//Devuelven la posición x o y de los bordes del playfield independientemente de su rotación
//(es decir, x_border_right devolverá la posición x del borde derecho del playfield, se corresponda
//este con i o con j)
//
//


FUNCTION x_border_right( )

BEGIN
	IF ( rotacion>=0 )
		RETURN( level.playfield_i-camera_offset_i );
	ELSE
		RETURN( display_j-level.playfield_j+camera_offset_j );
	END
END

FUNCTION x_border_left( )

BEGIN
	IF ( rotacion>=0 )
		RETURN( -camera_offset_i );
	ELSE
		RETURN( display_j+camera_offset_j );
	END
END

FUNCTION y_border_up( )

BEGIN
	IF ( rotacion>=0 )
		RETURN( -camera_offset_j );
	ELSE
		RETURN( -camera_offset_i );
	END
END

FUNCTION y_border_down( )

BEGIN
	IF ( rotacion>=0 )
		RETURN( level.playfield_j-camera_offset_j );
	ELSE
		RETURN( level.playfield_i-camera_offset_i );
	END
END