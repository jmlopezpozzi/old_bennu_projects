/*
* ciber-gorilas.prg
*
*Esta versión usa la información de los procesos de Bennu para
*representar todos los objetos y sus interacciones. El mapa lo definen
*el conjunto de procesos bloque, el objeto controlable del jugador
*usa procesos sensores para las colisiones y la función collision de
*Bennu se usa para comprobar (todas) las colisiones.
*
*El hecho de que haya dos versiones se debe a que al plantear el
*funcionamiento del juego se me ocurrieron las dos formas, y al no
*saber cuál era "la correcta" decidí hacerlo de las dos maneras para
*ver los pros y los contras de cada una.
*/


#define MAX_TILES_X screenx/tilesize
#define MAX_TILES_Y screeny/tilesize
#define H_HITBOXSIZE hitboxsize/2

#define NOLRKEY controller[0]
#define KEYLEFT controller[1]
#define KEYRIGHT controller[2]
#define KEYUP controller[3]
#define KEYDOWN controller[4]
#define KEYAIM controller[5]
#define KEYSHOOT controller[6]

#define PIXEL_CHECK 3
//#define STEPCHECK
//#define PRECISE_SENSOR
#define DEBUG_INFO
//#define FLOAT_DIRECTION
//#define NEGADORA
//PIXEL_CHECK==>1=ambos,2=solo hitbox,3=solo bananas
//STEPCHECK requiere PIXEL_CHECK

Import "mod_proc";
Import "mod_video";
Import "mod_rand";
Import "mod_map";
Import "mod_draw";
Import "mod_key";
Import "mod_text";
Import "mod_say";
Import "mod_grproc";
Import "mod_math";
Import "mod_time";



Const

	tilesize=16;
	hitboxsize=14;
	screenx=320;
	screeny=240;
	gravedad=0.175;
	accel=0.075;
	jet_fuerza=0.25;
	jet_max_fuel=50;
	min_angulo=0;
	max_angulo=90;
	min_fuerza=20;
	max_fuerza=100;         //en principio la fuerza es un porcentaje
	relacion_fuerza=0.085;  //la fuerza se multiplica por este numero
	tiempo_mensaje=170;     //tiempo en frames que dura el mensaje del arbitro
	max_puntos=3;           //opciones por defecto desde aqui...
	altura_maxima=11;
	altura_minima=2;
	ancho_minimo=2;
	ancho_maximo=5;         //...hasta aqui
	rango_ancho=5;          //maximos configurables en las opciones, desde aqui...
	rango_alto=12;
	rango_puntos=15;        //hasta aqui

End



Global

	int render_counter;    //numero de columnas dibujadas durante el proceso de generacion de mapa	
	int spawn[2]=3;	       //spawn[0] establece la distancia del spawn a partir de las paredes, spawn[1] y spawn[2] guardan la altura del gorila 1 y 2 al generar el mapa	
	int turn=1;            //indica que jugador tiene el control
	int turn_promotion=1;  //se usa para los cambios de turno, antes de que el jugador al que le toca gane el control
	int shooting=0;        //indica si un gorila está apuntando o si hay alguna banana volando. puede ser bool
	int puntos[2];         //puntos de cada jugador (de momento puntos[0] no se usa para nada ya que se pasa la variable pl para elegir el campo)

	bool controller[6];    //0=nolr,1=left,2=right,3=up,4=down,5=aim,6=shoot
	bool lr_helper[2];     //0=signal,1=previous signal,3=lorr_switch
	bool ud_helper[2];     //0=signal,1=previous signal,3=uord_switch
	bool aim_helper[1];    //0=signal,1=previous signal
	bool shoot_helper[1];  //0=signal,1=previous signal
	
	int graficos[5];       //guarda las id de los graficos al generarlos
	
	int simetria[MAX_TILES_X/2][1];    //[MAX_TILES_X/2] porque es el máximo que se puede llenar simetricamente (con edificios de ancho 1)

	struct opciones                    //opciones configurables
		int puntos=max_puntos;
		int ancho_min=ancho_minimo;
		int ancho_max=ancho_maximo;
		int altura_min=altura_minima;
		int altura_max=altura_maxima;
		bool simetrico=0;
	End

End



Declare Process hitbox(int pl)	

	Public

		float dx;
		float resta_dx;	
		float dy;
		float resta_dy;
		int idx;         //puede no ser necesario para precise_sensor
		int idy;
		int jetpack;
		int on_ground;
		int going;       //indica hacia donde se suma la gravedad
		int direction;   //indica en la direccion que va el hitbox, se consigue analizando dx
		int collider;    //puede ser bool. indica si hitbox está colisionando (por si se encaja)
		int player;
#ifdef PRECISE_SENSOR
		int direction_y;
#endif

#ifdef STEPCHECK
		int pcount;     //descomentar para activar el pixel_check cada dos actualizaciones de x
		int prev_xlec;
#endif

	End

End	



/*
*PROCESS Main()
*
*
*/

Process Main()

Begin

	set_mode(screenx,screeny,32);
	set_fps(60,0);

#ifdef DEBUG_INFO
	write_var(0,0,0,0,render_counter);
	write_var(0,0,30,0,fps);
	write_var(0,0,40,0,turn_promotion);
	write_var(0,0,50,0,turn);
#endif

	graphics_generation();
	map_generation();

	Loop
		controller_update();

		If (key(_esc))
			let_me_alone();
			exit();
		End

		If (KEYAIM)
			map_generation();
		End

		FRAME;

		If (KEYSHOOT)
			BREAK;
		End

		If (key(_o))
			options_menu();
		End
	End
	
	pause_menu();
	Hitbox(1);
	Hitbox(2);

	Loop
		If (key(_esc))
			let_me_alone();
			exit();
		End

		controller_update();

#ifdef DEBUG_INFO
		If (key(_t))
			turn_change();
		End
#endif
				
		FRAME;		
	End

End



/*FUNCTION graphics_generation()
*
*Genera los gráficos con funciones de dibujo y guarda sus id en la tabla global graficos[].
*Si la función es llamada y los gráficos han sido generados previamente no los vuelve a generar.
*/

Function graphics_generation()

Begin
	If (graficos[0]!=0)
		say("graficos ya existentes!");
		RETURN;
	End

	//BLOQUE
	graficos[0]=map_new(tilesize,tilesize,32);
	map_clear(0,graficos[0],rgb(255,0,0));
	drawing_map(0,graficos[0]);
	drawing_color(rgb(0,255,0));
	draw_rect(0,0,tilesize-1,tilesize-1);

	//HITBOX
	graficos[1]=map_new(hitboxsize,hitboxsize,32);
	map_clear(0,graficos[1],rgb(255,255,0));

	//BANANA
	graficos[2]=map_new(8,8,32);
	drawing_map(0,graficos[2]);
	drawing_color(rgb(255,255,127));
	draw_fcircle(4,4,3);

	//SENSORES SUELO/TECHO
	graficos[3]=map_new(hitboxsize-2,1,32);
	map_clear(0,graficos[3],rgb(255,0,255));

	//SENSORES LATERALES
	graficos[4]=map_new(1,hitboxsize-2,32);
	map_clear(0,graficos[4],rgb(255,0,255));

	//LETRERO DE PAUSA
	graficos[5]=map_new(text_width(0,"PAUSA")+2,text_height(0,"PAUSA")+2,32);
	map_clear(0,graficos[5],rgb(0,0,0));
	map_put(0,graficos[5],write_in_map(0,"PAUSA",0),1,1);

End



/*FUNCTION map_generation()
*
*Borra el mapa actual (si hay uno) y genera uno nuevo teniendo en cuenta los parámetros de la tabla global opciones .
*Para ello llama a la funcion edificio(int ancho, int alto) sucesivamente hasta llenar la pantalla. Le pasa parámetros
*generados aleatoriamente dentro de los rangos establecidos por la tabla global opciones.
*
*Para generar mapas simétricos se guardan sucesivamente los parámetros pasados a edificio(int ancho, int alto) en la
*tabla global simetria[MAX_TILES_X/2][1] hasta llegar a la mitad del mapa. Las siguientes llamadas a edificio(int ancho, int alto)
*se hacen recorriendo la tabla global simetria[MAX_TILES_X/2][1] desde la última posición escrita y hacia atrás y pasandole sus 
*valores a edificio(int ancho, int alto) .
*
*Tiene un mecanismo para que no se le pase el mismo valor como parámetro alto a dos llamadas sucesivas a
*la función edificio(int ancho, int alto) .
*/

Function map_generation()

Private 

	int i;
	int j;
	int altura;
	int altura_previa;
	int ancho;

End

Begin

	//RESETEA EL MAPA ANTERIOR
	While (exists(type bloque))  //borra el mapa previo (si hay alguno)
		signal(type bloque,s_kill);
	End

	render_counter=0;  //pone las variables que altera a 0 (por si tienen valores previos de otro mapa)

	For (i=1;i<3;i++)
		spawn[i]=0;
	End

	If (opciones.simetrico==1)
		For (i=0;i<MAX_TILES_X/2;i++)  //resetea las variables de simetria tambien
			For (j=0;j<2;j++)
				simetria[i][j]=0;
			End
		End
	End


	//EMPIEZA A GENERAR EL MAPA
	If (opciones.altura_min!=opciones.altura_max)
		If (opciones.simetrico==1)
			//MAPA SIMETRICO
			i=0;
			say("Llenando tabla simetria");
			Repeat
				altura=rand(opciones.altura_min,opciones.altura_max);
				ancho=rand(opciones.ancho_min,opciones.ancho_max);

				If (altura!=altura_previa)
					simetria[i][0]=ancho;
					simetria[i][1]=altura;
					say("i="+i+" ancho="+simetria[i][0]+" alto="+simetria[i][1]);
					i++;
					edificio(ancho, altura);
					rand_seed(rand(0,32000)+get_timer());
				Else
					rand_seed(rand(0,16000)+get_timer());
				End

				altura_previa=altura;
			Until (render_counter>MAX_TILES_X/2-opciones.ancho_max)

			say("ultima llamada a edificio antes de la simetría! render_counter="+render_counter);

			Repeat
				altura=rand(opciones.altura_min,opciones.altura_max);
			Until (altura!=altura_previa)

			simetria[i][0]=MAX_TILES_X/2-render_counter;
			simetria[i][1]=altura;
			edificio(MAX_TILES_X/2-render_counter,altura);
			say("i="+i+" ancho="+simetria[i][0]+" alto="+simetria[i][1]);

			While (i>=0)
				say("i="+i+" ancho="+simetria[i][0]+" alto="+simetria[i][1]);
				edificio(simetria[i][0],simetria[i][1]);
				i--;
			End
			i=0;
			spawn[2]=spawn[1];  //el spawn es el mismo si el mapa es simetrico
		Else
			//MAPA NO SIMETRICO
			Repeat	
				say("llamado a edificio!");
				altura=rand(opciones.altura_min,opciones.altura_max);

				If (altura!=altura_previa)
					edificio(rand(opciones.ancho_min,opciones.ancho_max),altura);
					rand_seed(rand(0,32000)+get_timer());
				Else
					rand_seed(rand(0,16000)+get_timer());
				End

				altura_previa=altura;
			Until (render_counter>MAX_TILES_X-opciones.ancho_max)
	
			say("ultima llamada a edificio! render_counter="+render_counter);

			Repeat
				altura=rand(opciones.altura_min,opciones.altura_max);
			Until (altura!=altura_previa)

			edificio(MAX_TILES_X-render_counter,altura);
		End
	Else
		//MAPA DE UNA SOLA ALTURA
		edificio(MAX_TILES_X,opciones.altura_min);  //en el caso de que la altura minima y maxima sean iguales en las opciones
	End

End



/*
*FUNCTION edificio(int ancho, int alto)
*
*Pone en el mapa un grupo de ancho*alto procesos bloque(x,y) . Lo hace creando tantas columnas como indique el parámetro ancho
*de tantos procesos bloque(x,y) como indique el parámetro alto, desde la parte inferior de la pantalla. Para saber en que punto
*del ancho del mapa tiene que empezar a los procesos bloque(x,y) lee la variable global render_counter, que lleva cuenta de cuantas
*columnas han sido dibujadas. Al terminar de llamar a los procesos bloque(x,y) suma la cantidad de columnas avanzadas al valor de
*la variable global render_counter.
*
*Si está activada la opción de mapa simétrico escribe los valores de los parámetros ancho y alto en la tabla global
*simetria[MAX_TILES_X/2][1] en sucesión a los valores que hayan escrito previas llamadas a edificio(int ancho, int alto) .*
*/

Function edificio(int ancho, int alto)

Private

	int i;
	int j;

End

Begin

	For (i=0;i<ancho;i++)
		If (spawn[1]==0 AND render_counter+i==spawn[0]-1)  //el -1 es pq render_counter empieza de 0
			spawn[1]=MAX_TILES_Y-alto;
			say("spawn1!=="+spawn[1]+" ancho=="+ancho+" alto=="+alto);
		End

		If (spawn[2]==0 AND render_counter+i==MAX_TILES_X-spawn[0])
			spawn[2]=MAX_TILES_Y-alto;
			say("spawn2!=="+spawn[2]+" ancho=="+ancho+" alto=="+alto);
		End

		For (j=0;j<alto;j++)			
			Bloque(render_counter*tilesize+tilesize*i+tilesize/2,screeny-tilesize*j-tilesize/2);
		End
	End

	render_counter=render_counter+i;

End



/*FUNCTION controller_update()
*
*Rellena los campos de la tabla global controller[6] según el estado del teclado, siguiendo distintas reglas para cada
*tecla o grupo de teclas que se usa. La mayoría de input se lee a través de esta tabla en todo el programa.
*/

Function controller_update()

Begin

	//NOLRKEY
	If (key(_left) OR key(_right))  //si izqd o drcha son pulsadas NORLEY vale 0 (y viceversa)
		NOLRKEY=0;
	Else
		NOLRKEY=1;
	End

	//LEFT OR RIGHT (exclusivas)
	If (NOLRKEY==0)  //si NORLEY vale 0 se ve que se hace con izqd y drch
		If (key(_left) AND key(_right))  //si ambas teclas se pulsan a la vez se pone [0]SIGNAL a 1
			lr_helper[0]=1;
		Else
			lr_helper[0]=0;  //si no, se pone [0]SIGNAL a 0 y [2]LORR según la tecla única pulsada (0=left,1=right)

			If (key(_left))
				lr_helper[2]=0;
			End
	
			If (key(_right))
				lr_helper[2]=1;
			End
		End

		If (lr_helper[0]==1 AND lr_helper[1]==0)  //si SIGNAL es 1 y PREVIOUS_SIGNAL=0 se hace el switch de LORR
			If (lr_helper[2]==0)
				lr_helper[2]=1;
			Else
				lr_helper[2]=0;
			End
		End

		lr_helper[1]=lr_helper[0];  //se le da a [1]PREVIOUS_SIGNAL el valor de [0]SIGNAL
			
		If (lr_helper[2]==0)  //según lo que valga LORR_SWITCH se pasa un estado a LEFT y a RIGHT 
			KEYLEFT=1;
			KEYRIGHT=0;
		Else
			KEYLEFT=0;
			KEYRIGHT=1;
		End
	Else
		//si NOSKEY está activa tanto LEFT como RIGHT se ponen a cero
		KEYLEFT=0;
		KEYRIGHT=0;
	End

	//UP OR DOWN (exclusivas)
	If (key(_up) OR key(_down))
		If (key(_up) AND key(_down))  //si ambas teclas se pulsan a la vez se pone [0]SIGNAL a 1
			ud_helper[0]=1;
		Else
			ud_helper[0]=0;  //si no, se pone [0]SIGNAL a 0 y [2]UORD según la tecla única pulsada (0=up,1=down)

			If (key(_up))
				ud_helper[2]=0;
			End
	
			If (key(_down))
				ud_helper[2]=1;
			End
		End

		If (ud_helper[0]==1 AND ud_helper[1]==0)  //si SIGNAL es 1 y PREVIOUS_SIGNAL=0 se hace el switch de UORD
			If (ud_helper[2]==0)
				ud_helper[2]=1;
			Else
				ud_helper[2]=0;
			End
		End

		ud_helper[1]=ud_helper[0];  //se le da a [1]PREVIOUS_SIGNAL el valor de [0]SIGNAL
		
		If (ud_helper[2]==0)  //según lo que valga UORD_SWITCH se pasa un estado a UP y a DOWN 
			KEYUP=1;
			KEYDOWN=0;
		Else
			KEYUP=0;
			KEYDOWN=1;
		End
	Else
		//si no se pulsan ni _up ni _down, tanto UP como DOWN se ponen a cero
		KEYUP=0;
		KEYDOWN=0;
	End

	//AIM (un pulso)
	If (key(_control))  //se comprueba CTRL (AIM)
		aim_helper[0]=1;  //si es que si se enciende su [0]SIGNAL

		If (aim_helper[0]==1 AND aim_helper[1]==0)  //y en el caso de que [0]SIGNAL valga 1 y [1]PREVIOUS_SIGNAL valga 0 se envía AIM
			KEYAIM=1;
		Else
			KEYAIM=0;
		End
	Else
		//si CTRL no se pulsa SIGNAL y AIM se ponen a 0
		aim_helper[0]=0;
		KEYAIM=0;
	End

	aim_helper[1]=aim_helper[0];  //Al final, [1]PREVIOUS_SIGNAL coge el valor de [0]SIGNAL*/

	//SHOOT (un pulso)
	If (key(_space))  //se comprueba SPACE (SHOOT), funciona igual que AIM
		shoot_helper[0]=1;	

		If (shoot_helper[0]==1 AND shoot_helper[1]==0)	
			KEYSHOOT=1;
		Else
			KEYSHOOT=0;
		End
	Else		
		shoot_helper[0]=0;
		KEYSHOOT=0;
	End

	shoot_helper[1]=shoot_helper[0];  //Al final, [1]PREVIOUS_SIGNAL coge el valor de [0]SIGNAL*/

End



/*
*FUNCTION turn_change()
*
*Altera el valor de la variable global turn_promotion entre dos posibles valores cada vez que es llamada.
*/

Function turn_change()

Begin

	If (turn_promotion==1)
		turn_promotion=2;
	Else
		turn_promotion=1;
	End

End



/*
*FUNCTION reset_game()
*
*Restaura los valores iniciales de ciertas variables, detiene al resto de procesos, llama al proceso Main() y termina.
*/

Function reset_game()

Begin

	puntos[1]=0;
	puntos[2]=0;
	shooting=0;
	turn=1;
	turn_promotion=1;

#ifdef PIXEL_CHECK
	map_clear(0,0,0);
#endif

	let_me_alone();
	Main();

End



/*
*PROCESS options_menu()
*
*Un menú que permite alterar los parámetros de la tabla global options.
*Duerme al resto de procesos y llama al proceso options_controller() , dejando sólo estos dos procesos en ejecución.
*Tiene un mecánismo para dar valores aleatorios a cada elemento de la tabla global options.
*Respeta rangos de mínimo y máximo al operar. Al terminal despierta al resto de procesos y llama a la función
*map_generation() para que genere un nuevo mapa.
*/

Process options_menu()

Private

	int idtexto;
	int idtextovar;
	int cursor;
	string textos[5]="SIMETRIA","PUNTOS PARA GANAR","ANCHO MINIMO","ANCHO MAXIMO","ALTURA MINIMA","ALTURA MAXIMA";

End

Begin

	signal(ALL_PROCESS,s_sleep);
	options_controller();

	Loop
		idtexto=write(0,screenx/2,screeny/2-10,4,textos[cursor]);

		Switch (cursor)
			Case 0:  //SIMETRIA
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.simetrico);

				Repeat
					If (KEYLEFT==1 OR KEYRIGHT==1)
						If (opciones.simetrico==0)
							opciones.simetrico=1;
						Else
							opciones.simetrico=0;
						End
					End
					
					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End

			Case 1:  //PUNTOS PARA GANAR
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.puntos);

				Repeat
					If (KEYRIGHT AND opciones.puntos<rango_puntos)
						opciones.puntos++;
					End

					If (KEYLEFT AND opciones.puntos>1)
						opciones.puntos--;
					End

					If (KEYAIM)
						rand_seed(rand(0,16000)+get_timer());
						opciones.puntos=rand(1,rango_puntos);
					End

					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End

			Case 2:  //ANCHO MINIMO
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.ancho_min);

				Repeat
					If (KEYRIGHT AND opciones.ancho_min<opciones.ancho_max)
						opciones.ancho_min++;
					End

					If (KEYLEFT AND opciones.ancho_min>1)
						opciones.ancho_min--;
					End

					If (KEYAIM)
						rand_seed(rand(0,16000)+get_timer());
						opciones.ancho_min=rand(1,opciones.ancho_max);
					End

					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End

			Case 3:  //ANCHO MAXIMO
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.ancho_max);

				Repeat
					If (KEYRIGHT AND opciones.ancho_max<rango_ancho)
						opciones.ancho_max++;
					End

					If (KEYLEFT AND opciones.ancho_max>opciones.ancho_min)
						opciones.ancho_max--;
					End

					If (KEYAIM)
						rand_seed(rand(0,16000)+get_timer());
						opciones.ancho_max=rand(opciones.ancho_min,rango_ancho);
					End

					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End

			Case 4:  //ALTURA MINIMA
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.altura_min);

				Repeat
					If (KEYRIGHT AND opciones.altura_min<opciones.altura_max)
						opciones.altura_min++;
					End

					If (KEYLEFT AND opciones.altura_min>1)
						opciones.altura_min--;
					End

					If (KEYAIM)
						rand_seed(rand(0,16000)+get_timer());
						opciones.altura_min=rand(1,opciones.altura_max);
					End

					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End

			Case 5:  //ALTURA MAXIMA
				idtextovar=write_var(0,screenx/2,screeny/2+10,4,opciones.altura_max);

				Repeat
					If (KEYRIGHT AND opciones.altura_max<rango_alto)
						opciones.altura_max++;
					End

					If (KEYLEFT AND opciones.altura_max>opciones.altura_min)
						opciones.altura_max--;
					End

					If (KEYAIM)
						rand_seed(rand(0,16000)+get_timer());
						opciones.altura_max=rand(opciones.altura_min,rango_alto);
					End

					FRAME;
				Until (KEYUP==1 OR KEYDOWN==1 OR KEYSHOOT==1)
			End
		End

		If (KEYUP)
			If (cursor!=0)
				cursor--;
			Else
				cursor=5;
			End
		End

		If (KEYDOWN)
			If (cursor!=5)
				cursor++;
			Else
				cursor=0;
			End
		End

		If (KEYSHOOT)
			BREAK;
		End

		delete_text(idtexto);
		delete_text(idtextovar);
	End

	delete_text(idtexto);
	delete_text(idtextovar);
	
	signal(son,s_kill);
	signal(ALL_PROCESS,s_wakeup);
	map_generation();

End



/*
*PROCESS options_controller()
*
*Es llamado por el proceso options_menu() una vez que este ha dormido al resto de procesos. Llama a la función controller_update() en
*un bucle (porque Main() ha quedado suspendido) para que options_menu() pueda leer input de la tabla global controller[6] .
*Hace algunas alteraciones a la tabla tras la llamada de controller_update() para usar reglas de input propias del menú de 
*opciones. Es terminado via señal de options_menu() . Se ejecuta con una prioridad superior a este una vez llamado.  
*/

Process options_controller()

Private

	bool direction_helper[1];  //0=SIGNAL,1=PREVIOUS SIGNAL
	int which;

Begin

	priority=father.priority-1;

	Loop
		controller_update();

		If (KEYUP OR KEYDOWN OR NOLRKEY==0)  //si se pulsa alguna tecla de direccion
			direction_helper[0]=1;           //se manda una señal

			If (KEYUP)  //segun la tecla que se haya pulsado 'which' recibe un valor
				which=3;
			End

			If (KEYDOWN)
				which=4;
			End

			If (KEYLEFT)  //las laterales prevalecen ('which' se sobreescribe)
				which=1;
			End

			If (KEYRIGHT)
				which=2;
			End           //(se podría poner que si NOLRKEY==0, KEYUP=0 y KEYDOWN=0 pero no parece hacer falta

			If (direction_helper[0]==1 AND direction_helper[1]==0)  //si es el instante en que hay señal (y antes no habia)
				controller[which]=1;                                //se activa la direccion apretada
			Else
				For (which=1;which<5;which++)                       //de lo contrario se ponen todas las direcciones a cero
					controller[which]=0;
				End
			End
		Else                        //si no se pulsa ninguna tecla de direccion
			direction_helper[0]=0;  //la señal vale 0
		End

		direction_helper[1]=direction_helper[0];  //finalmente, la señal anterior se actualiza antes de volver a hacer el bucle

		If (key(_esc))
			let_me_alone();
			exit();
		End

		FRAME;
	End

End



/*
*PROCESS pause_menu()
*
*Proceso que permite pausar (congelar) el resto de procesos, pausando así el juego. Lo crea el proceso Main() al empezar una partida.
*También permite resetear el juego desde el estado de pausa.
*/

Process pause_menu()

Private

	bool p_pulsado;

End

Begin
	
	z=-1;
	x=screenx/2;
	y=screeny/2;

	Loop
		If (key(_p) AND p_pulsado==0);
			p_pulsado=1;
			graph=graficos[5];
			signal(ALL_PROCESS,s_freeze_force);  //s_freeze_force porque algunos procesos ignoran s_freeze con signal_action

			Loop
				If (p_pulsado==1 AND !key(_p))
					p_pulsado=0;
				End

				If (key(_r))
					reset_game();
				End

				If (key(_p) AND p_pulsado==0)
					p_pulsado=1;
					graph=0;
					signal(ALL_PROCESS,s_wakeup);  //los procesos que tengan que volver a congelarse lo hacen en ANTI-DESCONGELANTE
					BREAK;
				End

				If (key(_esc))
					let_me_alone();
					exit();
		
					BREAK;
				End

				FRAME;
			End
		End

		If (p_pulsado==1 and !key(_p))
			p_pulsado=0;
		End

		FRAME;
	End

End
					


/*
*PROCESS bloque(x,y)
*
*Piezas de las que está formada el mapa. Recibe los parámetros locales x e y a partir de un bucle de la funcion edificio(int ancho, int alto) .
*Una vez creado se congela y queda en pantalla para que otros procesos (como hitbox(), banana() o los distintos sensores) interactuen con él.
*Al salir comprueba si su posición es la del bloque más bajo de la columna de spawn de cada jugador (definido en el primer campo de la tabla
*global spawn[2]) y si es así llama al proceso bloque_respawn(x,y) .
*/

Process bloque(x,y)

Begin

	graph=graficos[0];
	signal_action(s_wakeup,S_IGN);
	signal(id,s_freeze);

	FRAME;

OnExit

	If (exists(type hitbox) AND y==screeny-tilesize/2 AND (x==spawn[0]*tilesize-tilesize/2 OR x==screenx-(spawn[0]*tilesize-tilesize/2)));	//a falta de grid...
			say("bloque special onexit, id=="+id+" x=="+x+" y=="+y+" llamando a bloque_respawn()");
		bloque_respawn(x,y);
	End

End



/*
*PROCESS bloque_respawn(x,y)
*
*Genera el bloque más bajo de la columna de spawn de cada jugador si este es destruido. Espera a que un jugador anote un punto o se cambie
*de turno una vez destruido el bloque.
*/

Process bloque_respawn(x,y)

Private

	int turno_actual;

End

Begin

	turno_actual=turn;
		say("bloque_respawn(): esperando a generar bloque");
		
	Loop
		If (exists(type arbitro) OR turn!=turno_actual)
			bloque(x,y);
				say("bloque_respawn(): bloque generado");
			BREAK;
		End

		FRAME;
	End

End



#ifdef DEBUG_INFO

/*PROCESS hitbox_debug_info(int pl)
*
*Imprime en pantalla valores de variables con las que el proceso hitbox(int pl) que lo ha llamado está trabajando, durante su turno.
*Es llamado por cada proceso hitbox(int pl) en su creación y cada vez que es su turno. Termina cuando ya no es el turno del hitbox
*que lo llamó. Los datos se dibujan en un lado diferente de la pantalla para cada jugador. 
*/

Process hitbox_debug_info(int pl)

Private

	Hitbox hb;
	int id_textos_debug[11];
	string textos_debug[5]="going","dx","direction","jetpack","dy","on_ground";

	int i;
	int margen_w;   //el valor que se le pasará como x a la función write
	int margen_wv;  //el valor que se le pasará como x a la función write_var
	int align_w;    //el valor que se le pasará como alineación a la función write
	int align_wv;   //el valor que se le pasará como alineación a la función write_var

End

Begin

	hb=father;

	Switch (pl)
		Case 1:
			margen_w=screenx-65;
			align_w=0;
			margen_wv=screenx-70;
			align_wv=2;
		End

		Case 2:
			margen_w=65;
			align_w=2;
			margen_wv=70;
			align_wv=0;
		End
	End

	signal_action(s_freeze_tree,S_IGN);

	For (i=0;i<6;i++)
		id_textos_debug[i]=write(0,margen_w,i*10,align_w,textos_debug[i]);
	End

	id_textos_debug[6]=write_var(0,margen_wv,0,align_wv,hb.going);
	id_textos_debug[7]=write_var(0,margen_wv,10,align_wv,hb.dx);
	id_textos_debug[8]=write_var(0,margen_wv,20,align_wv,hb.direction);
	id_textos_debug[9]=write_var(0,margen_wv,30,align_wv,hb.jetpack);
	id_textos_debug[10]=write_var(0,margen_wv,40,align_wv,hb.dy);
	id_textos_debug[11]=write_var(0,margen_wv,50,align_wv,hb.on_ground);

	say("creado hitbox_debug_info() para pl=="+pl);

	Loop
		If (turn_promotion!=pl)			
			BREAK;
		End

		FRAME;
	End

OnExit

	For (i=0;i<12;i++)
		delete_text(id_textos_debug[i]);
	End

	say("hitbox_debug_info() pl=="+pl+" SALIENDO!!");

End
#endif



/*
*PROCESS hitbox(int pl)
*
*Proceso que representa la posición de cada gorila respecto al resto de elementos. Cada jugador controla uno por turnos. El control lo gana
*una vez el hitbox ha tocado el suelo desde el inicio del turno. Mientras el jugador tiene el control del hitbox puede moverlo por el mapa. 
*Para esto se tiene en cuenta el input leído de la tabla global controller[6] y parámetros de aceleración y gravedad. Cada jugador en su 
*turno puede entrar en modo disparo. Cuando esto pasa hitbox queda quieto (como si no fuera su turno) y pasa el control al proceso aim_process(pl). 
* 
*Este proceso es llamado por el proceso Main() al iniciar una partida, y por el proceso arbitro() al salir, después de que un jugador haya anotado
*un punto eliminando el gorila del otro jugador. Se crea en una posición establecida por la tabla global spawn[2] según el parámetro pl que se
*le pase. Llama al proceso limiter() y a distintos procesos sensor_*() los cuales necesita para interactuar con el mapa.
*/

Process hitbox(int pl)

Begin

	say("creado hitbox(pl=="+pl+")");
	graph=graficos[1];
	player=pl;

#ifdef DEBUG_INFO
	hitbox_debug_info(pl);
	say("hitbox pl=="+pl+" llamado a hitbox_debug_info(pl)");
#endif

	Switch (pl)
		Case 1:
			x=spawn[0]*tilesize-tilesize/2;
		End
		Case 2:
			x=screenx-(spawn[0]*tilesize-tilesize/2);
		End
	End

	y=(spawn[player])*tilesize-tilesize/2;

	signal_action(s_freeze_tree,S_IGN);  //los procesos hitbox no se congelarán cuando se quiera congelar todo su arbol genealogico

	sensor_ground();
#ifdef PRECISE_SENSOR
	sensor_side();
#else
	sensor_roof();
	sensor_side(-1);
	sensor_side(1);
#endif
	limiter();

	Loop
		//AIM
		If (KEYAIM AND on_ground==1 AND shooting==0)  //se pone aquí para que no entre en modo shooting de nuevo nada más salir
			aim_process(pl);	
			shooting=1;
		End

		//STATUS CONTROL
		If (turn_promotion==pl AND turn!=pl AND on_ground==1)
			turn=pl;
			say("hitbox pl=="+pl+" ha cambiado turn, turn=="+turn);
		End

		If ((turn_promotion!=pl OR shooting==1) AND on_ground==1)
			say("status control pl=="+pl+" on_ground=="+on_ground+" se queda quieto");
			dx=0;
			idx=0;
			going=0;

			signal(id,s_freeze_tree);

			Repeat
				FRAME;	
			Until (turn_promotion==pl AND shooting==0)

			say("status control pl=="+pl+" despierta");	

			signal(id,s_wakeup_tree);
#ifdef DEBUG_INFO
			hitbox_debug_info(pl);
#endif
		End	
		
		//CONTROL
		If (turn==pl)
			If (KEYRIGHT==1)
				going=1;
			End

			If (KEYLEFT==1)
				going=-1;
			End

			If (NOLRKEY==1 AND dx>0)  //deceleratrix naturalis dretae
				going=-1;		
			
				If (dx<=accel)
					going=0;
					dx=0;
				End
			End

			If (NOLRKEY==1 AND dx<0)  //deceleratrix naturalis izquierdum
				going=1;		

				If (dx>=accel*-1)
					going=0;
					dx=0;
				End	
			End

			If (KEYUP==1 AND jetpack!=0)
				dy=dy-jet_fuerza;
				jetpack=jetpack-1;
			End
		End

		//MOVIMIENTO
		If (going!=0)	
			dx=dx+accel*going;
		End
	
		If (on_ground==0)
			dy=dy+gravedad;
		End

#ifdef FLOAT_DIRECTION
		//DIRECCION (calculada a partir de floats)
		If (dx==0)  //no llega a leer el 0 del todo (con PRECISE_SENSOR)//probar a hacer la comprobacion en el sensor pero creo que es lo mismo
			direction=0;
		End
		If (dx>0)
			direction=1;
		End
		If (dx<0)
			direction=-1;
		End
#ifdef PRECISE_SENSOR
		If (dy>0)
			direction_y=1;
		End
		If (dy<0)
			direction_y=-1;
		End
		If (dy==0)	
			direction_y=0;
		End
#endif
#endif

		//FLOAT TO INT
		If (dx!=0)
			idx=dx+resta_dx;
			resta_dx=resta_dx+dx-idx;  //este tiene en cuenta los decimales que han sobrado. Es mejor
		Else				
			idx=0;
		End

		If (dy!=0)
			idy=dy+resta_dy;
			resta_dy=resta_dy+dy-idy;  //antes era "resta_dy=dy-idy;" pero creo que asi es mejor. Lo mismo con idx y las bananas	
		Else				
			idy=0;
		End

#ifndef FLOAT_DIRECTION
		//DIRECCION (calculada a partir de int)
		If (idx==0)	
			direction=0;
		End
		If (idx>0)
			direction=1;
		End
		If (idx<0)
			direction=-1;
		End
#ifdef PRECISE_SENSOR
		If (idy>0)
			direction_y=1;
		End
		If (idy<0)
			direction_y=-1;
		End
		If (idy==0)	
			direction_y=0;
		End
#endif
#endif

#ifndef PRECISE_SENSOR
		//DESENGANCHE
		If (collision(type bloque))
			collider=1;
		Else 
			collider=0;
		End

		FRAME;
			
		//POSICION
		x=x+idx;
		y=y+idy;

#endif

#ifdef PIXEL_CHECK
	#if PIXEL_CHECK!=3
		pixel_checker(x);
	#endif		
#endif
#ifdef PRECISE_SENSOR
		FRAME;
#endif
	
	End	//Loop

End



/*
*PROCESS limiter()
*
*Es creado por cada proceso hitbox(int pl) y confina su movimiento dentro de unos límites (cada jugador la mitad del mapa).
*El proceso establece unos límites según el proceso hitbox(int pl) que le haya llamado y cuando este rebasa alguno, el proceso
*limiter() altera variables de posición y movimiento de hitbox(int pl) (que son públicas). Si el hitbox sale por debajo del mapa
*desde el proceso limiter() se lo elimina y se le anota un punto al otro jugador llamando al proceso arbitro(int pl). 
*/

Process limiter()

Private

	Hitbox hb;
	int left_limit;
	int right_limit;
	int pl;
	
End

Begin

	hb=father.id;
	priority=father.priority-2;

	say("limiter creado para el player "+hb.player);
	
	Switch (hb.player)
		Case 1:
			left_limit=H_HITBOXSIZE;
			right_limit=screenx/2-H_HITBOXSIZE;
		End
		
		Case 2:
			left_limit=screenx/2+H_HITBOXSIZE;
			right_limit=screenx-H_HITBOXSIZE;
		End
	End

	Loop
		If (hb.x<left_limit)
			hb.going=0;
			hb.dx=0;
			hb.idx=0;
			hb.x=left_limit;
		End

		If (hb.x>right_limit)
			hb.going=0;
			hb.dx=0;
			hb.idx=0;
			hb.x=right_limit;
		End

		If (hb.y>screeny+H_HITBOXSIZE)  //caer por debajo del nivel mata al gorila
			signal_action(s_kill_tree,S_IGN);
			signal(hb.id,s_kill_tree);

			If (hb.player==1)
				pl=2;
			Else
				pl=1;
			End

			arbitro(pl);

			BREAK;
		End

		FRAME;

		//ANTI-DESCONGELANTE
		If ((turn_promotion!=hb.player OR shooting==1) AND get_status(id)==2)
			signal(id,s_freeze);
		End
	End

End



/*
*PROCESS aim_process(int who)
*
*Abre un menú en la partida que permite disparar una banana estableciendo la fuerza y el angulo que darle a esta. Este proceso puede ser
*llamado por uno de los dos procesos hitbox() cuando es su turno y se encuentra en el suelo. Del menú se puede salir para volver a la partida
*(haciendo que a través de hitbox() se salga del modo shooting y recuperando el conrol sobre este) o para disparar la banana.
*/

Process aim_process(int who)
	
Private

	int angulo;
	float fuerza=min_fuerza;
	int textos[3];

End

Begin

	textos[0]=write(0,father.x,father.y-60,4,"ANGULO");
	textos[1]=write_var(0,father.x,father.y-50,4,angulo);
	textos[2]=write(0,father.x,father.y-40,4,"FUERZA");
	textos[3]=write_var(0,father.x,father.y-30,4,fuerza);

	signal_action(id,s_freeze_tree,S_IGN);

	FRAME;  //Necesita este FRAME; para no leer en su primer bucle la misma señal de KEYAIM que lo invocó
		
	Loop
		If (KEYAIM)
			shooting=0;
			
			BREAK;
		End

		If (KEYUP AND angulo<max_angulo)
			angulo++;
		End

		If (KEYDOWN AND angulo>min_angulo)
			angulo--;
		End

		Switch (who)
			Case 1:
				If (KEYRIGHT AND fuerza<max_fuerza)
					fuerza++;
				End

				If (KEYLEFT AND fuerza>min_fuerza)
					fuerza--;
				End
			End

			Case 2:
				If (KEYRIGHT AND fuerza>min_fuerza)
					fuerza--;
				End

				If (KEYLEFT AND fuerza<max_fuerza)
					fuerza++;
				End
			End
		End

		If (KEYSHOOT)
			banana(father.x,father.y,angulo,fuerza,who);

			BREAK;
		End

		FRAME;
	End

OnExit

	For (angulo=0;angulo<4;angulo++)  //"angulo" se usa aquí como si fuera un contador "i", esto lo que hace es borrar textos...
		delete_text(textos[angulo]);  //...es decir, no tiene nada que ver con los angulos.
	End

End



/*
*PROCESS banana(x,y,int angulo, float fuerza, int who)
*
*Proceso que hace un recorrido parabólico determinado por sus parámetros de entrada y que elimina procesos bloque() o hitbox() al colisionar 
*con ellos. Según el parámetro int who (que indica que jugador la dispara) transforma el valor de int angulo al formato que entiende Bennu,
*teniendo en cuenta hacia a donde apunta el gorila. El valor float fuerza, que es pasado como porcentaje, se multiplica por una constante y
*por el seno y coseno de int angulo para establecer las variables de avance (float bdx; float bdy; ). Luego entra en un bucle en el que se 
*altera sucesivamente su posición teninendo en cuenta toda esta información y se comprueba si hay colisiones. Si este proceso colisiona contra
*un proceso bloque() lo destruye y pasa el turno, si colisiona contra el proceso hitbox() del otro jugador lo elimina y llama al proceso 
*arbitro(int pl) pasandole como parámetro int who (jugador que disparó). Si choca contra el mismo hitbox() del jugador que ha disparado, se 
*sale del modo shooting y se pasa de turno. En todos los casos el proceso banana() se destruye (así como si sale por los lados o por debajo).
*/

Process banana(x,y,int angulo, float fuerza, int who)

Private

	float bdx;
	float resta_bdx;
	float bdy;
	float resta_bdy;
	int bidx;
	int bidy;
	int bgoing;
	int collider;
	Hitbox hb;

#ifdef DEBUG_INFO
	int id_textos_debug[7];
	string textos_debug[3]="bdx","bidx","bdy","bidy";
	int i;
	int margen_w;	//el valor que se le pasará como x a la función write
	int margen_wv;	//el valor que se le pasará como x a la función write_var
	int align_w;	//el valor que se le pasará como alineación a la función write
	int align_wv;	//el valor que se le pasará como alineación a la función write_var
#endif

End

Begin

	graph=graficos[2];

	say("banana pre init parameters: x=="+x+" y=="+y+" angulo=="+angulo+" fuerza=="+fuerza+" who=="+who);
#ifdef DEBUG_INFO
	Switch (who)
		Case 2:
			margen_w=screenx-65;
			align_w=0;
			margen_wv=screenx-70;
			align_wv=2;
		End
	
		Case 1:
			margen_w=65;
			align_w=2;
			margen_wv=70;
			align_wv=0;
		End
	End

	For (i=0;i<4;i++)
		id_textos_debug[i]=write(0,margen_w,10*i,align_w,textos_debug[i]);
	End

	id_textos_debug[4]=write_var(0,margen_wv,0,align_wv,bdx);
	id_textos_debug[5]=write_var(0,margen_wv,10,align_wv,bidx);
	id_textos_debug[6]=write_var(0,margen_wv,20,align_wv,bdy);
	id_textos_debug[7]=write_var(0,margen_wv,30,align_wv,bidy);
#endif

	Switch (who)
		Case 1:
			angulo=angulo*1000;
			x=x+H_HITBOXSIZE+2;
			bgoing=1;
		End

		Case 2: 
			angulo=180000-angulo*1000;
			x=x-H_HITBOXSIZE-3; 
			bgoing=-1;
		End
	End

	y=y-H_HITBOXSIZE-2;

	fuerza=fuerza*relacion_fuerza;
	
	bdx=fuerza*cos(angulo);
	bdy=fuerza*sin(angulo);
		
	say("banana angulo=="+angulo+" fuerza=="+fuerza+" x=="+x+" y=="+y+" bgoing=="+bgoing+" bdx=="+bdx+" bdy=="+bdy);

	FRAME;

	Loop
		
		bidx=bdx+resta_bdx;
		resta_bdx=resta_bdx+bdx-bidx;
		
		x=x+bidx;

		bdy=bdy-gravedad;

		bidy=bdy+resta_bdy;
		resta_bdy=resta_bdy+bdy-bidy;
	
		y=y-bidy;

		If (collider=collision(type hitbox))
			hb=collider;

			If (hb.player!=who)
				signal(collider,s_kill_tree);
				arbitro(who);

				BREAK;
			
			Else
				shooting=0;
				turn_change();

				BREAK;
			End
		End

		If (collider=collision_circle(type bloque))
			shooting=0;
			turn_change();
			signal(collider,s_kill);

			BREAK;
		End

		If (x>screenx OR x<0 OR y>screeny)
			shooting=0;
			turn_change();

			BREAK;
		End

#ifdef PIXEL_CHECK
	#if PIXEL_CHECK!=2
		pixel_checker(x);
	#endif
#endif

		FRAME;
	End

#ifdef DEBUG_INFO
OnExit

	For (i=0;i<8;i++)
		delete_text(id_textos_debug[i]);
	End
#endif

End



/*
*PROCESS arbitro(int pl)
*
*Anota un punto al jugador que se corresponda con el parámetro int pl que recibe. Si la puntuación del jugador es la necesaria para ganar
*la partida (establecida en la estructura global options), llama al proceso game_over(pl). Si no, muestra la puntuación durante un tiempo
*determinado, cambia el turno y crea un proceso hitbox(int pl) para el jugador que acaba de ser eliminado.
*/

Process arbitro(int pl)

Private

	int mensaje[1];
	int muestra_mensaje;

End

Begin

	puntos[pl]=puntos[pl]+1;
	
	If (puntos[pl]==opciones.puntos)  //puntos[0] de momento no se usa (ha de cambiar si pl se convierte en bool)
		game_over(pl);
		signal(id,s_kill);
	End

	mensaje[0]=write(0,screenx/2,70,7,"PUNTO PARA EL JUGADOR "+pl); 	//esto tendrá que ser distinto si hago de pl un bool
	mensaje[1]=write(0,screenx/2,80,1,"JUGADOR 1: "+puntos[1]+" JUGADOR 2: "+puntos[2]);

	While (muestra_mensaje<tiempo_mensaje)
		muestra_mensaje++;
	
		FRAME;
	End

	If (turn_promotion==turn)  //evita que un jugador repita turno al otro morir por caer al suelo tras romperse un bloque debajo suyo
		turn_change();         //(debe ir antes de alterar _pl porque sino hitbox_debug_info() funciona mal tras alguien anotar un punto)
	End

	If (pl==1)
		pl=2;
	Else
		pl=1;
	End

	hitbox(pl);
	shooting=0;

OnExit

	If (mensaje[0]!=0)
		delete_text(mensaje[0]);
		delete_text(mensaje[1]);
	End

End



/*
*PROCESS game_over(int pl)
*
*Muestra un mensaje anunciando que jugador es el ganador de la partida (pasado como parámetro int pl por el proceso arbitro() ) y deja el
*juego en este estado hasta que se pulsa una tecla que llame a la función reset_game() .
*/

Process game_over(int pl)

Private

	int ganador;

End

Begin

	If (exists(type pause_menu))
		signal(type pause_menu,s_kill);
	End

	ganador=write(0,screenx/2,70,4,"GANA EL JUGADOR "+pl);

	Loop
		If (KEYSHOOT==1 OR KEYAIM==1 OR key(_r))
			reset_game();

			BREAK;
		End

		FRAME;
	End

OnExit

	delete_text(ganador);

End	


#ifdef PRECISE_SENSOR

/*
*PROCESS sensor_ground()
*ver. PRECISE_SENSOR
*
*Detecta colisiones verticales y mueve a hitbox() en el sentido que indique su variable pública int direction_y en caso de que no haya
*obstáculos. El sensor se posiciona encima o debajo de hitbox(), pegado a él, según si este asciende (arriba), desciende o está sobre el suelo (abajo).
*Entonces entra en un bucle que itera tantas veces como píxeles haya de moverse en vertical en el presente fotograma (dado por la variable
*pública int idy de hitbox() ). Avanza el valor de int direction_y (-1,0 o 1) en cada iteración, comprobando colisiones. Si hay una colisión
*detiene a hitbox() alterando variables suyas y sale del bucle. Entonces hace FRAME;
*
*La posición horizontal la recibe a través de sensor_side() después que este haya hecho sus cálculos. Asi mismo, sensor_ground() envía 
*información de posición vertical a sensor_side() . También determina si el jugador está tocando el suelo.
*
*Es creado por hitbox(), congelado cuando no es su turno o el juego está en modo shooting, y se elimina al eliminarse el proceso hitbox() que 
*le creó.
*/

Process sensor_ground()

Private

	int framer;
	int i;
	int correccion;
	Hitbox hb;
#ifdef NEGADORA
	bool negador;  //no se si esto hace falta en este sensor (supongo que la caida es mas rapida sin el negador)(aunque no tiene pk...
	               //...ya que direction_y se calcula a partir de idy)(pero el sensor se ejecuta antes por la prioridad!!)
#endif
	
End

Begin
		
	graph=graficos[3];
	
	z=father.z-1;
	priority=father.priority-1;  //(sin esto da el error de meterse un pixel en el suelo)

	hb=father.id;
		
	FRAME;  //se hace FRAME antes del LOOP para que el primer acceso desde el otro sensor (mediante bigbro) no de error
		
	Loop
		If (hb.direction_y==-1)
			correccion=H_HITBOXSIZE*hb.direction_y-1;  //el -1 es la correccion en si
		Else
			correccion=H_HITBOXSIZE;  //no multiplico *hb.direction_y porque es 1
		End							  //en el caso de direction_y==0, el sensor igual se queda abajo (en el suelo)
	
		If (hb.idy==0)	
			framer=1;  //itera solo una vez
#ifdef NEGADORA
			negador=0;  //multiplica direction por 0, asi no hay avance
#endif		
		Else
			framer=abs(hb.idy);
#ifdef NEGADORA
			negador=1;
#endif
		End

		For (i=0;i<framer;i++)  //antes se usaba el parametro de la sentencia FRAME para iterar varias veces, pero este for lo hace mejor	
			y=hb.y+correccion;

			If (collision (type bloque))
				If (hb.direction_y==-1)
					hb.dy=0;
					BREAK;
				Else
					If (hb.on_ground==0)
						hb.on_ground=1;
						hb.dy=0;
						hb.jetpack=jet_max_fuel;
						BREAK;
					End
				End
			Else
#ifdef NEGADORA
				hb.y=hb.y+hb.direction_y*negador;
#else
				hb.y=hb.y+hb.direction_y;
#endif

				If (hb.on_ground==1)
					hb.on_ground=0;
				End
			End
		End
		
		smallbro.y=hb.y;

		FRAME;

		//ANTI-DESCONGELANTE
		If ((turn_promotion!=hb.player OR shooting==1) AND hb.on_ground==1 AND get_status(id)==2)
			signal(id,s_freeze);
		End
			//si el sensor no está en su turno pero no esta congelado (esto pasa cuando se pausa 
			//se des-pausa), el sensor se autocongela (un frame esta despierto, habria que implementar
			//una pausa un poco menos guarra)
	End

End



/*PROCESS sensor_side()
*ver. PRECISE_SENSOR
*
*Detecta colisiones horizontales y mueve horizontalmente al proceso hitbox() que le llamó si no hay obstaculos, en el sentido marcado por su
*variable pública int direction. Se posiciona pegado a hitbox() a uno de sus lados según el valor de direction. Entra en un bucle que itera 
*tantas veces como indique la variable pública de hitbox() int idx (movimiento horizontal en píxeles), comprobando si hay obstaculos y moviendo
*hitbox el valor de direction (-1, 0 o 1) en cada iteración. Si hay algún obstaculo detiene a hitbox() y sale del bucle. Finalmente muestra el 
*resultado de ese bucle en pantalla (no durante la ejecución del bucle), haciendo FRAME;
*
*Recibe su posición vertical (y) a través de sensor_ground() y envía a este su posición horizontal (x).
*sensor_side() es creado por hitbox(), se congela cuando no es su turno o se está en modo shooting y se elimina junto a hitbox() .
*/

Process sensor_side()

Private

	int framer;
	int i;
	int correccion;
	Hitbox hb;
#ifdef NEGADORA
	bool negador;  //no estoy seguro de si hace falta
#endif

End

Begin

	graph=graficos[4];
		
	z=father.z-1;
	priority=father.priority-1;  //con el sensor preciso no parece imprescindible
	hb=father.id;
		
	FRAME;  //se hace FRAME antes del LOOP para que el primer acceso desde el otro sensor (mediante smallbro) no de error
		
	Loop	
		If (hb.direction==-1)  //funciona bien sin correccion aunque no se alinean del todo los lados con el hitbox
			correccion=H_HITBOXSIZE+1;
		Else
			correccion=H_HITBOXSIZE;
		End
		
		If (hb.idx==0)	
			framer=1;
#ifdef NEGADORA
			negador=0;
#endif
		Else
			framer=abs(hb.idx);
#ifdef NEGADORA
			negador=1;
#endif
		End
	
		For (i=0;i<framer;i++)
			x=hb.x+correccion*hb.direction;  //la x se puede acceder tanto mediante hb. como father. puesto que es LOCAL y no PUBLIC
			
 			If (collision(type bloque))  //las colisiones mejor que se comprueben solo una vez
				say("colision lateral");
				hb.going=0;
				hb.dx=0;
				BREAK;
			Else
#ifdef NEGADORA
				hb.x=hb.x+hb.direction*negador;
#else
				hb.x=hb.x+hb.direction;
#endif
			End
		End

		bigbro.x=hb.x;
				//se le pasa la x al otro sensor después de calcularla aquí. 
				//la y nos la pasa el otro sensor. si leemos la y de father desde aquí por alguna razón llega con retraso
		//y=father.y;  //esto hace que la posición y nos llegue con retraso

		FRAME;

		//ANTI-DESCONGELANTE
		If ((turn_promotion!=hb.player OR shooting==1) AND hb.on_ground==1 AND get_status(id)==2)
			signal(id,s_freeze);
		End

	End

End


#else


/*
*PROCESS sensor_ground()
*
*Detecta colisiones y detiene el movimiento de hitbox() en caso de que choque contra un obstaculo. También determina si el personaje está
*sobre un suelo que detenga la gravedad.
*
*Se posiciona debajo de hitbox(), separado del borde de este una cantidad de pixeles igual al valor de la variable publica de hitbox() int idy
*(que indica cuantos píxeles ha de moverse hitbox() en el siguiente fotograma). Al colisionar contra un proceso bloque(), sensor_ground() vuelve
*al borde inferior de hitbox() y empieza a bajar píxel a píxel contando cuantos pasos da hasta que vuelve a tocar al proceso bloque(). Entonces
*avanza a hitbox() la cantidad de píxeles que ha contado en este paso, dejandolo justo encima del suelo. Frena su movimiento vertical.
*
*sensor_ground() es creado por hitbox(), se congela cuando no es su turno o el juego está en modo shooting y se termina junto a hitbox()
*/

Process sensor_ground()

Private

	int i;
	Hitbox hb;

End

Begin

	graph=graficos[3];
	
	z=father.z-1;
	priority=father.priority-1;
	hb=father.id;
		
	Loop
		//POSICION	
		x=hb.x;
		y=hb.y+hb.idy+H_HITBOXSIZE;

		//COLISION
		If (hb.on_ground==1 AND NOT collision(type bloque))
			hb.on_ground=0;
		End

		If (hb.on_ground==0 AND collision(type bloque))
			hb.on_ground=1;
			hb.dy=0;
			hb.idy=0;
			hb.jetpack=jet_max_fuel;

			y=hb.y+H_HITBOXSIZE;	
			i=0;
			FRAME(0);  //resetea collision? 
			
			Repeat
				y=y+1;
				i=i+1;
			Until (collision(type bloque))

			hb.y=hb.y+i;
		End

		FRAME(0);

		//DESENGANCHE
		If (hb.collider==1 AND collision(type bloque))
			hb.y=hb.y-1;
		End	

		FRAME;

		//ANTI-DESCONGELANTE
		If ((turn_promotion!=hb.player OR shooting==1) AND hb.on_ground==1 AND get_status(id)==2)	
			signal(id,s_freeze);			
		End						
	End

End



/*
*PROCESS sensor_roof()
*
*Funciona igual que sensor_ground() pero detectando colisiones contra techos. Para esto opera desde el borde superior de hitbox() . Es más
*sencillo que sensor_ground() puesto que no tiene que determinar si el personaje se encuentra en el suelo.
*/

Process sensor_roof()

Private

	int i;
	Hitbox hb;

End

Begin

	graph=graficos[3];
	
	z=father.z-1;
	priority=father.priority-1;
	hb=father.id;
		
	Loop
		//POSICION	
		x=hb.x;
		y=hb.y+hb.idy-H_HITBOXSIZE;	

		//COLISION
		If (collision(type bloque))
			hb.dy=0;
			hb.idy=0;
			
			y=hb.y-H_HITBOXSIZE;	
			i=0;
			FRAME(0);  //resetea collision? (minimiza el problema pero no lo soluciona)
			
			Repeat
				y=y-1;
				i=i+1;
			Until (collision(type bloque))

			hb.y=hb.y-i+1;  //el +1 adicional es porque sino se queda enganchado en el techo (es lo que seria la correccion en los horizontales)
		End

		FRAME(0);

		//DESENGANCHE
		If (hb.collider==1 AND collision(type bloque))
			hb.y=hb.y+1;
		End	

		FRAME;

		//ANTI-DESCONGELANTE
		If ((turn_promotion!=hb.player OR shooting==1) AND hb.on_ground==1 AND get_status(id)==2)
			signal(id,s_freeze);
		End
	End

End



/*
*PROCESS sensor_side(int side)
*
*Se crea uno a cada lado según el parámetro side. Cuando hitbox() intenta hacer un movimiento horizontal, el sensor_side() del lado al que
*se quiera mover compureba si está contra una pared para no dejarle en caso que sea así. En movimiento, el sensor_side() que coincida con 
*el sentido horizontal en el que se mueva hitbox() se separa de este tantos píxeles como indique su variable pública int idx, mientras que 
*el otro sensor se pega al borde de hitbox() por su lado pertinente. En caso de colisión contra bloque() funciona como sensor_ground() y 
*sensor_roof(), volviendo al lado de hitbox() y contando cuantos píxeles hay hasta la pared.
*/

Process sensor_side(int side)

Private

	int i;
	Hitbox hb;
	int correccion;	
	int on_wall;  //puede ser bool
	
End

Begin

	graph=graficos[4];
	
	z=father.z-1;
	priority=father.priority-1;
	hb=father.id;

	If (side==-1)
		correccion=1;  //correccion=side+H_HITBOXSIZE*side;
	Else
		correccion=0;  //correccion=H_HITBOXSIZE*side;
	End	

	correccion=-correccion+H_HITBOXSIZE*side;
			
	Loop
		//COMPROBACION INICIAL
		If (on_wall==1 AND hb.direction==side)
			hb.going=0;
			hb.dx=0;
			hb.idx=0;
		End

		//POSICION
		If (hb.direction==side)
			x=hb.x+correccion+hb.idx;
		Else
			x=hb.x+correccion;
		End

		y=hb.y;

		//COLISION
		If (on_wall==1 AND NOT collision(type bloque))
			on_wall=0;
		End

		If (on_wall==0 AND collision(type bloque))
			on_wall=1;
			hb.going=0;
			hb.dx=0;
			hb.idx=0;

			x=hb.x+correccion;
			i=0;
			FRAME(0);
	
			Repeat
				x=x+side;
				i=i+side;
			Until (collision(type bloque))

			hb.x=hb.x+i;
		End
		
		FRAME(0);

		//DESENGANCHE
		If (hb.collider==1 AND collision(type bloque))
			hb.x=hb.x-side;
		End  //esto hace que no se atranque (junto a otro igual en en otro sensor)

		FRAME;

		//ANTI-DESCONGELANTE
		If ((turn_promotion!=hb.player OR shooting==1) AND hb.on_ground==1 AND get_status(id)==2)
			signal(id,s_freeze);
		End
	End
End	
		

#endif


#ifdef PIXEL_CHECK
#ifndef STEPCHECK

/*
*FUNCTION pixel_checker(int xparam)
*
*Recibe la posición x del proceso que le llama (hitbox() o banana() ) como parámetro int xparam. Llama a la función pchecker(i) 
*pasandole como parámetro i=xparam (¿?) que comprueba si el valor es par o impar. Según el resultado pinta en el fondo de la pantalla
*un píxel de un color o de otro. Esto hace que dichos procesos dejen una estela al moverse. Es una función de debug que sirve para 
*comprobar la programación de los movimientos.
*
*La versión que se ejecuta si se define STEPCHECK sólo imprime píxeles en el fondo de la pantalla cada dos actualizaciones del valor x
*del proceso que ha llamado a pixel_checker(), dejando una estela menos densa.
*/

Function pixel_checker(int xparam)

Private

	int pcheck;

End

Begin
	
	pcheck=pchecker(father.x);

	Switch (pcheck)
		Case 0:
			put_pixel(father.x,father.y,rgb(0,255,0));
		End
		Case 1:
			put_pixel(father.x,father.y,rgb(255,50,0));
		End
		Default:
			put_pixel(father.x,father.y,rgb(50,100,150));
		End
	End

End

#else

Function pixel_checker(int xparam)	//Esta version pinta un pixel cada dos llamadas actualizaciones de x

Private

	int pcheck;
	Hitbox hb;	
End

Begin
	hb=father.id;

	If (hb.pcount==2)
		hb.pcount=0;
	End

	If (hb.prev_xlec!=xparam)
		hb.pcount=hb.pcount+1;
	End

	If (hb.pcount==1)
		pcheck=pchecker(father.x);

		Switch (pcheck)
			Case 0:
				put_pixel(father.x,father.y,rgb(0,255,0));
			End
			Case 1:
				put_pixel(father.x,father.y,rgb(255,50,0));
			End
			Default:
				put_pixel(father.x,father.y,rgb(50,100,150));
			End
		End
	End
End
#endif


/*
*FUNCTION int pchecker(i)
*
*Determina si el número pasado como parámetro i es par o impar. Para esto le resta 2, si da 0 es par, si da 1 es impar y si da otro resultado
*se vuelve a llamar a sí misma pasando tal resultado como parámetro i.
*/

Function int pchecker(i)

Begin

	If (i>1)  //hace esto porque si i es 0 el programa peta
		i=i-2;
	End

	If (i>-1)	
		Switch (i)
			Case 0:
				return(i);  //si devuelve esto, es par
			End
			Case 1:
				return(i);  //si devuelve esto, es impar
			End
			Default:
				i=pchecker(i);  //si no es ni 1 ni 0, se llama a si misma
				return(i);
			End
		End
	End

End
#endif
