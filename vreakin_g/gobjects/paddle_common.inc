//PADDLE_COMMON.INC
//Líneas de codigo comunes para los procesos de comportamiento de pala. Incluye la gestión recogida y aplicado
//de power-ups.

//Clasifica los contenidos nuevos del array global de structs pw_catch[] copiando estas entradas en los campos
//de individuals[] o allball[] según si los power-ups que encuentra son para aplicar a las bolas a medida que 
//vayan rebotando con la pala o son para aplicar a todas las bolas

WHILE ( pw_catch_write_index!=pw_catch_read_index )
	IF ( pw_catch[pw_catch_read_index].pu_type<8 )  //comprueba si es allball o individual mirando si el valor de pu_type contra 8
		individuals[ind_write_index].pu_type = pw_catch[pw_catch_read_index].pu_type;
		individuals[ind_write_index].pu_value = pw_catch[pw_catch_read_index].pu_value;
		ind_write_index++;
		IF ( ind_write_index>=_individuals_elements )
			ind_write_index = 0;
		END
	ELSE
		allball[allb_write_index].pu_type = pw_catch[pw_catch_read_index].pu_type;
		allball[allb_write_index].pu_value = pw_catch[pw_catch_read_index].pu_value;
		allb_write_index++;
		IF ( allb_write_index>=_allball_elements )
			allb_write_index = 0;
		END
	END
	pw_catch_read_index++;
	IF ( pw_catch_read_index>=_pw_catch_elements )
		pw_catch_read_index = 0;
	END
END


//Si hay colisión con una bola y hay power-ups en individuals[] por aplicar aplica 1. Si no hay colisión, comprueba si 
//hay power-ups en allball[] pendientes de aplicar y los aplica.
//Sabe si hay colisión porque Ball() escribe un msg_type de colisión y un msg_value con su id en el proceso Paddle()
//Paddle_common utiliza esa id para aplicarle un power-up, si tiene guardados. Esto hace que, en caso de colisión
//simultanea con mas de una bola, solo una se pueda llevar el power-up.

IF ( father.msg_type==_ball_paddle_collision AND ind_read_index!=ind_write_index )  //otorga power ups almacenados a las bolas que van chocando (está limitado a solo una bola en caso de choque simultaneo)
	IF ( exists( father.msg_value ) )
		father.msg_value.msg_type = individuals[ind_read_index].pu_type;  //msg_value tiene la id de la ultima Ball que chocó
		father.msg_value.msg_value = individuals[ind_read_index].pu_value;

		say( g_frame_counter+" | Paddle_common: individuals to id=="+father.msg_value );
		say( "  ---msg_type=="+father.msg_type+" msg_value=="+father.msg_value );
		say( "  ---pu_type=="+individuals[ind_read_index].pu_type+" pu_value=="+individuals[ind_read_index].pu_value+" r_idx=="+ind_read_index+" w_idx=="+ind_write_index );

		ind_read_index++;
		IF ( ind_read_index>=_individuals_elements )
			ind_read_index = 0;
		END
	END
	father.msg_type = 0;
	father.msg_value = 0;

ELSEIF ( allb_write_index!=allb_read_index )											

	say( g_frame_counter+" | Paddle_common: allball" );
	say( "  ---pu_type=="+allball[allb_read_index].pu_type+" pu_value=="+allball[allb_read_index].pu_value+" r_idx=="+allb_read_index+" w_idx=="+allb_write_index );
	
	allball_pwup( allball[allb_read_index].pu_type-8, allball[allb_read_index].pu_value );  //se resta 8 porque la lista de individuales está 8 numeros atras de la de allball, con los mismos elementos y el mismo orden
	allb_read_index++;
	IF ( allb_read_index>=_allball_elements )
		allb_read_index = 0;
	END
END



IF ( father.i_pos < 0.0+H_PAD_LENGTH )	
	father.i_pos = 0.0+H_PAD_LENGTH;
	di = 0;
END
IF ( father.i_pos > level.playfield_i-H_PAD_LENGTH )	
	father.i_pos = level.playfield_i-H_PAD_LENGTH;
	di = 0;
END

IF ( rotacion < 0 )  //sys/ij-to-xy.inc
	father.x = display_j-father.j_pos+camera_offset_j;
	father.y = father.i_pos-camera_offset_i;
ELSE
	father.x = father.i_pos-camera_offset_i;
	father.y = father.j_pos-camera_offset_j;
END