//PADDLES.INC
//Procesos para la pala

CONST
	_paddle_types = 4;  //0-index
END



//
// PROCESS Paddle( i_pos, j_pos )
//
// Sólo se mueve horizontalmente. Comprueba los bordes laterales para limitar su movimiento. Sus características se definen en el 
//struct global gameplay.
// Su comportamiento depende de un proceso hijo al que ejecuta según el valor de gamempla.paddle_type. Estos procesos alteran las 
//variables globales i_pos y j_pos de Paddle, mientras que Paddle (este proceso) se sigue usando para detectar colisiones.
// 
// Paddle_original      //Se mueve a velocidad fija gameplay.paddle_max_i_speed en el sentido que toque
// Paddle_w_acc         //Acelera gameplay.paddle_acc al pulsar las teclas y decelera gameplay.paddle_dec al no haber pulsación
// Paddle_w_acc2        //Acelera paddle_acc, resta paddle_dec al cambiar de dirección hasta llegar a di=0, luego sigue acelerando paddle_acc
// Paddle_fast_turn     //Pone di=0 al cambiar de dirección, luego acelera paddle_acc
// Paddle_rebound       //Cambia el signo de di al cambiar de dirección, manteniendo su velocidad
//
//


PROCESS Paddle( i_pos, j_pos )

PRIVATE
	//debug
	int debug_text;
END

BEGIN
	say( "PADDLE" );
	file = idfpg_rt;
	graph = _paddle_graph;
	priority++;  //uniformar

	debug_text = write_var( 0, 0, 0, 0, i_pos );

	IF ( gameplay.paddle_type>_paddle_types )
		say( "Paddle: paddle_type "+gameplay.paddle_type+" not found, reverting to default" );
		gameplay.paddle_type = _default_paddle_type;
	END
	SWITCH ( gameplay.paddle_type )
		CASE 0:
			Paddle_original( );
		END
		CASE 1:
			Paddle_w_acc( );
		END
		CASE 2:
			Paddle_w_acc2( );
		END
		CASE 3:
			Paddle_fast_turn( );
		END
		CASE 4:
			Paddle_rebound( );
		END

		DEFAULT:
			say( "Paddle: ERROR, invalid paddle_type" );
		END
	END

	//Si la parte COMMON fuera en cada sub-proceso de comportamiento aquí irian estas líneas
	LOOP
		signal( id, s_freeze );
		FRAME;
	END

	//COMMON
	//LOOP
	//	IF ( i_pos < 0.0+H_PAD_LENGTH )	
	//		i_pos = 0.0+H_PAD_LENGTH;
	//	END
	//	IF ( i_pos > level.playfield_i-H_PAD_LENGTH )	
	//		i_pos = level.playfield_i-H_PAD_LENGTH;
	//	END
	//	
	//	INCLUDE "sys/ij-to-xy.inc";
	//
	//	FRAME;
	//END

ONEXIT
	IF ( exists( son ) )
		signal( son, s_kill );
	END
	delete_text( debug_text );
	say( "Paddle saliendo" );
END



//
// PROCESS Paddle_original( )
// Hace que paddle se mueva a una velocidad constante igual a gameplay.paddle_max_i_speed en cualquiera de sus dos sentidos.
//


PROCESS Paddle_original( )

PRIVATE
	float di;  //variable que se usa para alterar i_pos, valdrá avance o -avance

	//debug
	int debug_text;
END

BEGIN
	say( "Paddle_type "+gameplay.paddle_type+" is Paddle_original");
	priority = father.priority+1;	
	
	debug_text = write_var( 0, 0, 10, 0, di );

	LOOP
		IF ( KEYLEFT )
			di = -gameplay.paddle_max_i_speed;
		ELSEIF ( KEYRIGHT )
			di = gameplay.paddle_max_i_speed;
		ELSE
			di = 0;
		END

		IF ( di != 0 )
			father.i_pos = father.i_pos+di;
		END

		//Aquí irían las líneas comunes a todos los subprocesos. Esto se tendría que repetir en cada sub-proceso
		//(o usar un inc). Por motivos de codigo limpio de momento está en Paddle. Por otro lado, estando COMMON
		//en cada uno de los subprocesos se podría poner di a 0 en cuanto la pala toque la pared. (Como está ahora
		//puede que de problemas al acelerar desde la pared con los comportamientos w_acc y w_acc2)
		//
		//COMMON
		//
		//IF ( father.i_pos < 0.0+H_PAD_LENGTH )	
		//	father.i_pos = 0.0+H_PAD_LENGTH;
		//	di = 0;
		//END
		//IF ( father.i_pos > level.playfield_i-H_PAD_LENGTH )	
		//	father.i_pos = level.playfield_i-H_PAD_LENGTH;
		//	di = 0;
		//END
		//
		//IF ( rotacion < 0 )  //sys/ij-to-xy.inc
		//	father.x = display_j-father.j_pos+camera_offset_j;
		//	father.y = father.i_pos-camera_offset_i;
		//ELSE
		//	father.x = father.i_pos-camera_offset_i;
		//	father.y = father.j_pos-camera_offset_j;
		//END
		//

		INCLUDE "gobjects/paddle_common.inc";


		//priority check
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type PADDLE" );
		END
		
		FRAME;
	END
ONEXIT
	delete_text( debug_text );
	say( "Paddle_original( ) saliendo" );
END



//
// PROCESS Paddle_w_acc( )
// Hace que paddle sume a su velocidad el valor de gameplay.paddle_acc al moverse lateralmente y reste el valor de 
//gameplay.paddle_dec al dejar de moverse.
//


PROCESS Paddle_w_acc( )  //Paddle_w_acc

PRIVATE
	float di;  //variable que se usa para alterar i_pos, será alterada con paddle_acc o paddle_dec

	//debug
	int debug_text;
END

BEGIN
	say( "Paddle_type "+gameplay.paddle_type+" is Paddle_w_acc");
	priority = father.priority+1;

	debug_text = write_var( 0, 0, 10, 0, di );

	LOOP
		IF ( KEYLEFT )
			IF ( di>-gameplay.paddle_max_i_speed)
				di = di-gameplay.paddle_acc;
			END
		ELSEIF ( KEYRIGHT )
			IF ( di<gameplay.paddle_max_i_speed )
				di = di+gameplay.paddle_acc;
			END
		ELSE
			//deceleracion
			IF ( di>0.0 )
				di = di-gameplay.paddle_dec;
				IF ( di<0.0 )
					di = 0.0;
				END
			ELSEIF ( di<0.0 )
				di = di+gameplay.paddle_dec;
				IF ( di>0.0 )
					di = 0.0;
				END
			END

		END
		
		IF ( di>gameplay.paddle_max_i_speed )
			di = gameplay.paddle_max_i_speed;
		ELSEIF ( di<-gameplay.paddle_max_i_speed )
			di = -gameplay.paddle_max_i_speed;
		END
		
		IF ( di != 0.0 )
			father.i_pos = father.i_pos+di;
		END
		
		INCLUDE "gobjects/paddle_common.inc";

		FRAME;
	END
ONEXIT
	delete_text( debug_text );
	say( "Paddle_w_acc( ) saliendo" );
END



//
// PROCESS Paddle_w_acc( )
// Hace que paddle sume a su velocidad el valor de gameplay.paddle_acc al moverse lateralmente desde velocidad 0 y reste el
//valor de gameplay.paddle_dec al dirigirse a velocidad 0 (cambio de sentido en movimiento) o al dejar de moverse.
//


PROCESS Paddle_w_acc2( )  //Paddle_w_acc2

PRIVATE
	float di;  //variable que se usa para alterar i_pos, será alterada con paddle_acc o paddle_dec

	//debug
	int debug_text;
END

BEGIN
	say( "Paddle_type "+gameplay.paddle_type+" is Paddle_w_acc2");
	priority = father.priority+1;

	debug_text = write_var( 0, 0, 10, 0, di );

	LOOP
		//aceleracion
		IF ( KEYLEFT )
			IF ( di<=0.0 )  //IF( di<-gameplay.paddle_max_i_speed AND di<0.0)
				di = di-gameplay.paddle_acc;
			ELSEIF ( di>0.0 )
				di = di-gameplay.paddle_dec;
			END
		ELSEIF ( KEYRIGHT )
			IF ( di>=0.0 )
				di = di+gameplay.paddle_acc;
			ELSEIF ( di<0.0 )
				di = di+gameplay.paddle_dec;
			END
		ELSE
			//deceleracion
			IF ( di>0.0 )
				di = di-gameplay.paddle_dec;
				IF ( di<0.0 )
					di = 0.0;
				END
			ELSEIF ( di<0.0 )
				di = di+gameplay.paddle_dec;
				IF ( di>0.0 )
					di = 0.0;
				END
			END

		END
		
		IF ( di>gameplay.paddle_max_i_speed )
			di = gameplay.paddle_max_i_speed;
		ELSEIF ( di<-gameplay.paddle_max_i_speed )
			di = -gameplay.paddle_max_i_speed;
		END
		
		IF ( di != 0.0 )
			father.i_pos = father.i_pos+di;
		END

		INCLUDE "gobjects/paddle_common.inc";
		
		FRAME;
	END
ONEXIT
	delete_text( debug_text );
	say( "Paddle_w_acc2( ) saliendo" );
END



//
// PROCESS Paddle_fast_turn( )
// Hace que paddle sume a su velocidad el valor de gameplay.paddle_acc al moverse lateralmente desde velocidad 0, pase a 
//velocidad 0 en cuanto se de un cambio de sentido en movimiento) y reste gameplay.paddle_dec al dejar de moverse.
//


PROCESS Paddle_fast_turn( )  //Paddle_fast_turn

PRIVATE
	float di;  //variable que se usa para alterar i_pos, será alterada con paddle_acc o paddle_dec

	//debug
	int debug_text;
END

BEGIN
	say( "Paddle_type "+gameplay.paddle_type+" is Paddle_fast_turn" );
	priority = father.priority+1;

	debug_text = write_var( 0, 0, 10, 0, di );

	LOOP
		//aceleracion
		IF ( KEYLEFT )
			IF ( di<=0.0 )
				di = di-gameplay.paddle_acc;
			ELSEIF ( di>0.0 )
				di = 0.0;
			END
		ELSEIF ( KEYRIGHT )
			IF ( di>=0.0 )
				di = di+gameplay.paddle_acc;
			ELSEIF ( di<0.0 )
				di = 0.0;
			END
		ELSE
			//deceleracion
			IF ( di>0.0 )
				di = di-gameplay.paddle_dec;
				IF ( di<0.0 )
					di = 0.0;
				END
			ELSEIF ( di<0.0 )
				di = di+gameplay.paddle_dec;
				IF ( di>0.0 )
					di = 0.0;
				END
			END

		END
		
		IF ( di>gameplay.paddle_max_i_speed )
			di = gameplay.paddle_max_i_speed;
		ELSEIF ( di<-gameplay.paddle_max_i_speed )
			di = -gameplay.paddle_max_i_speed;
		END
		
		IF ( di != 0.0 )
			father.i_pos = father.i_pos+di;
		END

		INCLUDE "gobjects/paddle_common.inc";
		
		FRAME;
	END
ONEXIT
	delete_text( debug_text );
		say( "Paddle_fast_turn( ) saliendo" );
END



//
// PROCESS Paddle_rebound( )
// Hace que paddle sume a su velocidad el valor de gameplay.paddle_acc al moverse lateralmente desde velocidad 0, cambie el signo
//del valor de la velocidad actual al cambiar de sentido y reste el valor de gameplay.paddle_dec al dejar de moverse.
//


PROCESS Paddle_rebound( )  //Paddle_rebound

PRIVATE
	float di;  //variable que se usa para alterar i_pos, será alterada con paddle_acc o paddle_dec

	//debug
	int debug_text;
END

BEGIN
	say( "Paddle_type "+gameplay.paddle_type+" is Paddle_rebound" );
	priority = father.priority+1;

	debug_text = write_var( 0, 0, 10, 0, di );

	LOOP
		//aceleracion
		IF ( KEYLEFT )
			IF ( di<=0.0 )
				di = di-gameplay.paddle_acc;
			ELSEIF ( di>0.0 )
				di = -di;
			END
		ELSEIF ( KEYRIGHT )
			IF ( di>=0.0 )
				di = di+gameplay.paddle_acc;
			ELSEIF ( di<0.0 )
				di = -di;
			END
		ELSE
			//deceleracion
			IF ( di>0.0 )
				di = di-gameplay.paddle_dec;
				IF ( di<0.0 )
					di = 0.0;
				END
			ELSEIF ( di<0.0 )
				di = di+gameplay.paddle_dec;
				IF ( di>0.0 )
					di = 0.0;
				END
			END

		END
		
		IF ( di>gameplay.paddle_max_i_speed )
			di = gameplay.paddle_max_i_speed;
		ELSEIF ( di<-gameplay.paddle_max_i_speed )
			di = -gameplay.paddle_max_i_speed;
		END
		
		IF ( di != 0.0 )
			father.i_pos = father.i_pos+di;
		END
		
		INCLUDE "gobjects/paddle_common.inc";

		FRAME;
	END
ONEXIT
	delete_text( debug_text );
	say( "Paddle_rebound( ) saliendo" );
END



//
// PROCESS Paddle_live_switch( )
// Permite cambiar el comportamiento de Paddle() sin entrar en el new gameplay menu. Funciona invocado desde control menu, y permitiendo
//elegir un comportamiento del Paddle apretando el botón numerico correspondiente.
//


PROCESS Paddle_live_switch( )

PRIVATE
	int paddle_id;
	float paddle_i_pos;
	float paddle_j_pos;  //En principio j_pos es siempre igual...
END

BEGIN
	say( "PADDLE LIVE SWITCH" );
	say( "Pulsa el numero correspondiente para cambiar el comportamiento de la pala" );
	say( "--disponibles del 0 al "+_paddle_types );
	LOOP
		IF ( key( _0 ) )
			gameplay.paddle_type = 0;
			BREAK;
		ELSEIF ( key( _1 ) )
			gameplay.paddle_type = 1;
			BREAK;
		ELSEIF ( key( _2 ) )
			gameplay.paddle_type = 2;
			BREAK;
		ELSEIF ( key( _3 ) )
			gameplay.paddle_type = 3;
			BREAK;
		ELSEIF ( key( _4 ) )
			gameplay.paddle_type = 4;
			BREAK;
		END

		FRAME;
	END

	say( "Paddle_live_switch: paddle_type "+gameplay.paddle_type+" seleccionado" );

	WHILE ( paddle_id = get_id( type Paddle ) )
		paddle_i_pos = paddle_id.i_pos;
		paddle_j_pos = paddle_id.j_pos;
		signal( paddle_id, s_kill );
	END

	Paddle( paddle_i_pos, paddle_j_pos );
ONEXIT
	say( "Paddle_live_switch saliendo" );
END